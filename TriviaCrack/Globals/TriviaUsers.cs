﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Diagnostics;

namespace TriviaCrack
{
    public static class TriviaUsers
    {
        public static List<string> GetAllUserNames(string currentUser)
        {
            var UserManager = HttpContext.Current.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>();

            List<string> allUserNames = new List<string>();

            if (UserManager == null)
            {
                return new List<string>();
            }

            foreach (var item in UserManager.Users.ToList())
            {
                if (item.Id == currentUser)
                {
                    continue;
                }

                ExpandedUserDTO user = new ExpandedUserDTO()
                {
                    UserName = item.UserName,
                    Email = item.Email
                };
                allUserNames.Add(user.UserName);
            }
            return allUserNames;
        }

        public static string FindUserId(string userName)
        {
            var user = HttpContext.Current.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>().FindByName(userName);
            return user.Id;
        }
        public static ApplicationUser FindUser(string id)
        {
            var user = HttpContext.Current.GetOwinContext()
                .GetUserManager<ApplicationUserManager>().FindById(id);
            return user;
        }

    }
}