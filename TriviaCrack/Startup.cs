﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TriviaCrack.Startup))]
namespace TriviaCrack
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
