﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TriviaCrack.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        public ActionResult InternalServerError()
        {
            Response.StatusCode = 500;
            return View();
        }

        public ActionResult BadGateway()
        {
            Response.StatusCode = 502;
            return View();
        }

        public ActionResult ServiceUnavailable()
        {
            Response.StatusCode = 503;
            return View();
        }

        public ActionResult GatewayTimeout()
        {
            Response.StatusCode = 504;
            return View();
        }
    }
}