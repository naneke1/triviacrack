﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Castle.Core.Internal;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using TriviaCrack.Models;
using TriviaCrack.Data_Access;
using TriviaCrack.Services;

namespace TriviaCrack.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private TriviaContext db = new TriviaContext();
        private const int brainiacThreshold = 24;
        private const int moneybagsThreshold = 999;
        private const int categoryThreshold = 9;
        private const int gameThreshold = 9;
        private const int consecutiveThreshold = 4;
        private const int highReward = 250;
        private const int midReward = 100;
        private const int lowReward = 50;

        public ManageController()
        {
        }

        public ManageController(TriviaContext context)
        {
            db = context;
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            var userId = User.Identity.GetUserId();
            var player = db.Players.Single(s => s.Id == userId);

            if (!TriviaUsers.FindUser(player.Id).IsEnabled)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : message == ManageMessageId.ChangeProfilePhotoSuccess ? "Your profile photo has been changed."
                : message == ManageMessageId.ChangeLocationSuccess ? "Your location has been changed."
                : message == ManageMessageId.ManagePreferencesSuccess ? "Your preferences have been changed."
                : "";

            ViewData["level"] = player.Level;
            ViewData["score"] = player.OverallScore;
            ViewData["prevThreshold"] = player.PrevScoreThreshold;
            ViewData["challengeTotal"] = player.ChallengeCount;
            ViewData["challengeWins"] = player.ChallengeWins;
            var playerStatus = db.AchievementStatus.Single(a => a.PlayerId == userId);
            CheckPlayerBasicStatus(player, playerStatus);
            var playerQuestions = (from s in db.Statistics where s.PlayerId == userId select s).ToList();
            var categoryTotals = new List<int>{ 0, 0, 0, 0, 0, 0};
            var categoryCorrectTotals = new List<int>{ 0, 0, 0, 0, 0, 0 };
            GetTotals(playerQuestions, categoryTotals, categoryCorrectTotals);
            SetCategoryViewData(categoryTotals, categoryCorrectTotals);
            CheckPlayerCategoryStatus(player, playerStatus, categoryCorrectTotals);
            var playerGames = (from g in db.Games where g.Player1Id == userId || g.Player2Id == userId select g).ToList();
            var gameTotals = new List<int> { 0, 0, 0};
            GetGameTotals(playerGames, gameTotals, userId);
            var opponents = new List<Player>();
            GetOpponents(opponents, playerGames, userId);
            GetVersusStats(opponents, playerGames, userId);
            
            ViewData["gamesWon"] = gameTotals[0];
            ViewData["gamesLost"] = gameTotals[1];
            ViewData["gamesLostResigned"] = gameTotals[2];
            ViewData["opponents"] = opponents;
            var consecutive = GetConsecutiveQuestionsAnsweredCorrectly(playerQuestions, userId);
            ViewData["numConsecutive"] = consecutive;
            CheckPlayerGameStatus(gameTotals[0], playerStatus, player);
            CheckPlayerConsecutiveStatus(consecutive, playerStatus, player);

            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await UserManager.GetLoginsAsync(userId),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId),
                Location = TriviaUsers.FindUser(userId).Location,
                UserPhoto = TriviaUsers.FindUser(userId).UserPhoto
            };
            ViewData["coins"] = player.Coins;
            AchievementService service = new AchievementService(this.db);
            ViewData["achievements"] = service.GetAllAchievements();

            return View(model);
            
        }

        private void CheckPlayerConsecutiveStatus(int consecutive, AchievementStatus status, Player player)
        {
            if (consecutive > consecutiveThreshold && status.QuizMasterAchieved == false)
            {
                status.QuizMasterAchieved = true;
                player.AddCoins(lowReward);
            }

            var save = Task.Run(() => this.db.SaveChangesAsync()).GetAwaiter().GetResult();
        }

        private void CheckPlayerGameStatus(int winTotal, AchievementStatus status, Player player)
        {
            if (winTotal > gameThreshold && status.ChampionAchieved == false)
            {
                status.ChampionAchieved = true;
                player.AddCoins(highReward);
            }

            var save = Task.Run(() => this.db.SaveChangesAsync()).GetAwaiter().GetResult();
        }

        private void CheckPlayerCategoryStatus(Player player, AchievementStatus playerStatus, List<int> categoryCorrectTotals)
        {
            if (categoryCorrectTotals[0] > categoryThreshold && playerStatus.ArtConnoisseurAchieved == false)
            {
                player.AddCoins(midReward);
                playerStatus.ArtConnoisseurAchieved = true;
            }
            else if (categoryCorrectTotals[1] > categoryThreshold && playerStatus.PopCultureWhizAchieved == false)
            {
                player.AddCoins(midReward);
                playerStatus.PopCultureWhizAchieved = true;
            }
            else if (categoryCorrectTotals[3] > categoryThreshold && playerStatus.MapScholarAchieved == false)
            {
                player.AddCoins(midReward);
                playerStatus.MapScholarAchieved = true;
            }
            else if (categoryCorrectTotals[4] > categoryThreshold && playerStatus.HistoryBuffAchieved == false)
            {
                player.AddCoins(midReward);
                playerStatus.HistoryBuffAchieved = true;
            }
            else if (categoryCorrectTotals[5] > categoryThreshold && playerStatus.ScienceNerdAchieved == false)
            {
                player.AddCoins(midReward);
                playerStatus.ScienceNerdAchieved = true;
            }
            else if (categoryCorrectTotals[2] > categoryThreshold && playerStatus.SportsNutAchieved == false)
            {
                player.AddCoins(midReward);
                playerStatus.SportsNutAchieved = true;
            }

            var save = Task.Run(() => this.db.SaveChangesAsync()).GetAwaiter().GetResult();
        }

        private void CheckPlayerBasicStatus(Player player, AchievementStatus status)
        {
            if(player.Coins > moneybagsThreshold && status.MoneybagsAchieved == false)
            {
                status.MoneybagsAchieved = true;
                player.AddCoins(highReward);
            }

            if (player.OverallScore > brainiacThreshold && status.BrainiacAchieved == false)
            {
                status.BrainiacAchieved = true;
                player.AddCoins(highReward);
            }

            var save = Task.Run(() => this.db.SaveChangesAsync()).GetAwaiter().GetResult();
        }

        private int GetConsecutiveQuestionsAnsweredCorrectly(List<Statistics> playerQuestions, string userId)
        {
            var consecutiveCount = 0;

            if (playerQuestions.IsNullOrEmpty())
            {
                consecutiveCount = 0;
            }
            else
            {
                var maxGameId = playerQuestions.Last().GameId;

                for (int i = 0; i <= maxGameId; i++)
                {
                    var currentGameId = i;

                    var questionsInCurrentGame = GetQuestionsFromGivenGame(playerQuestions, currentGameId);
                    var currentConsecutiveCount = GetCurrentConsecutiveCount(questionsInCurrentGame);
                    if (currentConsecutiveCount > consecutiveCount)
                    {
                        consecutiveCount = currentConsecutiveCount;
                    }
                }
            }
            
            return consecutiveCount;
        }

        private int GetCurrentConsecutiveCount(List<Statistics> questionsInCurrentGame)
        {
            var count = 0;
            var maxCount = 0;
            for (int i = 0; i < questionsInCurrentGame.Count; i++)
            {
                if(questionsInCurrentGame[i].IsAnsweredCorrectly)
                {
                    count++;

                    if (count > maxCount)
                    {
                        maxCount = count;
                    }
                } else
                {
                    if (count > maxCount)
                    {
                        maxCount = count;
                    }
                    count = 0;
                }
            }

            return maxCount;
        }
        private List<Statistics> GetQuestionsFromGivenGame(List<Statistics> playerQuestions, int currentGameId)
        {
            List<Statistics> gameQuestions = new List<Statistics>();
            for (int i = 0; i < playerQuestions.Count; i++)
            {
                if (playerQuestions[i].GameId == currentGameId)
                {
                    gameQuestions.Add(playerQuestions[i]);
                }
            }

            return gameQuestions;
        }
        private void GetVersusStats(List<Player> opponents, List<Game> playerGames, string userId)
        {
            for(int i = 0; i < opponents.Count; i++)
            {
                List<int> versusStats = new List<int> { 0, 0 };
                CountResults(opponents[i].Id, userId, versusStats, playerGames);
                ViewData[opponents[i].UserName + "VersusWins"] = versusStats[0];
                ViewData[opponents[i].UserName + "VersusLosses"] = versusStats[1];
            }
        }

        private void CountResults(string oppId, string userId, List<int> versusStats, List<Game> playerGames)
        {
            for (int i = 0; i < playerGames.Count; i++)
            {
                if(!playerGames[i].IsActive && playerGames[i].Player1Id == oppId && playerGames[i].WinnerId == oppId)
                {
                    versusStats[0]++;
                } else if(!playerGames[i].IsActive && playerGames[i].Player1Id == oppId && playerGames[i].WinnerId != oppId)
                {
                    versusStats[1]++;
                } else if(!playerGames[i].IsActive && playerGames[i].Player2Id == oppId && playerGames[i].WinnerId == oppId)
                {
                    versusStats[0]++;
                } else if (!playerGames[i].IsActive && playerGames[i].Player2Id == oppId && playerGames[i].WinnerId != oppId)
                {
                    versusStats[1]++;
                }
            }
        }

        private void GetOpponents(List<Player> opponents, List<Game> playerGames, string playerId)
        {
            for(int i = 0; i < playerGames.Count; i++)
            {
                var displayNameP1 = GetUserWithoutEmail(playerGames[i].Player1.UserName);
                var displayNameP2 = GetUserWithoutEmail(playerGames[i].Player2.UserName);
                var player1 = playerGames[i].Player1;
                var player2 = playerGames[i].Player2;
                if (playerGames[i].Player1Id == playerId && !opponents.Contains(player2))
                {
                    opponents.Add(player2);
                    GetUserData(playerGames[i].Player2.UserName);
                }

                if (playerGames[i].Player2Id == playerId && !opponents.Contains(player1))
                {
                    opponents.Add(player1);
                    GetUserData(playerGames[i].Player1.UserName);
                }
            }

            if(opponents.Count == 0)
            {
                opponents.Add(new Player { UserName="No Friends Exist For This Player", Level = 0, OverallScore = 0, PrevScoreThreshold = 0});
            }
        }

        private string GetUserWithoutEmail(string fullname)
        {
            List<string> parts = fullname.Split('@').ToList();

            return parts[0];
        }

        private void GetUserData(string fullname)
        {
            var player = new Player {UserName = "No Friends Exist For This Player", Level = 0, OverallScore = 0, PrevScoreThreshold = 0 };
            var userId = "";
            if (fullname.Equals("No Friends Exist For This Player"))
            {
                var categoryTotals = new List<int> { 1, 1, 1, 1, 1, 1 };
                var categoryCorrectTotals = new List<int> { 1, 1, 1, 1, 1, 1 };
                SetOppCategoryViewData(categoryTotals, categoryCorrectTotals, fullname);
                var gameTotals = new List<int> { 1, 1, 1 };
                ViewData[player.UserName + "Wins"] = gameTotals[0];
                ViewData[player.UserName + "Losses"] = gameTotals[1];
                ViewData[player.UserName + "Resigns"] = gameTotals[2];
            } else
            {
                player = db.Players.Single(s => s.UserName == fullname);
                userId = player.Id;
                var playerQuestions = (from s in db.Statistics where s.PlayerId == userId select s).ToList();
                var categoryTotals = new List<int> { 0, 0, 0, 0, 0, 0 };
                var categoryCorrectTotals = new List<int> { 0, 0, 0, 0, 0, 0 };
                GetTotals(playerQuestions, categoryTotals, categoryCorrectTotals);
                SetOppCategoryViewData(categoryTotals, categoryCorrectTotals, fullname);
                var gameTotals = new List<int> { 0, 0, 0 };
                var oppGames = (from g in db.Games where g.Player1Id == userId || g.Player2Id == userId select g).ToList();
                GetGameTotals(oppGames, gameTotals, userId);
                ViewData[player.UserName + "Wins"] = gameTotals[0];
                ViewData[player.UserName + "Losses"] = gameTotals[1];
                ViewData[player.UserName + "Resigns"] = gameTotals[2];
            }
            
        }

        private void SetOppCategoryViewData(List<int> categoryTotals, List<int> categoryCorrectTotals, string oppName)
        {
            var currentOppArtTotal = oppName + "ArtTotal";
            var currentOppArtCorrect = oppName + "ArtCorrect";
            var currentOppEntertainTotal = oppName + "EntertainTotal";
            var currentOppEntertainCorrect = oppName + "EntertainCorrect";
            var currentOppGeographyTotal = oppName + "GeographyTotal";
            var currentOppGeographyCorrect = oppName + "GeographyCorrect";
            var currentOppHistoryTotal = oppName + "HistoryTotal";
            var currentOppHistoryCorrect = oppName + "HistoryCorrect";
            var currentOppScienceTotal = oppName + "ScienceTotal";
            var currentOppScienceCorrect = oppName + "ScienceCorrect";
            var currentOppSportsTotal = oppName + "SportsTotal";
            var currentOppSportsCorrect = oppName + "SportsCorrect";

            if (categoryTotals[0] == 0) //this condition is so page will render, can't divide by 0
            {
                ViewData[currentOppArtTotal] = 1;
            }
            else
            {
                ViewData[currentOppArtTotal] = categoryTotals[0];
            }
            ViewData[currentOppArtCorrect] = categoryCorrectTotals[0];

            if (categoryTotals[1] == 0)
            {
                ViewData[currentOppEntertainTotal] = 1;
            }
            else
            {
                ViewData[currentOppEntertainTotal] = categoryTotals[1];
            }
            ViewData[currentOppEntertainCorrect] = categoryCorrectTotals[1];

            if (categoryTotals[3] == 0)
            {
                ViewData[currentOppGeographyTotal] = 1;
            }
            else
            {
                ViewData[currentOppGeographyTotal] = categoryTotals[3];
            }
            ViewData[currentOppGeographyCorrect] = categoryCorrectTotals[3];

            if (categoryTotals[4] == 0)
            {
                ViewData[currentOppHistoryTotal] = 1;
            }
            else
            {
                ViewData[currentOppHistoryTotal] = categoryTotals[4];
            }
            ViewData[currentOppHistoryCorrect] = categoryCorrectTotals[4];

            if (categoryTotals[5] == 0)
            {
                ViewData[currentOppScienceTotal] = 1;
            }
            else
            {
                ViewData[currentOppScienceTotal] = categoryTotals[5];
            }
            ViewData[currentOppScienceCorrect] = categoryCorrectTotals[5];

            if (categoryTotals[2] == 0)
            {
                ViewData[currentOppSportsTotal] = 1;
            }
            else
            {
                ViewData[currentOppSportsTotal] = categoryTotals[2];
            }
            ViewData[currentOppSportsCorrect] = categoryCorrectTotals[2];
        }

        private void GetGameTotals(List<Game> playerGames, List<int> gameTotals, string currentUserId)
        {
            foreach (Game g in playerGames)
            {
                if (!g.IsActive && g.WinnerId == currentUserId)
                {
                    gameTotals[0]++;
                } else if (!g.IsActive && g.WinnerId != currentUserId)
                {
                    gameTotals[1]++;

                    if (g.DidResign)
                    {
                        gameTotals[2]++;
                    }
                }  
            }
        }

        private void SetCategoryViewData(List<int> categoryTotals, List<int> categoryCorrectTotals) {
            if (categoryTotals[0] == 0) //this condition is so page will render, can't divide by 0
            {
                ViewData["artTotal"] = 1;
            }
            else
            {
                ViewData["artTotal"] = categoryTotals[0];
            }
            ViewData["artCorrect"] = categoryCorrectTotals[0];

            if (categoryTotals[1] == 0)
            {
                ViewData["entertainmentTotal"] = 1;
            }
            else
            {
                ViewData["entertainmentTotal"] = categoryTotals[1];
            }
            ViewData["entertainmentCorrect"] = categoryCorrectTotals[1];

            if (categoryTotals[3] == 0)
            {
                ViewData["geographyTotal"] = 1;
            }
            else
            {
                ViewData["geographyTotal"] = categoryTotals[3];
            }
            ViewData["geographyCorrect"] = categoryCorrectTotals[3];

            if (categoryTotals[4] == 0)
            {
                ViewData["historyTotal"] = 1;
            }
            else
            {
                ViewData["historyTotal"] = categoryTotals[4];
            }
            ViewData["historyCorrect"] = categoryCorrectTotals[4];

            if (categoryTotals[5] == 0)
            {
                ViewData["scienceTotal"] = 1;
            }
            else
            {
                ViewData["scienceTotal"] = categoryTotals[5];
            }
            ViewData["scienceCorrect"] = categoryCorrectTotals[5];

            if (categoryTotals[2] == 0)
            {
                ViewData["sportsTotal"] = 1;
            }
            else
            {
                ViewData["sportsTotal"] = categoryTotals[2];
            }
            ViewData["sportsCorrect"] = categoryCorrectTotals[2];
        }
        private void GetTotals(List<Statistics> playerQuestions, List<int> categoryTotals, List<int> categoryCorrectTotals)
        {
            foreach (Statistics s in playerQuestions)
            {
                categoryTotals[s.Question.CategoryId - 1]++;
                if (s.IsAnsweredCorrectly)
                {
                    categoryCorrectTotals[s.Question.CategoryId - 1]++;
                }
            }
        }

        //
        // GET: /Manage/ChangeProfilePhoto
        public ActionResult ChangeProfilePhoto()
        {
            return PartialView("_EditPhoto");
        }

        [HttpPost]
        public async Task<ActionResult> ChangeProfilePhoto(IndexViewModel model)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (Request.Files.Count > 0)
            {
                byte[] imageData = null;
                HttpPostedFileBase file = Request.Files["ProfileImage"];

                if (file != null && file.ContentLength > 0)
                {
                    using (var binary = new BinaryReader(file.InputStream))
                    {
                        imageData = binary.ReadBytes(file.ContentLength);
                    }
                }

                user.UserPhoto = imageData;
            }

            var result = await UserManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }

            return RedirectToAction("Index", new { Message = ManageMessageId.ChangeProfilePhotoSuccess });
        }

        //
        // GET: /Manage/ChangeLocation
        public ActionResult ChangeLocation()
        {
            List<string> countryList = new List<string>();
            CultureInfo[] cInfoList = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo cInfo in cInfoList)
            {
                RegionInfo R = new RegionInfo(cInfo.LCID);
                if (!(countryList.Contains(R.EnglishName)))
                {
                    countryList.Add(R.EnglishName);
                }
            }

            countryList.Sort();
            ViewBag.CountryList = countryList;
            return View();
        }

        //
        // POST: /Manage/ChangeLocation
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeLocation(ChangeLocationViewModel model, FormCollection value)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            
            user.Location = value["CountryList"];

            var result = await UserManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }

            return RedirectToAction("Index", new { Message = ManageMessageId.ChangeLocationSuccess });
        }

        //
        // GET: /Manage/ManagePreferences
        public async Task<ActionResult> ManagePreferences()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            return View(new ManagePreferencesViewModel { Sound = user.Sound });
        }

        //
        // POST: /Manage/ManagePreferences
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ManagePreferences(ManagePreferencesViewModel model)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            user.Sound = model.Sound;

            Session["Sound"] = user.Sound;

            var result = await UserManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }

            return RedirectToAction("Index", new { Message = ManageMessageId.ManagePreferencesSuccess });
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            ChangeProfilePhotoSuccess,
            ChangeLocationSuccess,
            ManagePreferencesSuccess,
            Error
        }

#endregion
    }
}