﻿using PagedList;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Controllers
{
    public class AnswerController : Controller
    {
        private TriviaContext db;

        public AnswerController()
        {
            this.db = new TriviaContext();
        }

        public AnswerController(TriviaContext context)
        {
            this.db = context;
        }

        // GET: Answer
        public ActionResult Index(int? page)
        {
            return View(this.db.Answers.OrderBy(i => i.Text).ToPagedList(page ?? 1, 8));
        }

        // GET: Answer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = this.db.Answers.Find(id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            
            return View(answer);
        }

        // GET: Answer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Answer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Text")] Answer answer)
        {
            if (ModelState.IsValid)
            {
                this.db.Answers.Add(answer);
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(answer);
        }

        // GET: Answer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = this.db.Answers.Find(id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            
            return View(answer);
        }

        // POST: Answer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Text")] Answer answer)
        {
            if (ModelState.IsValid)
            {
                this.db.Entry(answer).State = EntityState.Modified;
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(answer);
        }

        // GET: Answer/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = this.db.Answers.Find(id);
            if (answer == null)
            {
                return HttpNotFound();
            }
           
            return View(answer);
        }

        // POST: Answer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Answer answer = this.db.Answers.Find(id);
            this.db.Answers.Remove(answer);
            this.db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
