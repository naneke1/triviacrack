﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using TriviaCrack.Services;

namespace TriviaCrack.Controllers
{
    public class StatisticsController : Controller
    {
        private TriviaContext db;
        private SiteStatsService statsService;
        private int artId;
        private int entertainmentId;
        private int sportsId;
        private int geographyId;
        private int historyId;
        private int scienceId;
        private int counter = 1;

        public StatisticsController()
        {
            this.db = new TriviaContext();
            this.statsService = new SiteStatsService(this.db);
        }

        public StatisticsController(TriviaContext context)
        {
            this.db = context;
            this.statsService = new SiteStatsService(this.db);
        }

        // GET: Statistics
        public ActionResult Index()
        {
            var statistics = this.db.Statistics.Include(s => s.Game).Include(s => s.Player).Include(s => s.Question);
            List<Player> players = this.db.Players.ToList();
            this.setCategoriesIDs();
            this.SetPlayerStatsViewBag(players);
            this.setOverallStatsViewBag();
            this.setMonthlyStatsViewBag();
            this.setWeeklyStatsViewBag();

            return View(statistics.ToList());
        }

        private void setCategoriesIDs()
        {
            this.artId = this.db.Categories.Single(c => c.Description == "Art").Id;
            this.entertainmentId = this.db.Categories.Single(c => c.Description == "Entertainment").Id;
            this.sportsId = this.db.Categories.Single(c => c.Description == "Sports").Id;
            this.geographyId = this.db.Categories.Single(c => c.Description == "Geography").Id;
            this.historyId = this.db.Categories.Single(c => c.Description == "History").Id;
            this.scienceId = this.db.Categories.Single(c => c.Description == "Science").Id;
        }

        private void setWeeklyStatsViewBag()
        {
            ViewBag.WeeklyArtStats = this.statsService.ReturnWeeklyCategoryStats(this.artId);
            ViewBag.WeeklyEntertainmentStats = this.statsService.ReturnWeeklyCategoryStats(this.entertainmentId);
            ViewBag.WeeklySportsStats = this.statsService.ReturnWeeklyCategoryStats(this.sportsId);
            ViewBag.WeeklyGeographyStats = this.statsService.ReturnWeeklyCategoryStats(this.geographyId);
            ViewBag.WeeklyHistoryStats = this.statsService.ReturnWeeklyCategoryStats(this.historyId);
            ViewBag.WeeklyScienceStats = this.statsService.ReturnWeeklyCategoryStats(this.scienceId);
        }

        private void setMonthlyStatsViewBag()
        {
            ViewBag.MonthlyArtStats = this.statsService.ReturnMonthlyCategoryStats(this.artId);
            ViewBag.MonthlyEntertainmentStats = this.statsService.ReturnMonthlyCategoryStats(this.entertainmentId);
            ViewBag.MonthlySportsStats = this.statsService.ReturnMonthlyCategoryStats(this.sportsId);
            ViewBag.MonthlyGeographyStats = this.statsService.ReturnMonthlyCategoryStats(this.geographyId);
            ViewBag.MonthlyHistoryStats = this.statsService.ReturnMonthlyCategoryStats(this.historyId);
            ViewBag.MonthlyScienceStats = this.statsService.ReturnMonthlyCategoryStats(this.scienceId);
        }

        private void setOverallStatsViewBag()
        {
            ViewBag.OverallArtStats = this.statsService.ReturnOverallCategoryStats(this.artId);
            ViewBag.OverallEntertainmentStats = this.statsService.ReturnOverallCategoryStats(this.entertainmentId);
            ViewBag.OverallSportsStats = this.statsService.ReturnOverallCategoryStats(this.sportsId);
            ViewBag.OverallGeographyStats = this.statsService.ReturnOverallCategoryStats(this.geographyId);
            ViewBag.OverallHistoryStats = this.statsService.ReturnOverallCategoryStats(this.historyId);
            ViewBag.OverallScienceStats = this.statsService.ReturnOverallCategoryStats(this.scienceId);
        }

        private void SetPlayerStatsViewBag(List<Player> allPlayers)
        {
            List<String> topArt = GetTopPlayer(allPlayers);
            ViewBag.TopArtName = topArt[0];
            ViewBag.TopArtScore = topArt[1];
            counter++;

            List<String> topEntertain = GetTopPlayer(allPlayers);
            ViewBag.TopEntertainName = topEntertain[0];
            ViewBag.TopEntertainScore = topEntertain[1];
            counter++;

            List<String> topGeography = GetTopPlayer(allPlayers);
            ViewBag.TopGeographyName = topGeography[0];
            ViewBag.TopGeographyScore = topGeography[1];
            counter++;

            List<String> topHistory = GetTopPlayer(allPlayers);
            ViewBag.TopHistoryName = topHistory[0];
            ViewBag.TopHistoryScore = topHistory[1];
            counter++;

            List<String> topScience = GetTopPlayer(allPlayers);
            ViewBag.TopScienceName = topScience[0];
            ViewBag.TopScienceScore = topScience[1];
            counter++;

            List<String> topSport = GetTopPlayer(allPlayers);
            ViewBag.TopSportsName = topSport[0];
            ViewBag.TopSportsScore = topSport[1];

            List<String> topOverall = GetTopOverallPlayer(allPlayers);
            ViewBag.TopOverallName = topOverall[0];
            ViewBag.TopOverallScore = topOverall[1];
        }

        private List<String> GetTopPlayer(List<Player> allPlayers)
        {
            List<String> topData = new List<String>();
            List<int> topScores = new List<int>();
            
            for(int i = 0; i < allPlayers.Count; i++)
            {
                topScores.Add(this.statsService.ReturnCategoryQuestionWinsForPlayer(counter, allPlayers[i].Id));
            }

            var highest = topScores.Max();
            var topIndex = topScores.IndexOf(highest);

            topData.Add(allPlayers[topIndex].UserName);
            topData.Add(highest.ToString());
            return topData;
        }

        private List<String> GetTopOverallPlayer(List<Player> allPlayers)
        {
            List<String> topOverallData = new List<String>();
            List<int> overallScores = new List<int>();

            for (int i = 0; i < allPlayers.Count; i++)
            {
                overallScores.Add(this.statsService.ReturnTotalQuestionWinsForPlayer(allPlayers[i].Id));
            }

            var highest = overallScores.Max();
            var topIndex = overallScores.IndexOf(highest);

            topOverallData.Add(allPlayers[topIndex].UserName);
            topOverallData.Add(highest.ToString());
            return topOverallData;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
