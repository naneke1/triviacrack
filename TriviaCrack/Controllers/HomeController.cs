﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Castle.Core.Internal;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using TriviaCrack.ViewModels;
using TriviaCrack.Services;

namespace TriviaCrack.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        private TriviaService triviaService;
        private TriviaContext db;

        public HomeController()
        {
            this.db = new TriviaContext();
        }

        public HomeController(TriviaContext context)
        {
            this.db = context;
        }

        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }

            ModelState.Clear();
            
            

            if (!db.Players.ToList().IsNullOrEmpty())
            {
                Session["LoggedInUserId"] = User.Identity.GetUserId();
                this.triviaService = new TriviaService(db);
                var loggedInPlayersGames = this.triviaService.FindPlayerGames();
                ViewData["loggedInPlayerId"] = this.triviaService.LoggedInPlayer.Id;
                
                ViewData["IsEnabled"] = TriviaUsers.FindUser(this.triviaService.LoggedInPlayer.Id).IsEnabled.ToString();
                return View(loggedInPlayersGames);
            }

            return View(this.triviaService.GetEmptyPlayerGames());
        }

        public ActionResult Help()
        {
            PowerUpService service = new PowerUpService(this.db);
            AchievementService achievementService = new AchievementService(this.db);

            ViewData["extraTimePowerUp"] = service.GetPowerUp("Extra Time");
            ViewData["bombPowerUp"] = service.GetPowerUp("Bomb");
            ViewData["autoCorrectPowerUp"] = service.GetPowerUp("Auto Correct");
            ViewData["achievements"] = achievementService.GetAllAchievements();
            return View();
        }

        //Finds the game the user wants to resign from.
        public ActionResult Resign(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = this.db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }

            var loggedInPlayerId = Session["LoggedInUserId"].ToString();
            ViewData["LoggedInPlayerId"] = loggedInPlayerId;

            return View(game);
        }

        //Resigns the game the user chose by setting the game's isActive attribute to false.
        [HttpPost, ActionName("Resign")]
        [ValidateAntiForgeryToken]
        public ActionResult ResignGame(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var gameToUpdate = this.db.Games.Find(id);
            try
            {
                if (gameToUpdate.Player1Id == Session["LoggedInUserId"].ToString())
                {
                    gameToUpdate.WinnerId = gameToUpdate.Player2Id;
                }

                else
                {
                    gameToUpdate.WinnerId = gameToUpdate.Player1Id;
                }
                gameToUpdate.IsActive = false;
                gameToUpdate.EndDate = DateTime.Now;
                gameToUpdate.DidResign = true;
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (RetryLimitExceededException  /*exception*/)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }
            return View(gameToUpdate);
        }

        public FileContentResult UserPhotos()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return GetDefaultImage(HttpContext.Server.MapPath(@"~/Images/noImg.png"));
            }

            String userId = Session["LoggedInUserId"].ToString();

            if (userId == null)
            {
                return GetDefaultImage(HttpContext.Server.MapPath(@"~/Images/noImg.png"));
            }
            var userImage = HttpContext.GetOwinContext().Get<ApplicationDbContext>().Users.FirstOrDefault(x => x.Id == userId);
            return new FileContentResult(userImage.UserPhoto, "image/jpeg");
        }

        private FileContentResult GetDefaultImage(string fileName)
        {
            BinaryReader binaryReader = new BinaryReader(new FileStream(fileName, FileMode.Open, FileAccess.Read));
            return File(binaryReader.ReadBytes((int)new FileInfo(fileName).Length), "image/png");
        }
    }
}