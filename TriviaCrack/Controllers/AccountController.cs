﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using TriviaCrack.Models;
using System.Configuration;
using System.Data.Entity.Migrations;
using Microsoft.AspNet.Identity.EntityFramework;
using System.IO;
using Castle.Core.Internal;
using TriviaCrack.Data_Access;

namespace TriviaCrack.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private TriviaContext db = new TriviaContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private int defaultStartLevel = 1;
        private int defaultCoins = 500;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            this.UserManager = userManager;
            this.SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return this._signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                this._signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return this._userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                this._userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            this.CreateAdminIfNeeded();

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = this.UserManager.FindByEmail(model.Email);

            if (!user.IsEnabled)
            {
                ModelState.AddModelError("", "This account has been locked.");
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await this.SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return this.RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await this.SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await this.SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return this.RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            List<string> countryList = new List<string>();
            CultureInfo[] cInfoList = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo cInfo in cInfoList)
            {
                RegionInfo R = new RegionInfo(cInfo.LCID);
                if (!(countryList.Contains(R.EnglishName)))
                {
                    countryList.Add(R.EnglishName);
                }
            }

            countryList.Sort();
            ViewBag.CountryList = countryList;
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register([Bind(Exclude = "UserPhoto")] RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                byte[] imageData = null;

                HttpPostedFileBase poImgFile = Request.Files["UserPhoto"];
                if (poImgFile != null && poImgFile.ContentLength > 0)
                {
                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        imageData = binary.ReadBytes(poImgFile.ContentLength);
                    }
                }

                if (imageData == null)
                {
                    FileInfo fileInfo = new FileInfo(HttpContext.Server.MapPath(@"~/Images/noImg.png"));
                    BinaryReader binaryReader = new BinaryReader(new FileStream(HttpContext.Server.MapPath(@"~/Images/noImg.png"), FileMode.Open, FileAccess.Read));
                    imageData = binaryReader.ReadBytes((int)fileInfo.Length);
                }

                var user = new ApplicationUser {UserName = model.Email, Email = model.Email, Location = model.Location, UserPhoto = imageData, IsEnabled = true };

                var result = await this.UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await this.SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    Player newPlayer = new Player { Id = user.Id, UserName = user.UserName, Level = this.defaultStartLevel };
                    newPlayer.AddCoins(this.defaultCoins);
                    this.db.Players.Add(newPlayer);
                    AchievementStatus newStatus = new AchievementStatus { PlayerId = newPlayer.Id};
                    this.db.AchievementStatus.Add(newStatus);
                    await this.db.SaveChangesAsync();
                    return RedirectToAction("Index", "Home");
                }

               this. AddErrors(result);
            }

            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await this.UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this.UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await this.UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await this.UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await this.UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            this.AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            Session["Workaround"] = 0;
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await this.SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await this.UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await this.SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await this.AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await this.SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return this.RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    var allUsers = TriviaUsers.GetAllUserNames("a1");
                    ViewBag.AllUsers = allUsers;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = this.AuthenticationManager.GetExternalLoginInfo();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }

               FileInfo fileInfo = new FileInfo(HttpContext.Server.MapPath(@"~/Images/noImg.png"));
                    BinaryReader binaryReader = new BinaryReader(new FileStream(HttpContext.Server.MapPath(@"~/Images/noImg.png"), FileMode.Open, FileAccess.Read));
                    var imageData = binaryReader.ReadBytes((int)fileInfo.Length);

                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, UserPhoto = imageData, Sound = true, IsEnabled = true };
                int defaultStartLevel = 1;
                Player player = new Player() { Id = user.Id, UserName = user.Email, Level = defaultStartLevel };
                int defaultCoins = 500;
                player.AddCoins(defaultCoins);
                var players = TriviaUsers.GetAllUserNames(player.Id);
                var playerFound = false;

                foreach (var current in players)
                {
                    if (current.Equals(player.UserName))
                    {
                        playerFound = true;
                    }
                }

                if (!playerFound)
                {
                    db.Players.AddOrUpdate(player);
                }
                
                AchievementStatus newStatus = new AchievementStatus { PlayerId = player.Id };
                this.db.AchievementStatus.AddOrUpdate(newStatus);
                this.db.SaveChanges();
                var result = this.UserManager.Create(user);
                if (result.Succeeded)
                {
                    result =  this.UserManager.AddLogin(user.Id, info.Login);
                    Session["LoggedInUserId"] = user.Id;
                    if (result.Succeeded)
                    {


                        this.SignInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                        return this.RedirectToLocal(returnUrl);
                    }
                }
                this.AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            this.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._userManager != null)
                {
                    this._userManager.Dispose();
                    this._userManager = null;
                }

                if (this._signInManager != null)
                {
                    this._signInManager.Dispose();
                    this._signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
        // Utility

        // Add CreateAdminIfNeeded
        #region private void CreateAdminIfNeeded()
        private void CreateAdminIfNeeded()
        {
            string AdminUserName = ConfigurationManager.AppSettings["AdminUserName"];
            string AdminPassword = ConfigurationManager.AppSettings["AdminPassword"];
            var existingAdministrator = UserManager.FindByEmail(AdminUserName);

            

            if (existingAdministrator == null)
            {

                if (!this.RoleManager.RoleExists("Administrator"))
                {
                    this.RoleManager.Create(new IdentityRole("Administrator"));
                }

                var newAdministrator = new ApplicationUser { Id = "a1", UserName = AdminUserName, Email = AdminUserName, Location = "United States", Sound = false, IsEnabled = true };
                var newAdministrator2 = new ApplicationUser { Id = "a2", UserName = "admin2@triviacrack.com", Email = "admin2@triviacrack.com", Location = "United States", Sound = true, IsEnabled = true };

                newAdministrator.UserPhoto = this.GetDefaultImageByteArray();
                newAdministrator2.UserPhoto = this.GetDefaultImageByteArray();

                var AdminUserCreateResult = this.UserManager.Create(newAdministrator, AdminPassword);
                var AdminUser2CreateResult = this.UserManager.Create(newAdministrator2, AdminPassword);

                this.UserManager.AddToRole(newAdministrator.Id, "Administrator");
                this.UserManager.AddToRole(newAdministrator2.Id, "Administrator");

                existingAdministrator = newAdministrator;
            }

            if (existingAdministrator != null)
            {
                if (this.db.Players.IsNullOrEmpty())
                {
                    Player a1 = new Player { Id = "a1", UserName = "admin@triviacrack.com" , Level = this.defaultStartLevel};
                    a1.AddCoins(this.defaultCoins);
                    this.db.Players.Add(a1);
                    Player a2 = new Player { Id = "a2", UserName = "admin2@triviacrack.com", Level = this.defaultStartLevel };
                    a2.AddCoins(this.defaultCoins);
                    this.db.Players.Add(a2);

                    this.db.SaveChanges();
                }
            }
        }
        #endregion

        private byte[] GetDefaultImageByteArray()
        {
            FileInfo fileInfo = new FileInfo(HttpContext.Server.MapPath(@"~/Images/noImg.png"));
            BinaryReader binaryReader = new BinaryReader(new FileStream(HttpContext.Server.MapPath(@"~/Images/noImg.png"), FileMode.Open, FileAccess.Read));
            return binaryReader.ReadBytes((int)fileInfo.Length);
        }
        // Add RoleManager
        #region public ApplicationRoleManager RoleManager
        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return this._roleManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                this._roleManager = value;
            }
        }
        #endregion

    }
}