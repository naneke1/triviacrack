﻿using System.Web.Mvc;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using TriviaCrack.Services;

namespace TriviaCrack.Controllers
{
    public class ClassicChallengeController : Controller
    {
        private TriviaService triviaService;

        public ClassicChallengeController()
        {
            this.triviaService = new TriviaService(new TriviaContext());
        }

        public ClassicChallengeController(TriviaContext context, string loggedInPlayerId)
        {
            this.triviaService = new TriviaService(context, loggedInPlayerId);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Question()
        {
            var tempGameId = TempData["gameId"] == null ? "" : TempData["gameId"] as string;

            if (tempGameId.Equals(""))
            {
                return RedirectToAction("Index", "Home");
            }

            int gameId = int.Parse(tempGameId);

            var currentPlayerChallengeData = this.triviaService.FindLoggedInPlayerChallengeData(gameId);
            var opponentPlayerChallengeData = this.triviaService.FindOpponentPlayerChallengeData(gameId);

            bool tieBreakerOccured = currentPlayerChallengeData.CurrentQuestion >= 7 && opponentPlayerChallengeData.CurrentQuestion == 6;
            bool opponentsChallengeTurn = currentPlayerChallengeData.CurrentQuestion >= 6 && opponentPlayerChallengeData.CurrentQuestion < 6;
            bool challengeEnded = (currentPlayerChallengeData.CurrentQuestion >= 6 && opponentPlayerChallengeData.CurrentQuestion >= 6) && (currentPlayerChallengeData.TotalCorrect != opponentPlayerChallengeData.TotalCorrect);

            if (tieBreakerOccured || opponentsChallengeTurn || challengeEnded)
            {
                this.triviaService.SwitchChallengeTurns(gameId);
                return RedirectToAction("Index", "Home");
            }

            ViewData["CurrentQuestion"] = currentPlayerChallengeData.CurrentQuestion;

            Question question = this.triviaService.GetNextChallengeQuestion(gameId);

            if (question == null)
            {
                return RedirectToAction("Index", "Home");
            }
            Session["currentQuestionId"] = question.Id;
            this.triviaService.AddQuestionStat(question.Id, gameId, this.triviaService.LoggedInPlayer.Id);

            ViewData["ClassicChallengeGameId"] = gameId;
            ViewData["TotalCorrect"] = currentPlayerChallengeData.TotalCorrect;
            ViewData["OpponentTotalCorrect"] = opponentPlayerChallengeData.TotalCorrect;
            ViewData["OpponentCurrentQuestion"] = opponentPlayerChallengeData.CurrentQuestion;
            TempData["gameId"] = "";

            return View(question);
        }

        [HttpPost]
        public ActionResult SwitchTurns(int? gameId)
        {
            int gameID = int.Parse(Session["gameId"].ToString());
            this.triviaService.SwitchChallengeTurns(gameID);

            return Json(new { redirectToUrl = Url.Action("Index", "Home") }, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult GetNextQuestion(int? gameId)
        {
            Session["gameId"] = gameId;
            TempData["gameId"] = gameId.ToString();

            return RedirectToAction("Question");
        }

        [HttpPost]
        public ActionResult StartNewClassicChallenge(string categoryToSteal, int? gameId, string categoryToBet)
        {
            Session["category"] = categoryToSteal;
            TempData["gameId"] = gameId.ToString();
            int gameID = int.Parse(gameId.ToString());

            this.triviaService.StartNewChallenge(gameID, categoryToSteal, categoryToBet);
            return Json(new { redirectToUrl = Url.Action("Question", "ClassicChallenge") }, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public ActionResult CorrectAnswer(int? gameId)
        {
            TempData["gameId"] = gameId.ToString();
            Session["gameId"] = gameId;
            int gameID = int.Parse(gameId.ToString());
            var questionID = int.Parse(Session["currentQuestionId"].ToString());
            this.triviaService.HandleCorrectAnswer(gameID);
            this.triviaService.MarkQuestionAnswered(gameID, this.triviaService.LoggedInPlayer.Id, questionID, "Challenge", true);

            return new EmptyResult();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public ActionResult IncorrectAnswer(int? gameId)
        {
            TempData["gameId"] = gameId.ToString();
            Session["gameId"] = gameId;
            int gameID = int.Parse(gameId.ToString());
            var questionID = int.Parse(Session["currentQuestionId"].ToString());
            this.triviaService.MarkQuestionAnswered(gameID, this.triviaService.LoggedInPlayer.Id, questionID, "Challenge", false);
            return new EmptyResult();
        }
    }
}