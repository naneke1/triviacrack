﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Web.Mvc;
using TriviaCrack.Data_Access;
using TriviaCrack.Services;

namespace TriviaCrack.Controllers
{
    public class GeneratorController : Controller
    {
        private TriviaService triviaService;

        public GeneratorController()
        {
            this.triviaService = new TriviaService(new TriviaContext());
        }

        public GeneratorController(TriviaContext context, string loggedInPlayerId)
        {
            this.triviaService = new TriviaService(context, loggedInPlayerId);
        }

        // GET: Generator
        public ActionResult Questions()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateQuestions(int? numberOfQuestions, string category, string difficulty)
        {
            string response = String.Empty;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string url = "https://opentdb.com/api.php?amount=" + numberOfQuestions + "&category=" + category + "&type=multiple";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";

            using (HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = webResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        response = reader.ReadToEnd();
                    }
                }
            }

            var jsonObject = JsonConvert.DeserializeObject<dynamic>(response);
            this.triviaService.AddQuestions(jsonObject, category, difficulty);

            return Json(new { redirectToUrl = Url.Action("Index", "Question") }, JsonRequestBehavior.AllowGet);
        }
    }
}