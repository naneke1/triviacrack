﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Controllers
{
    public class SubmittedQuestionsController : Controller
    {
        private TriviaContext db;

        public SubmittedQuestionsController(TriviaContext context)
        {
            this.db = context;
        }

        public SubmittedQuestionsController()
        {
            this.db = new TriviaContext();
        }

        // GET: SubmittedQuestions
        public ActionResult Index()
        {
            return View(db.SubmittedQuestions.ToList());
        }

        // GET: SubmittedQuestions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubmittedQuestion submittedQuestion = db.SubmittedQuestions.Find(id);
            if (submittedQuestion == null)
            {
                return HttpNotFound();
            }
            
            return View(submittedQuestion);
        }

        // GET: SubmittedQuestions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SubmittedQuestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Text,CorrectAnswer,IncorrectAnswer1,IncorrectAnswer2,IncorrectAnswer3,CategoryName")] SubmittedQuestion submittedQuestion)
        {
            if (ModelState.IsValid)
            {
                db.SubmittedQuestions.Add(submittedQuestion);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(submittedQuestion);
        }

        // GET: SubmittedQuestions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubmittedQuestion submittedQuestion = db.SubmittedQuestions.Find(id);
            if (submittedQuestion == null)
            {
                return HttpNotFound();
            }
            
            return View(submittedQuestion);
        }

        // POST: SubmittedQuestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Text,CorrectAnswer,IncorrectAnswer1,IncorrectAnswer2,IncorrectAnswer3,CategoryName")] SubmittedQuestion submittedQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(submittedQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(submittedQuestion);
        }

        // GET: SubmittedQuestions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubmittedQuestion submittedQuestion = db.SubmittedQuestions.Find(id);
            if (submittedQuestion == null)
            {
                return HttpNotFound();
            }
            
            return View(submittedQuestion);
        }

        // POST: SubmittedQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubmittedQuestion submittedQuestion = db.SubmittedQuestions.Find(id);
            db.SubmittedQuestions.Remove(submittedQuestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult Approve(int? questionId)
        {
            var approvedQuestion = (from s in db.SubmittedQuestions
                                    where s.Id == questionId
                                    select s).Single();

            var correctAnswerId = GetAnswerId(approvedQuestion.CorrectAnswer);
            var incorrectAnswer1Id = GetAnswerId(approvedQuestion.IncorrectAnswer1);
            var incorrectAnswer2Id = GetAnswerId(approvedQuestion.IncorrectAnswer2);
            var incorrectAnswer3Id = GetAnswerId(approvedQuestion.IncorrectAnswer3);
            var categoryID = GetCategoryId(approvedQuestion.CategoryName);
            
            var latestQuestion = new Question { Id = db.Questions.Count() + 1, Text = approvedQuestion.Text, CorrectAnswerId = correctAnswerId, AnswerChoice1Id = incorrectAnswer1Id, AnswerChoice2Id = correctAnswerId, AnswerChoice3Id = incorrectAnswer2Id, AnswerChoice4Id = incorrectAnswer3Id, CategoryId = categoryID };
            
            db.Questions.Add(latestQuestion);

            db.SubmittedQuestions.Remove(approvedQuestion);
            db.SaveChanges();
            return Json(new { redirectToUrl = Url.Action("Index", "SubmittedQuestions")} );
        }

        private int GetAnswerId(String answer)
        {
            var correctAnswerID = 0;
            
            for (int i = 1; i < db.Answers.Count() + 1; i++)
            {
                if (db.Answers.Find(i).Text == answer)
                {
                    correctAnswerID = i;
                }
            }

            if (correctAnswerID == 0)
            {
                var latestAnswer = new Answer { Id = db.Answers.Count() + 1, Text = answer };
                db.Answers.Add(latestAnswer);
                correctAnswerID = latestAnswer.Id;
                db.SaveChanges();
            }

            return correctAnswerID;
        }

        private int GetCategoryId(string categoryName)
        {
            var index = 0;
            for(int i = 1; i < db.Categories.Count() + 1; i++)
            {
                if(db.Categories.Find(i).Description == categoryName)
                {
                    index = i;
                }
            }

            return index;
        }
    }
}
