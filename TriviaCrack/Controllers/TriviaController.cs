﻿using System.Web.Mvc;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using TriviaCrack.ViewModels;
using TriviaCrack.Services;
using System.Linq;

namespace TriviaCrack.Controllers
{
    public class TriviaController : Controller
    {
        private TriviaService triviaService;
        private bool leveledUp = false;
        
        public TriviaController()
        {
            this.triviaService = new TriviaService(new TriviaContext());
        }

        public TriviaController(TriviaContext context, string loggedInPlayerId)
        {
            this.triviaService = new TriviaService(context, loggedInPlayerId);
        }

        //Retrieves a question based on which category the spinner lands on.
        public ActionResult Question()
        {
            var tempGameId = Session["gameId"].ToString();
            var category = Session["category"].ToString();
            var isCrownQuestion = Session["isCrownQuestion"].ToString();

            Game game = this.triviaService.FindGameById(int.Parse(tempGameId));
            Player player = this.triviaService.LoggedInPlayer;

            if (TempData["activeRequest"] == null)
            {
                this.IncorrectAnswer(int.Parse(tempGameId), isCrownQuestion);
                return RedirectToAction("Index", "Home");
            }

            if (game.PlayerTurn != player.Id)
            {
                return RedirectToAction("Index", "Home");
            }

            var gameInformation = this.triviaService.GetGameInformation(game, category, isCrownQuestion, player.Level);
            ViewData["level"] = player.Level;

            if (category != "Crown")
            {
                this.triviaService.AddQuestionStat(gameInformation.Question.Id, int.Parse(tempGameId), player.Id);
                Session["currentQuestionId"] = gameInformation.Question.Id;
            }
            return View(gameInformation);
        }

        //Called immediately when the spinner has landed on a category. Passes the category in which the question will be from.
        [HttpPost]
        public ActionResult Question(string categoryName, int? gameId, string isCrownQuestion)
        {
            Session["category"] = categoryName.ToString();
            Session["gameId"] = gameId.ToString();
            Session["isCrownQuestion"] = isCrownQuestion;
            TempData["activeRequest"] = true;

            return Json(new { redirectToUrl = Url.Action("Question", "Trivia") }, JsonRequestBehavior.AllowGet);
        }

        //Retrieves the new game page where the user can select a random opponent or search for one.
        [ActionName("Create")]
        public ActionResult CreateNewGame()
        {
            var player = this.triviaService.LoggedInPlayer;
            ViewBag.UserNames = TriviaUsers.GetAllUserNames(player.Id);
            ViewBag.Player1UserName = player.Id;
            ViewData["level"] = player.Level;

            return View("NewGame");
        }

        //Starts a new game by creating a Game object and adding it to the database.
        [ActionName("Start")]
        public ActionResult StartNewGame()
        {
            var playerTwoUserName = TempData["FriendName"] == null ? "" : TempData["FriendName"] as string;

            var newGame = this.triviaService.CreateNewGame(playerTwoUserName);

            if (newGame.Player1.OverallScore == newGame.Player1.PrevScoreThreshold && newGame.Player1.Level != 0) leveledUp = true;

            ViewData["PlayerCategories"] = new PlayerCategories { LoggedInPlayerCategories = new CategoriesCollected(), OpponentCategories = new CategoriesCollected() };
            ViewData["leveledup"] = leveledUp;
            ViewData["level"] = newGame.Player1.Level;
            ViewData["streak"] = 0;
            ViewData["GameRound"] = newGame.Round;

            ViewBag.GameId = newGame.Id;

            return View("Spinner");
        }

        //Passes the name of the player's opponent to the GET StartNewGame method.
        [HttpPost]
        [ActionName("Start")]
        public ActionResult StartNewGame(string friendName)
        {
            TempData["FriendName"] = friendName;
            return Json(new { redirectToUrl = Url.Action("Start", "Trivia") });
        }

        //Called if the user gets a question right and clicks the next question button.
        [ActionName("Next")]
        public ActionResult ResumeGame()
        {
            int gameId = int.Parse(Session["gameToResume"].ToString());
            var player = this.triviaService.LoggedInPlayer;

            Game game = this.triviaService.FindGameById(gameId);

            PlayerCategories playerCategories = this.triviaService.GetBothPlayerCategories(gameId);

            if (game.PlayerTurn != player.Id)
            {
                return RedirectToAction("Index", "Home");
            }

            if (game.Round == 1 && game.PlayerTurn == game.Player1Id && this.triviaService.GetCategoryCount(game.Id, player.Id) == 3)
            {
                this.triviaService.SwitchTurns(game.Id);
                return RedirectToAction("Index", "Home");
            }

            if (player.OverallScore == player.PrevScoreThreshold && player.Level != 0)
            {
                leveledUp = true;
            }

            ViewData["leveledup"] = leveledUp;
            ViewData["level"] = player.Level;
            ViewBag.GameId = gameId;
            ViewData["gameId"] = gameId;
            ViewData["LoggedInPlayerHasCategory"] = this.triviaService.PlayerHasCategory(playerCategories.LoggedInPlayerCategories).ToString();
            ViewData["OpponentHasCategory"] = this.triviaService.PlayerHasCategory(playerCategories.OpponentCategories).ToString();
            ViewData["streak"] = this.triviaService.FindLoggedInPlayerStreak(gameId).PlayerStreak;
            ViewData["PlayerCategories"] = playerCategories;
            ViewData["GameRound"] = game.Round;

            return View("Spinner");
        }

        //Sends the gameId to the GET ResumeGame method
        [HttpPost]
        public ActionResult CorrectAnswer(int? gameId, string questionType)
        {
            Session["gameToResume"] = gameId;
            int gameID = int.Parse(Session["gameToResume"].ToString());
            var player = this.triviaService.LoggedInPlayer;
            this.triviaService.IncrementLoggedInPlayerStreak(gameID);

            ViewData["level"] = player.Level;
            
            int questionID = int.Parse(Session["currentQuestionId"].ToString());
            //check q as answered correctly
            this.triviaService.MarkQuestionAnswered(gameID, player.Id, questionID, questionType, true);

            this.triviaService.IncrementPlayerStats(player.Id);
            return new EmptyResult();
        }

        //Updates the categories collected for the current player
        [HttpPost]
        public ActionResult CollectCategory(int? gameId, string category, string questionType)
        {
            Session["gameToResume"] = gameId;
            int gameID = int.Parse(Session["gameToResume"].ToString());
            this.triviaService.AddCategoryForLoggedInPlayer(gameID, category);
            var player = this.triviaService.LoggedInPlayer;
            var questionID = int.Parse(Session["currentQuestionId"].ToString());
            this.triviaService.MarkQuestionAnswered(gameID, player.Id, questionID, questionType, true);

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult IncorrectAnswer(int? gameId, string questionType)
        {
            Session["gameToResume"] = gameId;
            int gameID = int.Parse(Session["gameToResume"].ToString());

            this.triviaService.CheckRoundRulesForIncorrectAnswer(gameID);

            ViewData["level"] = this.triviaService.LoggedInPlayer.Level;
            //TODO: check if working
            var player = this.triviaService.LoggedInPlayer;
            var questionID = int.Parse(Session["currentQuestionId"].ToString());
            this.triviaService.MarkQuestionAnswered(gameID, player.Id, questionID, questionType, false);
            return new EmptyResult();
        }

        //Called when the user selects the 'Resume' action link on the home page.
        public ActionResult Continue(int? gameId)
        {
            Session["gameToResume"] = gameId;
            var player = this.triviaService.LoggedInPlayer;

            if (player.OverallScore == player.PrevScoreThreshold) leveledUp = true;
            ViewData["leveledup"] = leveledUp;
            ViewData["level"] = player.Level;
            return RedirectToAction("Next");
        }

        [HttpPost]
        public ActionResult UpdateCount(string questionID, string countName)
        {
            this.triviaService.UpdateQuestionRating(questionID, countName);
            return new EmptyResult();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [HttpGet]
        public ActionResult SelectCategory()
        {
            var tempGameId = TempData["GameId"] == null ? "" : TempData["GameId"] as string;

            if (tempGameId.Equals(""))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["PlayerCategories"] = (PlayerCategories)Session["PlayerCategories"];
            ViewData["GameId"] = int.Parse(tempGameId.ToString());
            ViewData["QuestionType"] = Session["QuestionType"].ToString();
            ViewData["OpponentName"] = this.triviaService.GetOpponent(int.Parse(tempGameId.ToString())).UserName;

            TempData["GameId"] = "";

            return View();
        }

        [HttpPost]
        public ActionResult SelectCategory(int? gameId, string questionType)
        {
            TempData["GameId"] = gameId.ToString();
            int gameID = int.Parse(gameId.ToString());

            Session["PlayerCategories"] = this.triviaService.GetBothPlayerCategories(gameID);
            Session["QuestionType"] = questionType;

            if (questionType.Equals("Challenge"))
            {
                this.triviaService.ResetBothPlayerStreaks(gameID);
            }

            return Json(new { redirectToUrl = Url.Action("SelectCategory", "Trivia") }, JsonRequestBehavior.AllowGet);
        }

        #region Coins Methods

        //Called if the user wants to earn coins with the spinner.
        [ActionName("CoinSpinner")]
        public ActionResult EarnCoins()
        {
            int gameId = int.Parse(Session["gameToResume"].ToString());
            var player = this.triviaService.LoggedInPlayer;

            Game game = this.triviaService.FindGameById(gameId);

            if (game.PlayerTurn == player.Id)
            {
                return RedirectToAction("Index", "Home");
            }
            
            return View("CoinSpinner");
        }

        //Adds Coins to current user
        public ActionResult CollectCoins()
        {
            var coinsEarned = (int) TempData["coins"];
            var playerId = this.triviaService.LoggedInPlayer.Id;

            this.triviaService.AddCoinsToPlayer(playerId, coinsEarned);

            return RedirectToAction("Index", "Home");
        }

        //Called when the user clicks to collect coins earned
        [HttpPost]
        public ActionResult CollectCoins(int coinsEarned)
        {
            TempData["coins"] = coinsEarned;
            return Json(new { redirectToUrl = Url.Action("CollectCoins", "Trivia") }, JsonRequestBehavior.AllowGet);
        }

        //Called when the user clicks to buy a power up
        [HttpPost]
        public ActionResult RemoveCoins(int coinsToRemove)
        {
            var playerId = this.triviaService.LoggedInPlayer.Id;
            this.triviaService.RemoveCoinsFRomPlayer(playerId, coinsToRemove);

            return new EmptyResult();
        }

        #endregion

        #region Finished Game Score Methods

        //Called if the user wants to view the score for a finished game.
        [ActionName("GameScore")]
        public ActionResult Score()
        {
            int gameId = int.Parse(Session["gameToView"].ToString());

            var scoreInformation = this.triviaService.GetScoreInformation(gameId);

            return View(scoreInformation);
        }

        //Called when the user selects the 'Score' action link on the home page.
        public ActionResult Score(int? gameId)
        {
            Session["gameToView"] = gameId;

            return RedirectToAction("GameScore");
        }

        #endregion
    }
}