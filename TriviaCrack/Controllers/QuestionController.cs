﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using TriviaCrack.ViewModels;

namespace TriviaCrack.Controllers
{
    public class QuestionController : Controller
    {
        private TriviaContext db = new TriviaContext();

        public QuestionController()
        {}

        public QuestionController(TriviaContext context)
        {
            this.db = context;
        }

        // GET: Question
        public ActionResult Index(int? page)
        {
            return View(db.Questions.OrderBy(i => i.Text).ToPagedList(page ?? 1, 8));
        }

        // GET: Question/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = this.db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            
            return View(question);
        }

        // GET: Question/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(this.db.Categories, "Id", "Description");
            ViewBag.Answers = new SelectList(this.db.Answers, "Id", "Text");
            var difficultyList = new List<string> {"Easy", "Medium", "Hard"};
            ViewBag.Difficulty = new SelectList(difficultyList);
            NewQuestion view = new NewQuestion();

            return View(view);
        }

        // POST: Question/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewQuestion question)
        {
            if (ModelState.IsValid)
            {
                var answerChoice1Id = this.FindAnswerID(question.CorrectChoice);
                var answerChoice2Id = this.FindAnswerID(question.Choice2);
                var answerChoice3Id = this.FindAnswerID(question.Choice3);
                var answerChoice4Id = this.FindAnswerID(question.Choice4);

                int correctAnswerId = answerChoice1Id;
                var newQuestion = new Question
                {
                    Text = question.Text,
                    CorrectAnswerId = correctAnswerId,
                    AnswerChoice1Id = answerChoice1Id,
                    AnswerChoice2Id = answerChoice2Id,
                    AnswerChoice3Id = answerChoice3Id,
                    AnswerChoice4Id = answerChoice4Id,
                    CategoryId = question.CategoryId,
                    Difficulty = question.Difficulty
                };

                this.db.Questions.Add(newQuestion);
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(this.db.Categories, "Id", "Description");

            return View();
        }

        // GET: Question/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = this.db.Questions.Find(id);
            var NewQuestion = this.GetNewQuestion(question);

            if (question == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(this.db.Categories, "Id", "Description", question.CategoryId);
            var difficultyList = new List<string> { "Easy", "Medium", "Hard" };
            ViewBag.Difficulty = new SelectList(difficultyList, question.Difficulty);


            return View(NewQuestion);
        }

        // POST: Question/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Text,CorrectChoice,Choice2,Choice3,Choice4,CategoryId,Difficulty")] NewQuestion newQuestion)
        {
            if (ModelState.IsValid)
            {
                var answerChoice1Id = this.FindAnswerID(newQuestion.CorrectChoice);
                var answerChoice2Id = this.FindAnswerID(newQuestion.Choice2);
                var answerChoice3Id = this.FindAnswerID(newQuestion.Choice3);
                var answerChoice4Id = this.FindAnswerID(newQuestion.Choice4);
                int correctAnswerId = answerChoice1Id;

                var question = this.GetQuestion(newQuestion);
                question.AnswerChoice1Id = answerChoice1Id;
                question.AnswerChoice2Id = answerChoice2Id;
                question.AnswerChoice3Id = answerChoice3Id;
                question.AnswerChoice4Id = answerChoice4Id;
                question.CorrectAnswerId = correctAnswerId;
                question.CategoryId = newQuestion.CategoryId;
                question.Text = newQuestion.Text;
                question.Difficulty = newQuestion.Difficulty;

                this.db.Entry(question).State = EntityState.Modified;
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(this.db.Categories, "Id", "Description", newQuestion.CategoryId);

            return View(newQuestion);
        }

        // GET: Question/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = this.db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            
            return View(question);
        }

        // POST: Question/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Question question = this.db.Questions.Find(id);
            this.db.Questions.Remove(question);
            this.db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }
            base.Dispose(disposing);
        }

        private NewQuestion GetNewQuestion(Question question)
        {
            var newQuestion = new NewQuestion {
                Text = question.Text,
                CorrectChoice = question.CorrectAnswer.Text,
                Choice2 = question.AnswerChoice2.Text,
                Choice3 = question.AnswerChoice3.Text,
                Choice4 = question.AnswerChoice4.Text,
                CategoryId = question.CategoryId
            };

            return newQuestion;
        }

        private Question GetQuestion(NewQuestion newQuestion)
        {
            var question = (from q in this.db.Questions
                            where q.Text == newQuestion.Text
                            select q).Single();
            return question;
        }

        private int FindAnswerID(string answer)
        {
            if (answer == null)
            {
                throw new ArgumentException("Null answer entered");
            }
            var correctAnswerID = 0;

            for (int i = 1; i < this.db.Answers.Count() + 1; i++)
            {
                if (this.db.Answers.Find(i).Text == answer)
                {
                    correctAnswerID = i;
                }
            }

            if (correctAnswerID == 0)
            {
                var latestAnswer = new Answer { Id = this.db.Answers.Count() + 1, Text = answer };
                this.db.Answers.Add(latestAnswer);
                correctAnswerID = latestAnswer.Id;
                this.db.SaveChanges();
            }

            return correctAnswerID;
        }
    }
}
