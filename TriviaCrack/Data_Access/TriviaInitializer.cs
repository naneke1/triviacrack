﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using TriviaCrack.Models;

namespace TriviaCrack.Data_Access
{
    public class TriviaInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<TriviaContext>
    {
        protected override void Seed(TriviaContext context)
        {

            if (context.Categories.ToList().IsNullOrEmpty())
            {
                var categories = new List<Category>
                {
                    new Category { Description = "Art" },
                    new Category { Description = "Entertainment" },
                    new Category { Description = "Sports" },
                    new Category { Description = "Geography" },
                    new Category { Description = "History" },
                    new Category { Description = "Science" }
                };

                categories.ForEach(category => context.Categories.Add(category));
                context.SaveChanges();
            }

            if (context.Answers.ToList().IsNullOrEmpty())
            {
                var answers = new List<Answer>
                {
                    new Answer {Text = "Hatching"},
                    new Answer {Text = "Screentone"},
                    new Answer {Text = "Whittling"},
                    new Answer {Text = "Pholage"},
                    new Answer {Text = "Constable"},
                    new Answer {Text = "Caravaggio"},
                    new Answer {Text = "El Greco"},
                    new Answer {Text = "Van Gogh"},
                    new Answer {Text = "Acrylic"},
                    new Answer {Text = "Casein"},
                    new Answer {Text = "Gouache"},
                    new Answer {Text = "Watercolor"},
                    new Answer {Text = "Kites"},
                    new Answer {Text = "High Heels"},
                    new Answer {Text = "Gunpowder"},
                    new Answer {Text = "Wine cork"},
                    new Answer {Text = "Elton John"},
                    new Answer {Text = "Madonna"},
                    new Answer {Text = "Paul McCartney"},
                    new Answer {Text = "Robbie Williams"},
                    new Answer {Text = "Bob Marley"},
                    new Answer {Text = "Desmond Dekker"},
                    new Answer {Text = "Jimmy Cliff"},
                    new Answer {Text = "Johnny Nash"},
                    new Answer {Text = "You Sassy Thing"},
                    new Answer {Text = "You Saucy Thing"},
                    new Answer {Text = "You Sexy Thing"},
                    new Answer {Text = "You Sixty Thing"},
                    new Answer {Text = "ELO"},
                    new Answer {Text = "Slade"},
                    new Answer {Text = "The Moody Blues"},
                    new Answer {Text = "The Move"},
                    new Answer {Text = "Status Quo"},
                    new Answer {Text = "The Rolling Stones"},
                    new Answer {Text = "The Searchers"},
                    new Answer {Text = "The Who"},
                    new Answer {Text = "The Brightness"},
                    new Answer {Text = "The Darkness"},
                    new Answer {Text = "The Dead Of Night"},
                    new Answer {Text = "The Shade"},
                    new Answer {Text = "Buddy Holly"},
                    new Answer {Text = "Elvis Presley"},
                    new Answer {Text = "Jimi Hendrix"},
                    new Answer {Text = "Jim Morrison"},
                    new Answer {Text = "Boyzone"},
                    new Answer {Text = "The Bachelors"},
                    new Answer {Text = "The Clancy Brothers"},
                    new Answer {Text = "The Dubliners"},
                    new Answer {Text = "Johnny Foul"},
                    new Answer {Text = "Johnny Horrible"},
                    new Answer {Text = "Johnny Nasty"},
                    new Answer {Text = "Johnny Rotten"},
                    new Answer {Text = "The Answer"},
                    new Answer {Text = "The Cure"},
                    new Answer {Text = "The Remedy"},
                    new Answer {Text = "The Solution"},
                    new Answer {Text = "2009"},
                    new Answer {Text = "2010"},
                    new Answer {Text = "2011"},
                    new Answer {Text = "2012"},
                    new Answer {Text = "2013"},
                    new Answer {Text = "2014"},
                    new Answer {Text = "2015"},
                    new Answer {Text = "2016"},
                    new Answer {Text = "2017"},
                    new Answer {Text = "2018"},
                    new Answer {Text = "2019"},
                    new Answer {Text = "2020"},
                    new Answer {Text = "2021"},
                    new Answer {Text = "2022"},
                    new Answer {Text = "2023"},
                    new Answer {Text = "2024"},
                    new Answer {Text = "2025"},
                    new Answer {Text = "2026"},
                    new Answer {Text = "2027"},
                    new Answer {Text = "2028"},
                    new Answer {Text = "Eagles"},
                    new Answer {Text = "Patriots"},
                    new Answer {Text = "Falcons"},
                    new Answer {Text = "Bears"},
                    new Answer {Text = "3"},
                    new Answer {Text = "1"},
                    new Answer {Text = "5"},
                    new Answer {Text = "2"},
                    new Answer {Text = "7"},
                    new Answer {Text = "4"},
                    new Answer {Text = "6"},
                    new Answer {Text = "8"},
                    new Answer {Text = "0"},
                    new Answer {Text = "9"},
                    new Answer {Text = "Rafael Nadal"},
                    new Answer {Text = "Andy Murray"},
                    new Answer {Text = "Roger Federer"},
                    new Answer {Text = "Pete Sampras"},
                    new Answer {Text = "17"},
                    new Answer {Text = "18"},
                    new Answer {Text = "19"},
                    new Answer {Text = "20"},
                    new Answer {Text = "Rome, Italy"},
                    new Answer {Text = "London, England"},
                    new Answer {Text = "Shanghai, China"},
                    new Answer {Text = "Austin, Texas"},
                    new Answer {Text = "Hudson"},
                    new Answer {Text = "Mississippi"},
                    new Answer {Text = "Ohio"},
                    new Answer {Text = "Potomac"},
                    new Answer {Text = "Manitoba"},
                    new Answer {Text = "Nova Scotia"},
                    new Answer {Text = "Ontario"},
                    new Answer {Text = "Quebec"},
                    new Answer {Text = "Alaska"},
                    new Answer {Text = "Maine"},
                    new Answer {Text = "Montana"},
                    new Answer {Text = "Oregon"},
                    new Answer {Text = "Charlotte"},
                    new Answer {Text = "Chicago"},
                    new Answer {Text = "Cincinnati"},
                    new Answer {Text = "Cleveland"},
                    new Answer {Text = "Florida"},
                    new Answer {Text = "Georgia"},
                    new Answer {Text = "South Carolina"},
                    new Answer {Text = "Texas"},
                    new Answer {Text = "July 4, 1776"},
                    new Answer {Text = "July 4, 1786"},
                    new Answer {Text = "July 4, 1876"},
                    new Answer {Text = "July 4, 1767"},
                    new Answer {Text = "Alaska Purchase"},
                    new Answer {Text = "Louisiana Purchase"},
                    new Answer {Text = "Florida Purchase"},
                    new Answer {Text = "Georgia Purchase"},
                    new Answer {Text = "Roulette"},
                    new Answer {Text = "Baseball"},
                    new Answer {Text = "Bowling"},
                    new Answer {Text = "Tennis"},
                    new Answer {Text = "Salem"},
                    new Answer {Text = "Jamestown"},
                    new Answer {Text = "Mass Bay"},
                    new Answer {Text = "St. Augustine"},
                    new Answer {Text = "10,000 years"},
                    new Answer {Text = "600,000 years"},
                    new Answer {Text = "6 million years"},
                    new Answer {Text = "60 million years"},
                    new Answer {Text = "Shoulder blade"},
                    new Answer {Text = "Knee cap"},
                    new Answer {Text = "Femur"},
                    new Answer {Text = "Lower jaw bone"},
                    new Answer {Text = "China"},
                    new Answer {Text = "France"},
                    new Answer {Text = "Japan"},
                    new Answer {Text = "Kangaroo"},
                    new Answer {Text = "Koala"},
                    new Answer {Text = "Platypus"},
                    new Answer {Text = "Tasmanian Devil"},
                    new Answer {Text = "The Bill of Rights"},
                    new Answer {Text = "The US Constitution"},
                    new Answer {Text = "The Declaration of Independence"},
                    new Answer {Text = "The Gettysburg Address"},
                    new Answer {Text = "All in the name of logging in"},
                    new Answer {Text = "Archery"},
                    new Answer {Text = "Shot Put"},
                    new Answer {Text = "Polo"},
                    new Answer {Text = "11"},
                    new Answer {Text = "Summer & Winter"},
                    new Answer {Text = "Summer & Fall"},
                    new Answer {Text = "Spring & Fall"},
                    new Answer {Text = "Spring & Winter"},
                    new Answer {Text = "14th"},
                    new Answer {Text = "15th"},
                    new Answer {Text = "16th"},
                    new Answer {Text = "17th"},
                    new Answer {Text = "Shape"},
                    new Answer {Text = "Line"},
                    new Answer {Text = "Space"},
                    new Answer {Text = "Form"},
                    new Answer {Text = "Himalayas"},
                    new Answer {Text = "Rockies"},
                    new Answer {Text = "Andes"},
                    new Answer {Text = "Alps"},
                    new Answer {Text = "Lake Kilimanjaro"},
                    new Answer {Text = "Lake Atlas"},
                    new Answer {Text = "Lake Victoria"},
                    new Answer {Text = "Lake Sahel"},
                    new Answer {Text = "Andrew Johnson"},
                    new Answer {Text = "James Buchanan"},
                    new Answer {Text = "Ulysses S. Grant"},
                    new Answer {Text = "Rutherford B. Hayes"},
                    new Answer {Text = "Harvard"},
                    new Answer {Text = "William and Mary"},
                    new Answer {Text = "Yale"},
                    new Answer {Text = "Moravian"},
                    new Answer {Text = "Graphite"},
                    new Answer {Text = "Silicon"},
                    new Answer {Text = "Charcoal"},
                    new Answer {Text = "Phosphorous"},
                    new Answer {Text = "Nitrogen"},
                    new Answer {Text = "Hydrogen"},
                    new Answer {Text = "Carbon Dioxide"},
                    new Answer {Text = "Oxygen"},
                    new Answer {Text = "Methane"},
                    new Answer {Text = "Nitrous Oxide"}
                    //new Answer {Text = ""}
                };

                answers.ForEach(answer => context.Answers.Add(answer));
                context.SaveChanges();
            }

            if (context.Questions.ToList().IsNullOrEmpty())
            {

                var questions = new List<Question>
                {
                    #region Art Questions

                    new Question
                    {
                        Text = "How many paintings did Vincent Van Gogh sell in his lifetime?",
                        Category = context.Categories.Find(1),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "1"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "1"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "3"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "5"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "7"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Name the technique that places lines closely side by side?",
                        Category = context.Categories.Find(1),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Hatching"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Hatching"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Screentone"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Whittling"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Pholage"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Who painted the Conversion of St. Paul?",
                        Category = context.Categories.Find(1),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Caravaggio"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Constable"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Caravaggio"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "El Greco"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Van Gogh"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Which types of paint does NOT use the color white?",
                        Category = context.Categories.Find(1),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Watercolor"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Casein"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Watercolor"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Gouache"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Acrylic"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "Leonardi Da Vinci invented which one of these items?",
                        Category = context.Categories.Find(1),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "High Heels"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Kites"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "High Heels"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Gunpowder"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Wine cork"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "The use of canvas as a primary painting surface came of age in which century?",
                        Category = context.Categories.Find(1),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "16th"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "14th"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "15th"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "16th"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "17th"),
                        Difficulty = "Hard"
                    },

                    new Question
                    {
                        Text = "When lines meet to form an enclosed area this is formed",
                        Category = context.Categories.Find(1),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Shape"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Shape"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Line"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Space"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Form"),
                        Difficulty = "Hard"
                    },

                    #endregion

                    #region Music Questions

                    new Question
                    {
                        Text = "Which reggae music legend was backed by The Wailers?",
                        Category = context.Categories.Find(2),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Bob Marley"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Bob Marley"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Desmond Dekker"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Jimmy Cliff"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Johnny Nash"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "With which song did Hot Chocolate have a major pop hit in November 1975?",
                        Category = context.Categories.Find(2),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "You Sexy Thing"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "You Sassy Thing"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "You Saucy Thing"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "You Sexy Thing"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "You Sixty Thing"),
                        Difficulty = "Easy"
                    },
                    new Question
                    {
                        Text = "Which pop group from the West Midlands is best known for a Christmas-themed single?",
                        Category = context.Categories.Find(2),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Slade"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "ELO"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Slade"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "The Moody Blues"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "The Move"),
                        Difficulty = "Easy"
                    },
                    new Question
                    {
                        Text = "Which enduring pop band memorably recorded 'Satisfaction' in 1965?",
                        Category = context.Categories.Find(2),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "The Rolling Stones"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Status Quo"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "The Rolling Stones"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "The Searchers"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "The Who"),
                        Difficulty = "Medium"
                    },
                    new Question
                    {
                        Text = "Which pop group, led by Justin Hawkins, originated in Lowestoft?",
                        Category = context.Categories.Find(2),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "The Darkness"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "The Brightness"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "The Dead Of Night"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "The Shade"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "The Darkness"),
                        Difficulty = "Medium"
                    },
                    new Question
                    {
                        Text = "Which pop music legend is respectfully remembered as 'The King'?",
                        Category = context.Categories.Find(2),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Elvis Presley"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Buddy Holly"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Elvis Presley"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Jimi Hendrix"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Jim Morrison"),
                        Difficulty = "Hard"
                    },
                    new Question
                    {
                        Text = "Pop singer Ronan Keating first shot to fame as part of which Irish boy band?",
                        Category = context.Categories.Find(2),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Boyzone"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Boyzone"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "The Bachelors"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "The Clancy Brothers"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "The Dubliners"),
                        Difficulty = "Hard"
                    },
                    new Question
                    {
                        Text = "How was punk singer John Lydon of The Sex Pistols known?",
                        Category = context.Categories.Find(2),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Johnny Rotten"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Johnny Foul"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Johnny Horrible"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Johnny Nasty"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Johnny Rotten"),
                        Difficulty = "Medium"
                    },
                    new Question
                    {
                        Text = "Pop singer, guitarist and songwriter Robert Smith founded which group?",
                        Category = context.Categories.Find(2),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "The Cure"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "The Answer"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "The Cure"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "The Remedy"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "The Solution"),
                        Difficulty = "Hard"
                    },

                    #endregion

                    #region Sports Questions

                    new Question
                    {
                        Text = "Who won the 2018 Super Bowl?",
                        Category = context.Categories.Find(3),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Eagles"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Patriots"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Eagles"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Falcons"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Bears"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "How many points is a field goal worth in football?",
                        Category = context.Categories.Find(3),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "3"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "3"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "1"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "2"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "7"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Which male tennis player has won the most Grand Slam titles?",
                        Category = context.Categories.Find(3),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Roger Federer"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Rafael Nadal"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Andy Murray"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Roger Federer"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Pete Sampras"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "How many holes are there in a full round of golf?",
                        Category = context.Categories.Find(3),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "18"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "17"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "18"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "19"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "20"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Which city hosted the 2012 Summer Olympics?",
                        Category = context.Categories.Find(3),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "London, England"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Rome, Italy"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Shanghai, China"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Austin, Texas"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "London, England"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "What sport uses a bow and arrows?",
                        Category = context.Categories.Find(3),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Archery"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Bowling"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Shot Put"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Archery"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Polo"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "How many teams were originally in the NBA?",
                        Category = context.Categories.Find(3),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "11"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "20"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "18"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "17"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "11"),
                        Difficulty = "Hard"
                    },

                    new Question
                    {
                        Text = "The Olympic Games take place in which two seasons?",
                        Category = context.Categories.Find(3),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Summer & Winter"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Summer & Winter"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Summer & Fall"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Spring & Fall"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Spring & Winter"),
                        Difficulty = "Hard"
                    },

                    #endregion

                    #region Geography Questions

                    new Question
                    {
                        Text = "Upon which river does Washington DC stand?",
                        Category = context.Categories.Find(4),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Potomac"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Hudson"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Mississippi"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Ohio"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Potomac"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "In which Canadian province is Montreal located?",
                        Category = context.Categories.Find(4),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Quebec"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Manitoba"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Nova Scotia"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Ontario"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Quebec"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Which of the United States is situated north of 50 degrees latitude?",
                        Category = context.Categories.Find(4),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Alaska"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Alaska"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Maine"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Montana"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Oregon"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Which Illinois city has the third largest population in the USA?",
                        Category = context.Categories.Find(4),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Chicago"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Charlotte"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Chicago"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Cincinnati"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Cleveland"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "In which American state is the Everglades National Park?",
                        Category = context.Categories.Find(4),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Florida"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Georgia"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "South Carolina"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Texas"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Florida"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "What is the largest lake in Africa?",
                        Category = context.Categories.Find(4),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Lake Victoria"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Lake Atlas"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Lake Victoria"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Lake Victoria"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Lake Sahel"),
                        Difficulty = "Hard"
                    },

                    new Question
                    {
                        Text = "Which of the following form a mountain range in Asia that seperates the Indian subcontinent from the Tibetan Plateau?",
                        Category = context.Categories.Find(4),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Himalayas"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Rockies"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Himalayas"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Andes"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Alps"),
                        Difficulty = "Hard"
                    },

                    #endregion

                    #region History Questions

                    new Question
                    {
                        Text = "What was the date of the signing of the Declaration of Independence",
                        Category = context.Categories.Find(5),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "July 4, 1776"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "July 4, 1776"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "July 4, 1786"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "July 4, 1876"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "July 4, 1767"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Thomas Jefferson negotiated what purchase from France in 1803?",
                        Category = context.Categories.Find(5),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Louisiana Purchase"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Alaska Purchase"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Louisiana Purchase"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Florida Purchase"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Georgia Purchase"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Which game was created by French mathematician Blaise Pascal?",
                        Category = context.Categories.Find(5),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Roulette"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Baseball"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Roulette"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Bowling"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Tennis"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "What was the first permanent English Settlement in the United States?",
                        Category = context.Categories.Find(5),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Jamestown"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Salem"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Jamestown"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Mass Bay"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "St. Augustine"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "Which famous document begins with 'When in the course of human events...'?",
                        Category = context.Categories.Find(5),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "The Declaration of Independence"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "The Bill of Rights"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "The US Constitution"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "The Declaration of Independence"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "The Gettysburg Address"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "Who became president after the assassination of Abraham Lincoln?",
                        Category = context.Categories.Find(5),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Andrew Johnson"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Andrew Johnson"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Ulysses S. Grant"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "James Buchanan"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Rutherford B. Hayes"),
                        Difficulty = "Hard"
                    },

                    new Question
                    {
                        Text = "Which is the oldest university in the USA?",
                        Category = context.Categories.Find(5),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Harvard"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Moravian"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "William and Mary"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Yale"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Harvard"),
                        Difficulty = "Hard"
                    },

                    #endregion

                    #region Science Questions

                    new Question
                    {
                        Text = "How long ago did dinosaurs become extinct?",
                        Category = context.Categories.Find(6),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "60 million years"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "10,000 years"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "600,000 years"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "6 million years"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "60 million years"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "What is the scapula is more commonly called?",
                        Category = context.Categories.Find(6),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Shoulder blade"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Shoulder blade"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Knee cap"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Femur"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Lower jaw bone"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "Which of these animals lays eggs?",
                        Category = context.Categories.Find(6),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Platypus"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Kangaroo"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Koala"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Platypus"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Tasmanian Devil"),
                        Difficulty = "Easy"
                    },

                    new Question
                    {
                        Text = "How many of the 8 planets other than Earth are smaller than Earth?",
                        Category = context.Categories.Find(6),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "4"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "0"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "2"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "4"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "6"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "Which of the following is used in pencils?",
                        Category = context.Categories.Find(6),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Graphite"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Silicon"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Graphite"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Charcoal"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Phosphorous"),
                        Difficulty = "Medium"
                    },

                    new Question
                    {
                        Text = "The gas usually filled in the electric bulb is?",
                        Category = context.Categories.Find(6),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Nitrogen"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Hydrogen"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Carbon Dioxide"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Nitrogen"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Oxygen"),
                        Difficulty = "Hard"
                    },

                    new Question
                    {
                        Text = "Which of these is not known as a greenhouse gas?",
                        Category = context.Categories.Find(6),
                        CorrectAnswer = context.Answers.Single(a => a.Text == "Hydrogen"),
                        AnswerChoice1 = context.Answers.Single(a => a.Text == "Methane"),
                        AnswerChoice2 = context.Answers.Single(a => a.Text == "Nitrous Oxide"),
                        AnswerChoice3 = context.Answers.Single(a => a.Text == "Carbon Dioxide"),
                        AnswerChoice4 = context.Answers.Single(a => a.Text == "Hydrogen"),
                        Difficulty = "Hard"
                    }
        
                    #endregion
                };

                questions.ForEach(question => context.Questions.Add(question));
                context.SaveChanges();


            }

            if (context.Players.ToList().IsNullOrEmpty())
            {
                var players = new List<Player> {
                    new Player { Id = "a1", UserName = "admin@triviacrack.com" },
                    new Player { Id = "a2", UserName = "admin2@triviacrack.com" }
                };
                players.ForEach(player => player.AddCoins(500));
                players.ForEach(player => context.Players.Add(player));
                context.SaveChanges();
            }

            if (context.Games.ToList().IsNullOrEmpty())
            {
                var games = new List<Game>
                {
                    new Game {
                        Player1 = context.Players.Single(p => p.Id == "a1"),
                        Player2 = context.Players.Single(p => p.Id == "a2"),
                        CurrentPlayer = context.Players.Single(p => p.Id == "a1"),
                        StartDate = new DateTime(2018, 1, 1, 12, 30, 0),
                        EndDate = new DateTime(2018, 1, 1, 3, 0, 0),
                        IsActive = false,
                        Winner = context.Players.Single(p => p.Id == "a1")
                    },

                    new Game {
                        Player1 = context.Players.Single(p => p.Id == "a1"),
                        Player2 = context.Players.Single(p => p.Id == "a2"),
                        CurrentPlayer = context.Players.Single(p => p.Id == "a2"),
                        StartDate = new DateTime(2018, 3, 25, 4, 0, 0),
                        EndDate = new DateTime(2018, 3, 27, 5, 0, 0),
                        IsActive = false,
                        Winner = context.Players.Single(p => p.Id == "a2")
                    },
                    new Game {
                        Player1 = context.Players.Single(p => p.Id == "a1"),
                        Player2 = context.Players.Single(p => p.Id == "a2"),
                        CurrentPlayer = context.Players.Single(p => p.Id == "a1"),
                        StartDate = new DateTime(2018, 1, 1, 12, 30, 0),
                        EndDate = new DateTime(2018, 1, 1, 3, 0, 0),
                        IsActive = true,
                        PlayerTurn = "a1",
                        Round = 1
                    }
                };

                games.ForEach(game => context.Games.Add(game));
                context.SaveChanges();
            }

            if (context.CategoriesCollected.ToList().IsNullOrEmpty())
            {
                var categoriesCollected = new List<CategoriesCollected>
                {
                    new CategoriesCollected
                    {
                        GameId = 3,
                        PlayerId = "a1",
                        ArtCollected = true,
                        EntertainmentCollected = true,
                        SportsCollected = false,
                        GeographyCollected = false,
                        HistoryCollected = false,
                        ScienceCollected = true
                    },

                    new CategoriesCollected
                    {
                        GameId = 3,
                        PlayerId = "a2",
                        ArtCollected = false,
                        EntertainmentCollected = false,
                        SportsCollected = true,
                        GeographyCollected = false,
                        HistoryCollected = true,
                        ScienceCollected = true
                    }
                };
                categoriesCollected.ForEach(cc => context.CategoriesCollected.Add(cc));
                context.SaveChanges();
            }

            if (context.Streaks.ToList().IsNullOrEmpty())
            {
                var streaks = new List<Streak>
                {
                    new Streak
                    {
                        GameId = 3,
                        PlayerId = "a1",
                        PlayerStreak = 2
                    },
                    new Streak
                    {
                        GameId = 3,
                        PlayerId = "a2",
                        PlayerStreak = 0
                    }
                };

                streaks.ForEach(streak => context.Streaks.Add(streak));
                context.SaveChanges();
            }

            if (context.Statistics.ToList().IsNullOrEmpty())
            {
                var stats = new List<Statistics>
                {
                    new Statistics {
                        Game = context.Games.Find(1),
                        Player = context.Players.Single(p => p.Id == "a1"),
                        Question = context.Questions.Find(1),
                        IsAnsweredCorrectly = false,
                        DateAnswered = new DateTime(2018, 1, 1, 12, 40, 0),
                        IsChallengeQuestion = false,
                        IsCrownQuestion = false
                    },

                    new Statistics {
                        Game = context.Games.Find(2),
                        Player = context.Players.Single(p => p.Id == "a2"),
                        Question = context.Questions.Find(2),
                        IsAnsweredCorrectly = true,
                        DateAnswered = new DateTime(2018, 3, 26, 4, 10, 0),
                        IsChallengeQuestion = false,
                        IsCrownQuestion = true
                    },

                    new Statistics {
                        Game = context.Games.Find(2),
                        Player = context.Players.Single(p => p.Id == "a2"),
                        Question = context.Questions.Find(3),
                        IsAnsweredCorrectly = true,
                        DateAnswered = new DateTime(2018, 3, 26, 4, 12, 0),
                        IsChallengeQuestion = false,
                        IsCrownQuestion = false
                    }
                };

                stats.ForEach(stat => context.Statistics.Add(stat));
                context.SaveChanges();


            }

            if (context.PowerUps.ToList().IsNullOrEmpty())
            {
                var powerUps = new List<PowerUp>
                {
                    new PowerUp { Name = "Extra Time", Cost = 1 },
                    new PowerUp { Name = "Bomb", Cost =  5 },
                    new PowerUp { Name = "Auto Correct", Cost = 250 }
                };

                powerUps.ForEach(power => context.PowerUps.Add(power));
                context.SaveChanges();
            }

            if (context.AchievementStatus.ToList().IsNullOrEmpty())
            {
                var statusList = new List<AchievementStatus>
                {
                    new AchievementStatus { PlayerId = "a1" },
                    new AchievementStatus { PlayerId = "a2" }
                };

                statusList.ForEach(status => context.AchievementStatus.Add(status));
                context.SaveChanges();
            }

            if (context.Achievements.ToList().IsNullOrEmpty())
            {
                var achievementList = new List<Achievement>
                {
                    new Achievement { Id = 1, Name = "Art Connoisseur", Description = "Answer 10 Art Questions Correctly"},
                    new Achievement { Id = 2, Name = "Brainiac", Description = "Answer 25 Questions Correctly" },
                    new Achievement { Id = 3, Name = "Champion", Description = "Win 10 Games"},
                    new Achievement { Id = 4, Name = "History Buff", Description = "Answer 10 History Questions Correctly" },
                    new Achievement { Id = 5, Name = "Map Scholar", Description = "Answer 10 Geography Questions Correctly"},
                    new Achievement { Id = 6, Name = "Moneybags", Description = "Collect 1000 Coins" },
                    new Achievement { Id = 7, Name = "Pop Culture Whiz", Description = "Answer 10 Entertainment Questions Correctly"},
                    new Achievement { Id = 8, Name = "Quiz Master", Description = "Answer 5 Consecutive Questions Correctly" },
                    new Achievement { Id = 9, Name = "Science Nerd", Description = "Answer 10 Science Questions Correctly"},
                    new Achievement { Id = 10, Name = "Sports Nut", Description = "Answer 10 Sports Questions Correctly" }
                };

                achievementList.ForEach(achievement => context.Achievements.Add(achievement));
                context.SaveChanges();
            }

            base.Seed(context);
        }
    }
}