﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TriviaCrack.Models;

namespace TriviaCrack.Data_Access
{
    public class TriviaContext : DbContext
    {
        public TriviaContext() : base("TriviaContext")
        {
            Database.SetInitializer(new TriviaInitializer());
        }

        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Answer> Answers { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<SubmittedQuestion> SubmittedQuestions { get; set; }
        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<CategoriesCollected> CategoriesCollected { get; set; }
        public virtual DbSet<Streak> Streaks { get; set; }
        public virtual DbSet<ClassicChallengeQuestion> ClassicChallengeQuestions { get; set; }
        public virtual DbSet<Statistics> Statistics { get; set; }
        public virtual DbSet<PowerUp> PowerUps { get; set; }
        public virtual DbSet<AchievementStatus> AchievementStatus { get; set; }
        public virtual DbSet<Achievement> Achievements { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}