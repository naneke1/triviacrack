﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TriviaCrack.Models;

namespace TriviaCrack.ViewModels
{
    public class PlayerGames
    {
        public Player LoggedInPlayer { get; set; }
        public IEnumerable<Game> ActiveGames { get; set; }
        public IEnumerable<Game> FinishedGames { get; set; }
    }
}