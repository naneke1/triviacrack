﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TriviaCrack.Models;

namespace TriviaCrack.ViewModels
{
    public class GameInformation
    {
        public Question Question { get; set; }
        public Game Game { get; set; }
        public Player Player { get; set; }
        public PlayerCategories PlayerCategoriesCollected { get; set; }
        public int PlayerStreak { get; set; }
        public String PlayerHasCategory { get; set; }
        public String OpponentHasCategoryToSteal { get; set; }
        public String IsCrownQuestion { get; set; }
        public int CategoryCount { get; set; }
        public PowerUp ExtraTime { get; set; }
        public PowerUp Bomb { get; set; }
        public PowerUp AutoCorrect { get; set; }
    }
}