﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TriviaCrack.Models;

namespace TriviaCrack.ViewModels
{
    public class PlayerCategories
    {
        public CategoriesCollected LoggedInPlayerCategories { get; set; }
        public CategoriesCollected OpponentCategories { get; set; }
    }
}