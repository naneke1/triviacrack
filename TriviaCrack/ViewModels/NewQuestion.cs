﻿using TriviaCrack.Models;

namespace TriviaCrack.ViewModels
{
    public class NewQuestion
    {
        public string Text { get; set; }
        public string CorrectChoice { get; set; }
        public string Choice2 { get; set; }
        public string Choice3 { get; set; }
        public string Choice4 { get; set; }
        public int CategoryId { get; set; }
        public string Difficulty { get; set; }
    }
}