﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TriviaCrack.ViewModels
{
    public class ScoreInformation
    {
        public int CategoriesCountOpponent { get; set; }
        public int CategoriesCountLoggedIn { get; set; }

        public int GeographyStatsOpponent { get; set; }
        public int GeographyStatsLoggedIn { get; set; }
        public int ArtStatsOpponent { get; set; }
        public int ArtStatsLoggedIn { get; set; }
        public int EntertainmentStatsOpponent { get; set; }
        public int EntertainmentStatsLoggedIn { get; set; }
        public int HistoryStatsOpponent { get; set; }
        public int HistoryStatsLoggedIn { get; set; }
        public int ScienceStatsOpponent { get; set; }
        public int ScienceStatsLoggedIn { get; set; }
        public int SportsStatsOpponent { get; set; }
        public int SportsStatsLoggedIn { get; set; }

        public int ChallengeStatsOpponent { get; set; }
        public int ChallengeStatsLoggedIn { get; set; }
        public int CrownStatsOpponent { get; set; }
        public int CrownStatsLoggedIn { get; set; }
        public int CorrectStatsOpponent { get; set; }
        public int CorrectStatsLoggedIn { get; set; }
        public int IncorrectStatsOpponent { get; set; }
        public int IncorrectStatsLoggedIn { get; set; }
        public int AnsweredStatsOpponent { get; set; }
        public int AnsweredStatsLoggedIn { get; set; }
    }
}