﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TriviaCrack.Models
{
    public class SubmittedQuestion
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Question")]
        public string Text { get; set; }
        [Required]
        [Display(Name = "Correct Answer")]
        public string CorrectAnswer { get; set; }
        [Required]
        [Display(Name = "Incorrect Answer 1")]
        public string IncorrectAnswer1 { get; set; }
        [Required]
        [Display(Name = "Incorrect Answer 2")]
        public string IncorrectAnswer2 { get; set; }
        [Required]
        [Display(Name = "Incorrect Answer 3")]
        public string IncorrectAnswer3 { get; set; }
        [Required]
        [Display(Name = "Category")]
        public string CategoryName { get; set; }
    }
}