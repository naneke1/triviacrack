﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace TriviaCrack.Models
{
    public class Game
    {
        public int Id { get; set; }

        [ForeignKey("Player1")]
        public string Player1Id { get; set; }
        public virtual Player Player1 { get; set; }

        [ForeignKey("Player2")]
        public string Player2Id { get; set; }
        public virtual Player Player2 { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public bool IsActive { get; set; }

        [ForeignKey("Winner")]
        public string WinnerId { get; set; }
        public virtual Player Winner { get; set; }

        [ForeignKey("CurrentPlayer")]
        public string PlayerTurn { get; set; }
        public virtual Player CurrentPlayer { get; set; }

        public bool ChallengeInProgress { get; set; }

        public int Round { get; set; }

        [DefaultValue(false)]
        public bool DidResign { get; set; }
    }
}