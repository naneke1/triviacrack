﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TriviaCrack.Models
{
    public class ClassicChallengeQuestion
    {
        [Key, Column(Order = 0)]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }

        [Key, Column(Order = 1)]
        public string PlayerId { get; set; }
        public virtual Player Player { get; set; }

        [ForeignKey("Question1")]
        public int? Question1Id { get; set; }
        public virtual Question Question1 { get; set; }

        [ForeignKey("Question2")]
        public int? Question2Id { get; set; }
        public virtual Question Question2 { get; set; }

        [ForeignKey("Question3")]
        public int? Question3Id { get; set; }
        public virtual Question Question3 { get; set; }

        [ForeignKey("Question4")]
        public int? Question4Id { get; set; }
        public virtual Question Question4 { get; set; }

        [ForeignKey("Question5")]
        public int? Question5Id { get; set; }
        public virtual Question Question5 { get; set; }

        [ForeignKey("Question6")]
        public int? Question6Id { get; set; }
        public virtual Question Question6 { get; set; }

        [ForeignKey("TiebreakerQuestion")]
        public int? TiebreakerQuestionId { get; set; }
        public virtual Question TiebreakerQuestion { get; set; }

        public int TotalCorrect { get; set; }
        public int CurrentQuestion { get; set; }

        public string CategoryAtStake { get; set; }
    }
}