﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TriviaCrack.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string Text { get; set; }

        [ForeignKey("CorrectAnswer")]
        public int? CorrectAnswerId { get; set; }
        public virtual Answer CorrectAnswer { get; set; }

        [ForeignKey("AnswerChoice1")]
        public int? AnswerChoice1Id { get; set; }
        public virtual Answer AnswerChoice1 { get; set; }

        [ForeignKey("AnswerChoice2")]
        public int? AnswerChoice2Id { get; set; }
        public virtual Answer AnswerChoice2 { get; set; }

        [ForeignKey("AnswerChoice3")]
        public int? AnswerChoice3Id { get; set; }
        public virtual Answer AnswerChoice3 { get; set; }

        [ForeignKey("AnswerChoice4")]
        public int? AnswerChoice4Id { get; set; }
        public virtual Answer AnswerChoice4 { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        [DefaultValue(0)]
        public int FunRatingCount { get; set; }
        [DefaultValue(0)]
        public int BoringRatingCount { get; set; }

        public string Difficulty { get; set; }
    }
}