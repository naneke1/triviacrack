﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TriviaCrack.Models
{
    public class AchievementStatus
    {
        [Key]
        [ForeignKey("Player")]
        public string PlayerId { get; set; }
        public virtual Player Player { get; set; }

        [DefaultValue(false)]
        public bool ArtConnoisseurAchieved { get; set; }
        [DefaultValue(false)]
        public bool BrainiacAchieved { get; set; }
        [DefaultValue(false)]
        public bool ChampionAchieved { get; set; }
        [DefaultValue(false)]
        public bool HistoryBuffAchieved { get; set; }
        [DefaultValue(false)]
        public bool MapScholarAchieved { get; set; }
        [DefaultValue(false)]
        public bool MoneybagsAchieved { get; set; }
        [DefaultValue(false)]
        public bool PopCultureWhizAchieved { get; set; }
        [DefaultValue(false)]
        public bool QuizMasterAchieved { get; set; }
        [DefaultValue(false)]
        public bool ScienceNerdAchieved { get; set; }
        [DefaultValue(false)]
        public bool SportsNutAchieved { get; set; }
    }
}