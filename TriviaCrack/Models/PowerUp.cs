﻿using System.ComponentModel.DataAnnotations;

namespace TriviaCrack.Models
{
    public class PowerUp
    {
        [Key]
        public string Name { get; set; }

        public int Cost { get; set; }
    }
}