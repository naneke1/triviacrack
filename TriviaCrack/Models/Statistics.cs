﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TriviaCrack.Models
{
    public class Statistics
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey("Game")]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("Player")]
        public string PlayerId { get; set; }
        public virtual Player Player { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("Question")]
        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }

        public bool IsAnsweredCorrectly { get; set; }
        public DateTime DateAnswered { get; set; }

        public bool IsCrownQuestion { get; set; }

        public bool IsChallengeQuestion { get; set; }
    }
}