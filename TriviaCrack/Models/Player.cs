﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TriviaCrack.Models
{
    public class Player
    {
        [Key]
        public string Id { get; set; }

        public string UserName { get; set; }

        [DefaultValue(0)]
        public int OverallScore { get; set; }

        [DefaultValue(1)]       //Must be set in AccountController methods CreateAdminIfNeeded & Register as well as TriviaInitializer
        public int Level { get; set; }

        [DefaultValue(0)]
        public int PrevScoreThreshold { get; set; }

        [DefaultValue(500)]     //Must be set in AccountController methods CreateAdminIfNeeded & Register as well as TriviaInitializer
        public int Coins { get; private set; }

        [DefaultValue(0)]
        public int ChallengeWins { get; set; }

        [DefaultValue(0)]
        public int ChallengeCount { get; set; }

        public void RemoveCoins(int amount)
        {
            if (this.Coins - amount >= 0)
            {
                this.Coins -= amount;
            }
        }

        public void AddCoins(int amount)
        {
            if (amount >= 0)
            {
                this.Coins += amount;
            }
        }
    }
}