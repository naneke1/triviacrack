﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TriviaCrack.Models
{
    public class CategoriesCollected
    {
        [Key, Column(Order = 0)]
        [ForeignKey("Game")]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Player")]
        public string PlayerId { get; set; }
        public virtual Player Player { get; set; }

        public bool ArtCollected { get; set; }
        public bool EntertainmentCollected { get; set; }
        public bool SportsCollected { get; set; }
        public bool GeographyCollected { get; set; }
        public bool HistoryCollected { get; set; }
        public bool ScienceCollected { get; set; }
    }
}