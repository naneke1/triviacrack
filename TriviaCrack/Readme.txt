# Introduction
This is a readme file for our Capstone TriviaCrackle project. It is based on the existing app named Trivia Crack.
You can find it here: https://triviacrackle.azurewebsites.net/. 

# Getting Started
1.	How to Run:
    Just go to the site in the above URL. The site runs in-browser and works across all browsers. I would recommend using Firefox.

2.	User Names and Passwords
    Username: admin@triviacrack.com      Password: Password#1
    Username: admin2@triviacrack.com     Password: Password#1

3.	Helpful Tips:
    Explanations of the power-ups and achievements can found on the site's help page: https://triviacrackle.azurewebsites.net/Home/Help
    If you try to login externally and it redirects to an empty login page: wait a bit and try again. The server is really slow and it takes
    some time to register the external login. If you get an error page, try refreshing the page or wait until it redirects. 

# Build and Test
If you want to build the project, clone the latest version of the project from this repository into Visual Studio. To run tests in Visual Studio
go to the menu bar and click: Tests -> Run -> All Tests. To run the project click the button with the green triangle and 'Internet Explorer' on it.
You can use the drop down on the button to change browsers to your browser of choice. 

# Contribute
This is a project for academic use only so modifying this code after the due date (4/25/18) is not allowed. 