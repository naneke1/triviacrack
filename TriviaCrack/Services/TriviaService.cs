﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using TriviaCrack.ViewModels;

namespace TriviaCrack.Services
{
    public class TriviaService
    {
        private const int ART = 25;
        private const int ENTERTAINMENT = 12;
        private const int SPORTS = 21;
        private const int GEOGRAPHY = 22;
        private const int HISTORY = 23;
        private const int SCIENCE = 17;

        private CategoryCollectService categoryCollectService;
        private QuestionService questionService;
        private AnswerService answerService;
        private GameService gameService;
        private StreakService streakService;
        private PlayerService playerService;
        private ClassicChallengeService classicChallengeService;
        private PowerUpService powerUpService;
        private SiteStatsService statService;

        public Player LoggedInPlayer { get; }

        public TriviaService(TriviaContext db)
        {
            this.categoryCollectService = new CategoryCollectService(db);
            this.questionService = new QuestionService(db);
            this.answerService = new AnswerService(db);
            this.gameService = new GameService(db);
            this.streakService = new StreakService(db);
            this.playerService = new PlayerService(db);
            this.powerUpService = new PowerUpService(db);
            this.statService = new SiteStatsService(db);
            this.classicChallengeService = new ClassicChallengeService(db, this.categoryCollectService, this.questionService);
            this.LoggedInPlayer = this.playerService.FindPlayerById(System.Web.HttpContext.Current.Session["LoggedInUserId"].ToString());
        }

        public TriviaService(TriviaContext db, string loggedInPlayerId)
        {
            this.categoryCollectService = new CategoryCollectService(db);
            this.questionService = new QuestionService(db);
            this.answerService = new AnswerService(db);
            this.gameService = new GameService(db);
            this.streakService = new StreakService(db);
            this.playerService = new PlayerService(db);
            this.powerUpService = new PowerUpService(db);
            this.statService = new SiteStatsService(db);
            this.classicChallengeService = new ClassicChallengeService(db, this.categoryCollectService, this.questionService);
            this.LoggedInPlayer = this.playerService.FindPlayerById(loggedInPlayerId);
        }

        #region Game Methods

        public Game FindGameById(int gameId)
        {
            return this.gameService.FindGameById(gameId);
        }

        public void SwitchTurns(int gameId)
        {
            var game = this.FindGameById(gameId);
            this.gameService.SwitchTurns(game, this.LoggedInPlayer.Id);
            this.streakService.ResetPlayerStreak(game.Id, this.LoggedInPlayer.Id);
        }

        public void EndGame(Game game)
        {
            this.gameService.EndGame(game, this.LoggedInPlayer.Id);
        }

        public Game CreateNewGame(string playerTwoUsername)
        {
            var player1 = this.LoggedInPlayer;
            var player2 = this.FindPlayerByUsername(playerTwoUsername);
            var newGame = this.gameService.Create(player1, player2);

            this.categoryCollectService.Create(newGame.Id, player1.Id);
            this.categoryCollectService.Create(newGame.Id, player2.Id);

            this.streakService.AddDefaultPlayerStreaks(newGame);

            return newGame;
        }

        public PlayerGames GetEmptyPlayerGames()
        {
            return new PlayerGames()
            {
                LoggedInPlayer = this.LoggedInPlayer,
                ActiveGames = new List<Game>(),
                FinishedGames = new List<Game>()
            };
        }

        public PlayerGames FindPlayerGames()
        {
            var loggedInPlayerGames = this.gameService.FindPlayerGames(this.LoggedInPlayer.Id);

            return new PlayerGames()
            {
                LoggedInPlayer = this.LoggedInPlayer,
                ActiveGames = loggedInPlayerGames.Where(game => game.IsActive).ToList(),
                FinishedGames = loggedInPlayerGames.Where(game => !game.IsActive).ToList()
            };
        }

        public void CheckRoundRulesForIncorrectAnswer(int gameId)
        {
            var gameToUpdate = this.FindGameById(gameId);
            var player = this.LoggedInPlayer;
            var opponent = this.GetOpponent(gameToUpdate.Id);
            
            var player1CategoriesCollected = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(gameToUpdate.Id, player.Id);
            var player2CategoriesCollected =
                this.categoryCollectService.FindOpponentCategoriesCollected(gameToUpdate.Id, player.Id);

            if (EndOfGameAndLoggedInPlayerHasMostCategories(gameToUpdate, player, player1CategoriesCollected, player2CategoriesCollected))
            {
                this.gameService.EndGame(gameToUpdate, player.Id);
            }
            else if (EndOfGameAndOpponentHasMostCategories(gameToUpdate, opponent, player1CategoriesCollected, player2CategoriesCollected))
            {
                this.gameService.EndGame(gameToUpdate, opponent.Id);
            }
            else if (EndOfGameAndCategoriesAreTied(gameToUpdate, player1CategoriesCollected, player2CategoriesCollected))
            {
                this.SwitchTurns(gameToUpdate.Id);
                this.StartNewChallenge(gameToUpdate.Id, "", "");
            }
            else
            {
                this.SwitchTurns(gameToUpdate.Id);
            }
        }

        private bool EndOfGameAndLoggedInPlayerHasMostCategories(Game game, Player player, 
            CategoriesCollected player1CategoriesCollected, CategoriesCollected player2CategoriesCollected)
        {
            return game.Round >= 5 && game.PlayerTurn == player.Id &&
                   this.categoryCollectService.GetCategoryCount(player1CategoriesCollected) >
                   this.categoryCollectService.GetCategoryCount(player2CategoriesCollected);
        }

        private bool EndOfGameAndOpponentHasMostCategories(Game game, Player opponent,
            CategoriesCollected player1CategoriesCollected, CategoriesCollected player2CategoriesCollected)
        {
            return game.Round >= 5 && game.PlayerTurn == opponent.Id &&
                   this.categoryCollectService.GetCategoryCount(player2CategoriesCollected) >
                   this.categoryCollectService.GetCategoryCount(player1CategoriesCollected);
        }

        private bool EndOfGameAndCategoriesAreTied(Game game,
            CategoriesCollected player1CategoriesCollected, CategoriesCollected player2CategoriesCollected)
        {
            return game.Round >= 5 && 
                   this.categoryCollectService.GetCategoryCount(player1CategoriesCollected) ==
                   this.categoryCollectService.GetCategoryCount(player2CategoriesCollected);
        }

        #endregion

        #region Player Methods

        public Player GetOpponent(int gameId)
        {
            var game = this.gameService.FindGameById(gameId);
            var opponent = game.Player1Id == this.LoggedInPlayer.Id ? game.Player2 : game.Player1;

            return opponent;
        }

        public Player FindPlayerByUsername(string username)
        {
            return this.playerService.FindPlayerByUsername(username);
        }

        public void IncrementPlayerStats(string playerId)
        {
            this.playerService.IncrementStats(playerId);
        }
        public void AddCoinsToPlayer(string playerId, int amount)
        {
            this.playerService.AddCoinsToPlayer(playerId, amount);
        }

        public void RemoveCoinsFRomPlayer(string playerId, int amount)
        {
            this.playerService.RemoveCoinsFromPlayer(playerId, amount);
        }

        #endregion

        #region Information Methods

        public GameInformation GetGameInformation(Game game, string category, string isCrownQuestion, int playerLevel)
        {
            PlayerCategories playerCategories = new PlayerCategories
            {
                LoggedInPlayerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(game.Id, this.LoggedInPlayer.Id),
                OpponentCategories = this.categoryCollectService.FindOpponentCategoriesCollected(game.Id, this.LoggedInPlayer.Id)
            };

            return new GameInformation
            {
                Question = this.findQuestionByCategoryAndDifficulty(category, playerLevel),
                Game = game,
                Player = this.LoggedInPlayer,
                PlayerStreak = this.streakService.FindLoggedInPlayerStreak(game.Id, this.LoggedInPlayer.Id).PlayerStreak,
                PlayerCategoriesCollected = playerCategories,
                OpponentHasCategoryToSteal = this.categoryCollectService.PlayerHasCategory(playerCategories.OpponentCategories).ToString(),
                PlayerHasCategory = this.categoryCollectService.PlayerHasCategory(playerCategories.LoggedInPlayerCategories).ToString(),
                IsCrownQuestion = isCrownQuestion,
                CategoryCount = this.categoryCollectService.GetCategoryCount(playerCategories.LoggedInPlayerCategories),
                ExtraTime = this.powerUpService.GetPowerUp("Extra Time"),
                Bomb = this.powerUpService.GetPowerUp("Bomb"),
                AutoCorrect = this.powerUpService.GetPowerUp("Auto Correct")
            };
        }

        #endregion

        #region Question Methods

        public void UpdateQuestionRating(string questionId, string rating)
        {
            var currentQuestion = this.questionService.FindQuestionById(int.Parse(questionId));
            this.questionService.UpdateRating(currentQuestion, rating);
        }

        public void AddQuestions(dynamic jsonObject, string category, string difficulty)
        {
            for (int i = 0; i < jsonObject.results.Count; i++)
            {
                if (jsonObject.results[i].incorrect_answers.Count < 3) continue;

                string questionText = jsonObject.results[i].question;
                string correctAnswer = jsonObject.results[i].correct_answer;
                string[] answers = {jsonObject.results[i].incorrect_answers[0], jsonObject.results[i].incorrect_answers[1],
                                    jsonObject.results[i].correct_answer, jsonObject.results[i].incorrect_answers[2] };

                this.ShuffleAnswers(answers);
                this.GenerateQuestion(questionText, correctAnswer, answers, this.GetCategory(category), difficulty);
            }
        }

        private int GetCategory(string category)
        {
            switch (int.Parse(category))
            {
                case ART:
                    return 1;
                case ENTERTAINMENT:
                    return 2;
                case SPORTS:
                    return 3;
                case GEOGRAPHY:
                    return 4;
                case HISTORY:
                    return 5;
                case SCIENCE:
                    return 6;
            }
            return 0;
        }

        private void GenerateQuestion(string questionText, string correctAnswerText, string[] answerChoices, int category, string difficulty)
        {
            string formattedQuestionText = HttpUtility.HtmlDecode(questionText);
            var correctAnswerId = this.answerService.GetAnswerId(correctAnswerText);
            var answerChoice1Id = this.answerService.GetAnswerId(answerChoices[0]);
            var answerChoice2Id = this.answerService.GetAnswerId(answerChoices[1]);
            var answerChoice3Id = this.answerService.GetAnswerId(answerChoices[2]);
            var answerChoice4Id = this.answerService.GetAnswerId(answerChoices[3]);
            var categoryId = category;

            this.questionService.Create(formattedQuestionText, correctAnswerId, answerChoice1Id, answerChoice2Id, answerChoice3Id, answerChoice4Id, categoryId, difficulty);
        }

        private Question findQuestionByCategoryAndDifficulty(string category, int playerLevel)
        {
            Question question = null;
            if (playerLevel <= 2)
            {
                question = this.questionService.FindQuestionByCategoryAndDifficulty(category, "Easy");
            } else if (playerLevel <= 3)
            {
                question = this.questionService.FindQuestionByCategoryAndDifficulty(category, "Medium");
            }
            else
            {
                question = this.questionService.FindQuestionByCategoryAndDifficulty(category, "Hard");
            }
            return question;
        }

        #endregion

        #region Answer Methods

        public void ShuffleAnswers(string[] answerChoices)
        {
            this.answerService.ShuffleAnswerChoices(answerChoices);
        }
        #endregion

        #region Streak Methods
        public Streak FindLoggedInPlayerStreak(int gameId)
        {
            return this.streakService.FindLoggedInPlayerStreak(gameId, this.LoggedInPlayer.Id);
        }

        public void IncrementLoggedInPlayerStreak(int gameId)
        {
            this.streakService.IncrementPlayerStreak(gameId, this.LoggedInPlayer.Id);
        }

        public void ResetLoggedInPlayerStreak(int gameId)
        {
            this.streakService.ResetPlayerStreak(gameId, this.LoggedInPlayer.Id);
        }

        public void ResetBothPlayerStreaks(int gameId)
        {
            this.streakService.ResetBothPlayerStreaks(gameId, this.LoggedInPlayer.Id);
        }

        #endregion

        #region Category Methods

        public PlayerCategories GetBothPlayerCategories(int gameId)
        {
            return new PlayerCategories
            {
                LoggedInPlayerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(gameId, this.LoggedInPlayer.Id),
                OpponentCategories = this.categoryCollectService.FindOpponentCategoriesCollected(gameId, this.LoggedInPlayer.Id)
            };
        }

        public bool PlayerHasCategory(CategoriesCollected playerCategoriesCollected)
        {
            return this.categoryCollectService.PlayerHasCategory(playerCategoriesCollected);
        }

        public void AddCategoryForLoggedInPlayer(int gameId, string category)
        {
            var playerCategoriesToUpdate = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(gameId, this.LoggedInPlayer.Id);
            this.categoryCollectService.AddCategory(playerCategoriesToUpdate, category);

            if (this.categoryCollectService.GetCategoryCount(playerCategoriesToUpdate) == 6)
            {
                this.EndGame(this.FindGameById(gameId));
            }

            this.ResetLoggedInPlayerStreak(gameId);
        }

        public int GetCategoryCount(int gameId, string playerId)
        {
            var playerCategoriesToUpdate = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(gameId, this.LoggedInPlayer.Id);
            return this.categoryCollectService.GetCategoryCount(playerCategoriesToUpdate);
        }

        #endregion

        #region Classic Challenge Methods

        public ClassicChallengeQuestion FindLoggedInPlayerChallengeData(int gameId)
        {
            return this.classicChallengeService.FindLoggedInPlayerChallengeData(gameId, this.LoggedInPlayer.Id);
        }

        public ClassicChallengeQuestion FindOpponentPlayerChallengeData(int gameId)
        {
            return this.classicChallengeService.FindOpponentChallengeData(gameId, this.LoggedInPlayer.Id);
        }

        public void SwitchChallengeTurns(int gameId)
        {
            var game = this.gameService.FindGameById(gameId);
            this.classicChallengeService.SwitchTurns(game, this.LoggedInPlayer.Id);
        }

        public Question GetNextChallengeQuestion(int gameId)
        {
            var currentPlayerChallengeData = this.classicChallengeService.FindLoggedInPlayerChallengeData(gameId, this.LoggedInPlayer.Id);
            var question = this.classicChallengeService.GetNextQuestion(currentPlayerChallengeData);

            if (question != null)
            {
                this.classicChallengeService.IncrementQuestionCount(currentPlayerChallengeData);
            }

            return question;
        }

        public void StartNewChallenge(int gameId, string categoryToSteal, string categoryToBet)
        {
            var game = this.gameService.FindGameById(gameId);
            var opponent = game.Player1Id == this.LoggedInPlayer.Id ? game.Player2 : game.Player1;

            this.classicChallengeService.CreateChallenge(this.LoggedInPlayer.Id, game, opponent, categoryToSteal, categoryToBet);
        }

        public void HandleCorrectAnswer(int gameId)
        {
            this.classicChallengeService.HandleCorrectAnswer(gameId, this.LoggedInPlayer.Id);
        }

        #endregion

        #region Site Stats Methods

        public void AddQuestionStat(int questionId, int gameId, string playerId)
        {
            this.statService.AddQuestionStat(questionId, gameId, playerId);
        }

        public void MarkQuestionAnswered(int gameId, string playerId, int questionId, string questionType, bool isCorrect)
        {
            bool isCrown = false;
            bool isChallenge = false;

            switch (questionType)
            {
                case "Crown":
                    isCrown = true;
                    break;
                case "Challenge":
                    isChallenge = true;
                    break;
                case "Regular":
                    break;
            }
            this.statService.MarkQuestionAnswered(gameId, playerId, questionId, isCrown, isChallenge, isCorrect);
        }

        public int ReturnPlayerCategoryStatsPerGame(string category, int gameId, string playerId)
        {
            int categoryId = this.GetCategoryId(category);
            return this.statService.ReturnPlayerCategoryStatsPerGame(categoryId, gameId, playerId);
        }

        private int GetCategoryId(string category)
        {
            switch (category)
            {
                case "ART":
                    return 1;
                case "ENTERTAINMENT":
                    return 2;
                case "SPORTS":
                    return 3;
                case "GEOGRAPHY":
                    return 4;
                case "HISTORY":
                    return 5;
                case "SCIENCE":
                    return 6;
            }
            return 0;
        }

        public int ReturnCorrectAnsweredPlayerCount(int gameId, string playerId)
        {
            return  this.statService.ReturnCorrectAnsweredPlayerCount(gameId, playerId);
        }

        public int ReturnIncorrectAnsweredPlayerCount(int gameId, string playerId)
        {
            return this.statService.ReturnIncorrectAnsweredPlayerCount(gameId, playerId);
        }

        public int ReturnTotalAnsweredPlayerCount(int gameId, string playerId)
        {
            return this.statService.ReturnTotalAnsweredPlayerCount(gameId, playerId);
        }

        public int ReturnCrownQuestionPlayerCount(int gameId, string playerId)
        {
            return this.statService.ReturnCrownPlayerCount(gameId, playerId);
        }

        public int ReturnChallengeQuestionPlayerCount(int gameId, string playerId)
        {
            return this.statService.ReturnChallengePlayerCount(gameId, playerId);
        }

        #endregion

        #region Score Methods

        public ScoreInformation GetScoreInformation(int gameId)
        {
            var player = this.LoggedInPlayer.Id;
            var opponent = this.GetOpponent(gameId).Id;
            int categoriesCountOpponent;
            int categoriesCountLoggedIn;

            try
            {
                PlayerCategories playerCategories = new PlayerCategories
                {
                    LoggedInPlayerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(gameId, player),
                    OpponentCategories = this.categoryCollectService.FindOpponentCategoriesCollected(gameId, player)
                };

                categoriesCountOpponent =
                    this.categoryCollectService.GetCategoryCount(playerCategories.OpponentCategories);
                categoriesCountLoggedIn =
                    this.categoryCollectService.GetCategoryCount(playerCategories.LoggedInPlayerCategories);
            }
            catch (System.InvalidOperationException)
            {

                categoriesCountOpponent = 0;
                categoriesCountLoggedIn = 0;
            }

            return new ScoreInformation
            {
                CategoriesCountOpponent = categoriesCountOpponent,
                CategoriesCountLoggedIn = categoriesCountLoggedIn,

                ArtStatsOpponent = this.ReturnPlayerCategoryStatsPerGame("ART", gameId, opponent),
                ArtStatsLoggedIn = this.ReturnPlayerCategoryStatsPerGame("ART", gameId, player),
                GeographyStatsOpponent = this.ReturnPlayerCategoryStatsPerGame("GEOGRAPHY", gameId, opponent),
                GeographyStatsLoggedIn = this.ReturnPlayerCategoryStatsPerGame("GEOGRAPHY", gameId, player),
                EntertainmentStatsOpponent = this.ReturnPlayerCategoryStatsPerGame("ENTERTAINMENT", gameId, opponent),
                EntertainmentStatsLoggedIn = this.ReturnPlayerCategoryStatsPerGame("ENTERTAINMENT", gameId, player),
                HistoryStatsOpponent = this.ReturnPlayerCategoryStatsPerGame("HISTORY", gameId, opponent),
                HistoryStatsLoggedIn = this.ReturnPlayerCategoryStatsPerGame("HISTORY", gameId, player),
                ScienceStatsOpponent = this.ReturnPlayerCategoryStatsPerGame("SCIENCE", gameId, opponent),
                ScienceStatsLoggedIn = this.ReturnPlayerCategoryStatsPerGame("SCIENCE", gameId, player),
                SportsStatsOpponent = this.ReturnPlayerCategoryStatsPerGame("SPORTS", gameId, opponent),
                SportsStatsLoggedIn = this.ReturnPlayerCategoryStatsPerGame("SPORTS", gameId, player),

                ChallengeStatsOpponent = this.ReturnChallengeQuestionPlayerCount(gameId, opponent),
                ChallengeStatsLoggedIn = this.ReturnChallengeQuestionPlayerCount( gameId, player),
                CrownStatsOpponent = this.ReturnCrownQuestionPlayerCount(gameId, opponent),
                CrownStatsLoggedIn = this.ReturnCrownQuestionPlayerCount(gameId, player),
                CorrectStatsOpponent = this.ReturnCorrectAnsweredPlayerCount(gameId, opponent),
                CorrectStatsLoggedIn = this.ReturnCorrectAnsweredPlayerCount(gameId, player),
                IncorrectStatsOpponent = this.ReturnIncorrectAnsweredPlayerCount(gameId, opponent),
                IncorrectStatsLoggedIn = this.ReturnIncorrectAnsweredPlayerCount(gameId, player),
                AnsweredStatsOpponent = this.ReturnTotalAnsweredPlayerCount(gameId, opponent),
                AnsweredStatsLoggedIn = this.ReturnTotalAnsweredPlayerCount(gameId, player)
            };
        }

        #endregion
    }
}