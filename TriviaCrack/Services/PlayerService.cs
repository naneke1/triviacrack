﻿using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class PlayerService
    {
        private TriviaContext context;

        public PlayerService(TriviaContext db)
        {
            this.context = db;
        }

        public Player FindPlayerById(string playerId)
        {
            var id = playerId;
            Player player = this.context.Players.SingleOrDefault(pal => pal.Id == id);
            return player;
        }

        public Player FindPlayerByUsername(string userName)
        {
            return this.context.Players.Single(p => p.UserName == userName);
        }

        public void IncrementStats(string playerId)
        {
            var player = this.FindPlayerById(playerId);
            player.OverallScore++;
            this.CheckLevel(player);
            this.context.SaveChanges();
        }

        private void CheckLevel(Player currentPlayer)
        {
            if (currentPlayer.Level == 0)
            {
                currentPlayer.Level = 1;
            }

            var ptsNeeded = currentPlayer.OverallScore - currentPlayer.PrevScoreThreshold;

            if (ptsNeeded == (currentPlayer.Level + 2))
            {
                currentPlayer.Level++;
                currentPlayer.PrevScoreThreshold = currentPlayer.OverallScore;
            }
        }

        public void AddCoinsToPlayer(string playerId, int amount)
        {
            var player = this.FindPlayerById(playerId);
            player.AddCoins(amount);
            this.context.SaveChanges();
        }

        public void RemoveCoinsFromPlayer(string playerId, int amount)
        {
            var player = this.FindPlayerById(playerId);
            player.RemoveCoins(amount);
            this.context.SaveChanges();
        }
    }
}