﻿using System;
using System.Collections.Generic;
using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class GameService
    {
        private TriviaContext context;

        public GameService(TriviaContext triviaContext)
        {
            this.context = triviaContext;
        }

        public Game FindGameById(int gameId)
        {
            return this.context.Games.Single(g => g.Id == gameId);
        }

        public Game Create(Player playerOne, Player playerTwo)
        {
            Game newGame = new Game
            {
                Player1Id = playerOne.Id,
                Player1 = playerOne,
                Player2Id = playerTwo.Id,
                Player2 = playerTwo,
                StartDate = Convert.ToDateTime(DateTime.UtcNow.ToString("MM/dd/yyyy")),
                IsActive = true,
                PlayerTurn = playerOne.Id,
                ChallengeInProgress = false,
                Round = 1
            };
            this.context.Games.Add(newGame);
            this.context.SaveChanges();
            return newGame;
        }

        public void IncrementGameRound(Game game)
        {
            game.Round++;
            this.context.SaveChanges();
        }

        public void SwitchTurns(Game gameToUpdate, string loggedInPlayerId)
        {
            var playerId = loggedInPlayerId;

            if (gameToUpdate.Player1Id == playerId)
            {
                gameToUpdate.PlayerTurn = gameToUpdate.Player2Id;
            }
            else
            {
                gameToUpdate.PlayerTurn = gameToUpdate.Player1Id;
                this.IncrementGameRound(gameToUpdate);
            }
            this.context.SaveChanges();
        }

        public IEnumerable<Game> FindPlayerGames(string playerId)
        {
            return this.context.Games.Where(g => g.Player1.Id == playerId || g.Player2.Id == playerId).ToList();
        }

        public void EndGame(Game game, string winnerId)
        {
            game.IsActive = false;
            game.WinnerId = winnerId;
            game.EndDate = DateTime.Now;
            this.context.SaveChanges();
        }
    }
}