﻿using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class StreakService
    {
        private TriviaContext context;

        public StreakService(TriviaContext triviaContext)
        {
            this.context = triviaContext;
        }

        public Streak FindLoggedInPlayerStreak(int gameId, string playerId)
        {
            return this.context.Streaks.Single(s => s.GameId == gameId && s.PlayerId == playerId);
        }

        public Streak FindOpponentPlayerStreak(int gameId, string playerId)
        {
            return this.context.Streaks.Single(s => s.GameId == gameId && s.PlayerId != playerId);
        }

        public void ResetPlayerStreak(int gameId, string playerId)
        {
            this.FindLoggedInPlayerStreak(gameId, playerId).PlayerStreak = 0;
            this.context.SaveChanges();
        }

        public void IncrementPlayerStreak(int gameId, string playerId)
        {
            this.FindLoggedInPlayerStreak(gameId, playerId).PlayerStreak++;
            this.context.SaveChanges();
        }

        public void AddDefaultPlayerStreaks(Game game)
        {
            this.context.Streaks.Add(new Streak { GameId = game.Id, PlayerId = game.Player1Id });
            this.context.Streaks.Add(new Streak { GameId = game.Id, PlayerId = game.Player2Id });
            this.context.SaveChanges();
        }

        public void ResetBothPlayerStreaks(int gameId, string playerId)
        {
            this.FindLoggedInPlayerStreak(gameId, playerId).PlayerStreak = 0;
            this.FindOpponentPlayerStreak(gameId, playerId).PlayerStreak = 0;
            this.context.SaveChanges();
        }
    }
}