﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class AnswerService
    {
        private TriviaContext context;
        private Random random = new Random();

        public AnswerService(TriviaContext triviaContext)
        {
            this.context = triviaContext;
        }

        public int GetAnswerId(string answerText)
        {
            int answerId = 0;
            string answer = HttpUtility.HtmlDecode(answerText);
            
            if (!this.context.Answers.Any(a => a.Text == answer))
            {
                var latestAnswer = new Answer { Id = this.context.Answers.Count() + 1, Text = answer };
                this.context.Answers.Add(latestAnswer);
                answerId = latestAnswer.Id;
                this.context.SaveChanges();
                return answerId;
            }
            
            for (int i = 1; i < this.context.Answers.Count() + 1; i++)
            {
                if (this.context.Answers.Find(i).Text == answer)
                {
                    answerId = i;
                }
            }

            return answerId;
        }

        public void ShuffleAnswerChoices(string[] answers)
        {
            int n = answers.Length;
            while (n > 1)
            {
                int k = random.Next(n--);
                string temp = answers[n];
                answers[n] = answers[k];
                answers[k] = temp;
            }
        }
    }
}