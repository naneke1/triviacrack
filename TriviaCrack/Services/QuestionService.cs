﻿using System;
using System.Collections.Generic;
using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class QuestionService
    {
        private TriviaContext context;

        public QuestionService(TriviaContext triviaContext)
        {
            this.context = triviaContext;
        }

        public void Create(string questionText, int correctAnswerId, int answerChoice1Id, int answerChoice2Id, int answerChoice3Id, int answerChoice4Id, int categoryId, string difficulty)
        {
            if (this.context.Questions.Any(q => q.Text == questionText)) return;

            var question = new Question
            {
                Text = questionText,
                CorrectAnswerId = correctAnswerId,
                AnswerChoice1Id = answerChoice1Id,
                AnswerChoice2Id = answerChoice2Id,
                AnswerChoice3Id = answerChoice3Id,
                AnswerChoice4Id = answerChoice4Id,
                CategoryId = categoryId,
                Difficulty = difficulty
            };
            this.context.Questions.Add(question);
            this.context.SaveChanges();
        }

        public Question FindQuestionById(int questionId)
        {
            return this.context.Questions.Single(q => q.Id == questionId);
        }

        public Question FindQuestionByCategory(string category)
        {
            return this.context.Questions.Where(q => q.Category.Description == category).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
        }

        public Question FindQuestionByCategoryAndDifficulty(string category, string difficulty)
        {
            return this.context.Questions.Where(q => q.Category.Description == category && q.Difficulty == difficulty).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
        }

        public void UpdateRating(Question question, string rating)
        {
            if (rating.Equals("fun"))
            {
                question.FunRatingCount++;
            }
            else
            {
                question.BoringRatingCount++;
            }
            this.context.SaveChanges();
        }

        public List<Question> GetRandomQuestions(string category, int amount)
        {
            var categoryQuestions = category == "" ? this.context.Questions.ToList() : this.context.Questions.Where(q => q.Category.Description == category).ToList();
            
            var shuffledQuestions = this.ShuffleQuestions(categoryQuestions);
            var randomQuestions = new List<Question>();

            for (int i = 0; i < amount; i++)
            {
                randomQuestions.Add(shuffledQuestions.ElementAt(i));
            }

            return randomQuestions;
        }

        private IEnumerable<Question> ShuffleQuestions(IEnumerable<Question> source)
        {
            Random random = new Random();
            var questions = source.ToList();
            for (int i = questions.Count; i > 0; i--)
            {
                var index = random.Next(i);
                var temp = questions[index];
                questions[index] = questions[i - 1];
                questions[i - 1] = temp;
                yield return temp;
            }
        }

    }
}