﻿using System;
using System.Collections.Generic;
using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class SiteStatsService
    {
        private TriviaContext context;
        private List<Statistics> currentWeekStats = new List<Statistics>();

        public SiteStatsService(TriviaContext db)
        {
            this.context = db;
        }

        public int ReturnChallengePlayerCount(int gameId, string playerId)
        {
            return this.context.Statistics.Count(s =>
                s.IsAnsweredCorrectly &&
                s.IsChallengeQuestion &&
                s.PlayerId == playerId &&
                s.Game.Id == gameId);
        }

        public int ReturnCrownPlayerCount(int gameId, string playerId)
        {
            return this.context.Statistics.Count(s =>
                s.IsAnsweredCorrectly &&
                s.IsCrownQuestion &&
                s.PlayerId == playerId &&
                s.Game.Id == gameId);
        }

        public int ReturnCorrectAnsweredPlayerCount(int gameId, string playerId)
        {
            return this.context.Statistics.Count(s =>
                s.IsAnsweredCorrectly &&
                s.PlayerId == playerId &&
                s.Game.Id == gameId);
        }

        public int ReturnIncorrectAnsweredPlayerCount(int gameId, string playerId)
        {
            return this.context.Statistics.Count(s =>
                s.IsAnsweredCorrectly  == false &&
                s.PlayerId == playerId &&
                s.Game.Id == gameId);
        }

        public int ReturnTotalAnsweredPlayerCount(int gameId, string playerId)
        {
            return this.context.Statistics.Count(s =>
                s.PlayerId == playerId &&
                s.Game.Id == gameId);
        }

        public int ReturnTotalQuestionWinsForPlayer(string playerId)
        {
            return this.context.Statistics.Count(s =>
                s.IsAnsweredCorrectly &&
                s.PlayerId == playerId);
        }

        public int ReturnCategoryQuestionWinsForPlayer(int categoryId, string playerId)
        {
            return this.context.Statistics.Count(s => 
            s.IsAnsweredCorrectly && 
            s.Question.CategoryId == categoryId && 
            s.PlayerId == playerId);
        }

        public int ReturnPlayerCategoryStatsPerGame(int categoryId, int gameId, string playerId)
        {
            var total = this.context.Statistics.Count(
                s => s.GameId == gameId && 
                s.Question.CategoryId == categoryId &&
                s.PlayerId == playerId);

            var correct = this.context.Statistics.Count(
                s => s.IsAnsweredCorrectly &&
                     s.GameId == gameId &&
                     s.Question.CategoryId == categoryId &&
                     s.PlayerId == playerId);

            if (total == 0)
            {
                return 0;
            }

            return (int)Math.Round(correct * 100.00 / total, MidpointRounding.AwayFromZero);
        }

        public int ReturnOverallCategoryStats(int categoryId)
        {
            var total = this.context.Statistics.Count();
            var correct = this.context.Statistics.Count(s => s.IsAnsweredCorrectly && s.Question.CategoryId == categoryId);

            if (total == 0)
            {
                return 0;
            }

            return (int) Math.Round(correct * 100.00 / total, MidpointRounding.AwayFromZero);
        }

        public int ReturnMonthlyCategoryStats(int categoryId)
        {
            var total = this.context.Statistics.Count(
                s => s.DateAnswered.Month == DateTime.Now.Month);
            var correct = this.context.Statistics.Count(
                s => s.IsAnsweredCorrectly && 
                s.Question.CategoryId == categoryId && 
                s.DateAnswered.Month == DateTime.Now.Month &&
                s.DateAnswered.Year == DateTime.Now.Year);

            if (total == 0)
            {
                return 0;
            }

            return (int)Math.Round(correct * 100.00 / total, MidpointRounding.AwayFromZero);
        }

        public int ReturnWeeklyCategoryStats(int categoryId)
        {
            this.determineCurrentWeekStats();
            var total = this.currentWeekStats.Count;
            var correct = this.currentWeekStats.Count(
                s => s.IsAnsweredCorrectly &&
                     s.Question.CategoryId == categoryId);

            if (total == 0)
            {
                return 0;
            }

            return (int)Math.Round(correct * 100.00 / total, MidpointRounding.AwayFromZero);
        }

        public void AddQuestionStat(int questionId, int gameId, string playerId)
        {
            //create statistics item
            Statistics currentStat = new Statistics();
            currentStat.GameId = gameId;
            currentStat.PlayerId = playerId;
            currentStat.QuestionId = questionId;
            currentStat.IsAnsweredCorrectly = false;
            currentStat.IsCrownQuestion = false;
            currentStat.IsChallengeQuestion = false;
            currentStat.DateAnswered = System.DateTime.Now.Date;
            //add to statistics table only if doesn't already exist for today/this game
            var queryStat = this.context.Statistics.SingleOrDefault(s => s.GameId == currentStat.GameId && s.PlayerId == currentStat.PlayerId && s.QuestionId == currentStat.QuestionId);
            if (queryStat == null)
            {
                this.context.Statistics.Add(currentStat);
                this.context.SaveChanges();
            }
        }

        public void MarkQuestionAnswered(int gameId, string playerId, int questionId, bool isCrown, bool isChallenge, bool isCorrect)
        {
            Statistics current = this.context.Statistics.Single(s => s.GameId == gameId && s.PlayerId == playerId && s.QuestionId == questionId);
            current.IsAnsweredCorrectly = isCorrect;
            current.IsCrownQuestion = isCrown;
            current.IsChallengeQuestion = isChallenge;
            this.context.SaveChanges();
        }

        private void determineCurrentWeekStats()
        {
            DateTime startDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek);
            DateTime endDate = startDate.AddDays(6);
            
            var currWeekStats = from stat in this.context.Statistics
                                    where stat.DateAnswered >= startDate && stat.DateAnswered <= endDate
                                    select stat;
            this.currentWeekStats = currWeekStats.ToList();
        }
    }
}