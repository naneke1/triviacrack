﻿using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class PowerUpService
    {
        private TriviaContext context;

        public PowerUpService(TriviaContext db)
        {
            this.context = db;
        }

        public int GetPowerUpCost(string powerUpName)
        {
            PowerUp power = this.GetPowerUp(powerUpName);

            if (power != null)
            {
                return power.Cost; 
            }

            return 0;
        }

        public PowerUp GetPowerUp(string powerUpName)
        {
            return this.context.PowerUps.Single(p => p.Name == powerUpName);
        }
    }
}