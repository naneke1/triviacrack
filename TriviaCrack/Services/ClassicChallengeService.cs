using System.Collections.Generic;
using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class ClassicChallengeService {
        private TriviaContext context;
        private CategoryCollectService categoryCollectService;
        private QuestionService questionService;

        public ClassicChallengeService(TriviaContext triviaContext, CategoryCollectService categoryService, QuestionService questionService) {
            this.context = triviaContext;
            this.categoryCollectService = categoryService;
            this.questionService = questionService;
        }

        public void CreateChallenge(string loggedInPlayerId, Game game, Player opponent, string categoryToSteal, string categoryToBet) {
            var randomizedQuestions = this.questionService.GetRandomQuestions(categoryToSteal, 7);
            var loggedInPlayerChallengeData = this.CreateLoggedInPlayerChallengeData(game.Id, loggedInPlayerId, categoryToBet, randomizedQuestions);
            var loggedInPlayer = game.Player1Id == loggedInPlayerId ? game.Player1 : game.Player2;
            var opponentChallengeData = this.CreateOpponentChallengeData(game, opponent, categoryToSteal, loggedInPlayerChallengeData);
            this.context.ClassicChallengeQuestions.Add(loggedInPlayerChallengeData);
            this.context.ClassicChallengeQuestions.Add(opponentChallengeData);
            game.ChallengeInProgress = true;
            loggedInPlayerChallengeData.Player = loggedInPlayer;
            this.IncrementChallengeCount(loggedInPlayerChallengeData, opponentChallengeData);
            this.context.SaveChanges();
        }

        public void EndChallenge(ClassicChallengeQuestion currentPlayer, ClassicChallengeQuestion opponent, Game currentGame) {
            currentGame.ChallengeInProgress = false;

            if (LoggedInPlayerWon(currentPlayer, opponent)) {
                currentGame.PlayerTurn = currentPlayer.PlayerId;
                this.RewardWinner(currentPlayer, opponent);
            }
            else {
                currentGame.PlayerTurn = opponent.PlayerId;
                this.RewardWinner(opponent, currentPlayer);
            }

            this.DeleteChallenge(currentPlayer, opponent);
            currentGame.Round++;
            this.context.SaveChanges();
        }

        public void EndFinalChallenge(ClassicChallengeQuestion currentPlayer, ClassicChallengeQuestion opponent,
            Game currentGame)
        {
            currentGame.ChallengeInProgress = false;
            currentGame.IsActive = false;
            currentGame.WinnerId = LoggedInPlayerWon(currentPlayer, opponent) ? currentPlayer.PlayerId : opponent.PlayerId;
            this.DeleteChallenge(currentPlayer, opponent);
            this.context.SaveChanges();
        }

        public void HandleCorrectAnswer(int gameId, string playerId) {
            var currentChallenge = this.FindLoggedInPlayerChallengeData(gameId, playerId);
            currentChallenge.TotalCorrect++;
            this.context.SaveChanges();
        }

        private void RewardWinner(ClassicChallengeQuestion player, ClassicChallengeQuestion opponent) {
            var loggedInPlayerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(player.GameId, player.PlayerId);
            var opponentPlayerCategories = this.categoryCollectService.FindOpponentCategoriesCollected(player.GameId, player.PlayerId);
            this.categoryCollectService.AddCategory(loggedInPlayerCategories, opponent.CategoryAtStake);
            this.categoryCollectService.RemoveCategory(opponentPlayerCategories, opponent.CategoryAtStake);
            player.Player.ChallengeWins++;
            this.context.SaveChanges();
        }

        private bool LoggedInPlayerWon(ClassicChallengeQuestion currentPlayer, ClassicChallengeQuestion opponent) {
            return currentPlayer.TotalCorrect > opponent.TotalCorrect;
        }

        public void IncrementQuestionCount(ClassicChallengeQuestion currentPlayer)
        {
            currentPlayer.CurrentQuestion++;
            this.context.SaveChanges();
        }

        public void IncrementChallengeCount(ClassicChallengeQuestion currentPlayer, ClassicChallengeQuestion opponent)
        {
            currentPlayer.Player.ChallengeCount++;
            opponent.Player.ChallengeCount++;
        }

        public ClassicChallengeQuestion FindLoggedInPlayerChallengeData(int gameId, string playerId) {
            return this.context.ClassicChallengeQuestions.Single(c => c.GameId == gameId && c.PlayerId == playerId);
        }

        public ClassicChallengeQuestion FindOpponentChallengeData(int gameId, string playerId) {
            return this.context.ClassicChallengeQuestions.Single(c => c.GameId == gameId && c.PlayerId != playerId);
        }

        private void DeleteChallenge(ClassicChallengeQuestion player, ClassicChallengeQuestion opponent) {
            this.context.ClassicChallengeQuestions.Remove(player);
            this.context.ClassicChallengeQuestions.Remove(opponent);
        }

        public void SwitchTurns(Game game, string playerId) {
            var currentPlayerChallengeData = this.FindLoggedInPlayerChallengeData(game.Id, playerId);
            var opponentChallengeData = this.FindOpponentChallengeData(game.Id, playerId);

            if (currentPlayerChallengeData.CurrentQuestion >= 6 && opponentChallengeData.CurrentQuestion >= 6)
            {
                if (game.Round >= 5)
                {
                    this.EndFinalChallenge(currentPlayerChallengeData, opponentChallengeData, game);
                }
                else
                {
                    this.EndChallenge(currentPlayerChallengeData, opponentChallengeData, game);
                }
                
            }
            else
            {
                game.PlayerTurn = game.Player1Id.Equals(playerId) ? game.Player2Id : game.Player1Id;
            }
            this.context.SaveChanges();
        }

        public Question GetNextQuestion(ClassicChallengeQuestion loggedInPlayerChallengeData)
        {
            Question nextQuestion = null;
            switch (loggedInPlayerChallengeData.CurrentQuestion)
            {
                case 0:
                    nextQuestion = loggedInPlayerChallengeData.Question1;
                    break;
                case 1:
                    nextQuestion = loggedInPlayerChallengeData.Question2;
                    break;
                case 2:
                    nextQuestion = loggedInPlayerChallengeData.Question3;
                    break;
                case 3:
                    nextQuestion = loggedInPlayerChallengeData.Question4;
                    break;
                case 4:
                    nextQuestion = loggedInPlayerChallengeData.Question5;
                    break;
                case 5:
                    nextQuestion = loggedInPlayerChallengeData.Question6;
                    break;
                case 6:
                    nextQuestion = loggedInPlayerChallengeData.TiebreakerQuestion;
                    break;
            }
            return nextQuestion;
        }

        private ClassicChallengeQuestion CreateLoggedInPlayerChallengeData(int gameId, string playerId, string category, IEnumerable<Question> randomQuestions) {
            return new ClassicChallengeQuestion
            {
                GameId = gameId,
                PlayerId = playerId,
                Question1Id = randomQuestions.ElementAt(0).Id,
                Question1 = randomQuestions.ElementAt(0),
                Question2Id = randomQuestions.ElementAt(1).Id,
                Question2 = randomQuestions.ElementAt(1),
                Question3Id = randomQuestions.ElementAt(2).Id,
                Question3 = randomQuestions.ElementAt(2),
                Question4Id = randomQuestions.ElementAt(3).Id,
                Question4 = randomQuestions.ElementAt(3),
                Question5Id = randomQuestions.ElementAt(4).Id,
                Question5 = randomQuestions.ElementAt(4),
                Question6Id = randomQuestions.ElementAt(5).Id,
                Question6 = randomQuestions.ElementAt(5),
                TiebreakerQuestionId = randomQuestions.ElementAt(6).Id,
                TiebreakerQuestion = randomQuestions.ElementAt(6),
                CategoryAtStake = category
            };
        }

        private ClassicChallengeQuestion CreateOpponentChallengeData(Game game, Player opponent, string category, ClassicChallengeQuestion existingChallenge) {
            return new ClassicChallengeQuestion
            {
                GameId = game.Id,
                Game = game,
                Player = opponent,
                PlayerId = opponent.Id,
                Question1Id = existingChallenge.Question1Id,
                Question1 = existingChallenge.Question1,
                Question2Id = existingChallenge.Question2Id,
                Question2 = existingChallenge.Question2,
                Question3Id = existingChallenge.Question3Id,
                Question3 = existingChallenge.Question3,
                Question4Id = existingChallenge.Question4Id,
                Question4 = existingChallenge.Question4,
                Question5Id = existingChallenge.Question5Id,
                Question5 = existingChallenge.Question5,
                Question6Id = existingChallenge.Question6Id,
                Question6 = existingChallenge.Question6,
                TiebreakerQuestionId = existingChallenge.TiebreakerQuestionId,
                TiebreakerQuestion = existingChallenge.TiebreakerQuestion,
                CategoryAtStake = category
            };
        }
    }
}