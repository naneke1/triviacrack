﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class AchievementService
    {
        private TriviaContext context;

        public AchievementService(TriviaContext db)
        {
            this.context = db;
        }

        public Achievement GetAchievement(string achievementName)
        {
            return this.context.Achievements.Single(p => p.Name == achievementName);
        }

        public List<Achievement> GetAllAchievements()
        {
            return this.context.Achievements.ToList();
        }
    }
}