using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Services
{
    public class CategoryCollectService
    {
        private TriviaContext context;

        public CategoryCollectService(TriviaContext triviaContext)
        {
            this.context = triviaContext;
        }

        public CategoriesCollected Create(int gameId, string playerId)
        {
            CategoriesCollected categoriesCollected = new CategoriesCollected
            {
                GameId = gameId,
                PlayerId = playerId,
                ArtCollected = false,
                EntertainmentCollected = false,
                SportsCollected = false,
                GeographyCollected = false,
                HistoryCollected = false,
                ScienceCollected = false
            };
            this.context.CategoriesCollected.Add(categoriesCollected);
            this.context.SaveChanges();
            return categoriesCollected;
        }

        public CategoriesCollected FindLoggedInPlayerCategoriesCollected(int gameId, string playerId)
        {
            return this.context.CategoriesCollected.Single(c => c.GameId == gameId && c.PlayerId == playerId);
        }

        public CategoriesCollected FindOpponentCategoriesCollected(int gameId, string playerId)
        {
            return this.context.CategoriesCollected.Single(c => c.GameId == gameId && c.PlayerId != playerId);
        }

        public void AddCategory(CategoriesCollected playerCategories, string category)
        {
            switch (category)
            {
                case "Art":
                    playerCategories.ArtCollected = true;
                    break;
                case "Entertainment":
                    playerCategories.EntertainmentCollected = true;
                    break;
                case "Sports":
                    playerCategories.SportsCollected = true;
                    break;
                case "Geography":
                    playerCategories.GeographyCollected = true;
                    break;
                case "History":
                    playerCategories.HistoryCollected = true;
                    break;
                case "Science":
                    playerCategories.ScienceCollected = true;
                    break;
            }
            this.context.SaveChanges();
        }

        public void RemoveCategory(CategoriesCollected playerCategories, string category)
        {
            switch (category)
            {
                case "Art":
                    playerCategories.ArtCollected = false;
                    break;
                case "Entertainment":
                    playerCategories.EntertainmentCollected = false;
                    break;
                case "Sports":
                    playerCategories.SportsCollected = false;
                    break;
                case "Geography":
                    playerCategories.GeographyCollected = false;
                    break;
                case "History":
                    playerCategories.HistoryCollected = false;
                    break;
                case "Science":
                    playerCategories.ScienceCollected = false;
                    break;
            }
            this.context.SaveChanges();
        }

        public int GetCategoryCount(CategoriesCollected playerCategories)
        {
            var count = 0;
            bool[] categoriesCollected = {
                                            playerCategories.ArtCollected, playerCategories.EntertainmentCollected,
                                            playerCategories.SportsCollected, playerCategories.GeographyCollected,
                                            playerCategories.HistoryCollected, playerCategories.ScienceCollected
            };

            for (int i = 0; i < categoriesCollected.Length; i++)
            {
                if (categoriesCollected[i]) count++;
            }

            return count;
        }

        public bool PlayerHasCategory(CategoriesCollected playerCategories)
        {
            return playerCategories.ArtCollected || playerCategories.EntertainmentCollected || playerCategories.SportsCollected || playerCategories.GeographyCollected || playerCategories.HistoryCollected || playerCategories.ScienceCollected;
        }
    }
}