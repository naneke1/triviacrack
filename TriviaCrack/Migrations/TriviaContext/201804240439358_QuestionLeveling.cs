namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuestionLeveling : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Question", "Difficulty", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Question", "Difficulty");
        }
    }
}
