namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ListingAchievements : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Achievement",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Achievement");
        }
    }
}
