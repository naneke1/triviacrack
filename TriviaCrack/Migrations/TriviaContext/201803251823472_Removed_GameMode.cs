namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Removed_GameMode : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Game", "GameMode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Game", "GameMode", c => c.String());
        }
    }
}
