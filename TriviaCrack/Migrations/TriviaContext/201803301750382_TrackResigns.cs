namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrackResigns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Game", "DidResign", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Game", "DidResign");
        }
    }
}
