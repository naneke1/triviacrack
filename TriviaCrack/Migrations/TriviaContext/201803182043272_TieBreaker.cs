namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TieBreaker : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClassicChallengeQuestion", "TiebreakerQuestionId", c => c.Int());
            CreateIndex("dbo.ClassicChallengeQuestion", "TiebreakerQuestionId");
            AddForeignKey("dbo.ClassicChallengeQuestion", "TiebreakerQuestionId", "dbo.Question", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClassicChallengeQuestion", "TiebreakerQuestionId", "dbo.Question");
            DropIndex("dbo.ClassicChallengeQuestion", new[] { "TiebreakerQuestionId" });
            DropColumn("dbo.ClassicChallengeQuestion", "TiebreakerQuestionId");
        }
    }
}
