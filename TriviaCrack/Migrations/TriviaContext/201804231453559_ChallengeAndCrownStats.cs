namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChallengeAndCrownStats : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Statistics", "IsCrownQuestion", c => c.Boolean(nullable: false));
            AddColumn("dbo.Statistics", "IsChallengeQuestion", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Statistics", "IsChallengeQuestion");
            DropColumn("dbo.Statistics", "IsCrownQuestion");
        }
    }
}
