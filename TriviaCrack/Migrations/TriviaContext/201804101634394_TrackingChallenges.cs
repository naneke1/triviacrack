namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrackingChallenges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Player", "ChallengeWins", c => c.Int(nullable: false));
            AddColumn("dbo.Player", "ChallengeCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Player", "ChallengeCount");
            DropColumn("dbo.Player", "ChallengeWins");
        }
    }
}
