namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AchievementStatusTracking : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AchievementStatus",
                c => new
                    {
                        PlayerId = c.String(nullable: false, maxLength: 128),
                        ArtConnoisseurAchieved = c.Boolean(nullable: false),
                        BrainiacAchieved = c.Boolean(nullable: false),
                        ChampionAchieved = c.Boolean(nullable: false),
                        HistoryBuffAchieved = c.Boolean(nullable: false),
                        MapScholarAchieved = c.Boolean(nullable: false),
                        MoneybagsAchieved = c.Boolean(nullable: false),
                        PopCultureWhizAchieved = c.Boolean(nullable: false),
                        QuizMasterAchieved = c.Boolean(nullable: false),
                        ScienceNerdAchieved = c.Boolean(nullable: false),
                        SportsNutAchieved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerId)
                .ForeignKey("dbo.Player", t => t.PlayerId)
                .Index(t => t.PlayerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AchievementStatus", "PlayerId", "dbo.Player");
            DropIndex("dbo.AchievementStatus", new[] { "PlayerId" });
            DropTable("dbo.AchievementStatus");
        }
    }
}
