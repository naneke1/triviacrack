namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlayerCoins : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Player", "Coins", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Player", "Coins");
        }
    }
}
