namespace TriviaCrack.Migrations.TriviaContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Merging : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Description = c.String(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                    "dbo.Answer",
                    c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.CategoriesCollected",
                c => new
                {
                    GameId = c.Int(nullable: false),
                    PlayerId = c.String(nullable: false, maxLength: 128),
                    ArtCollected = c.Boolean(nullable: false),
                    EntertainmentCollected = c.Boolean(nullable: false),
                    SportsCollected = c.Boolean(nullable: false),
                    GeographyCollected = c.Boolean(nullable: false),
                    HistoryCollected = c.Boolean(nullable: false),
                    ScienceCollected = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => new { t.GameId, t.PlayerId })
                .ForeignKey("dbo.Game", t => t.GameId, cascadeDelete: true)
                .ForeignKey("dbo.Player", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.GameId)
                .Index(t => t.PlayerId);

            CreateTable(
                "dbo.Game",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Player1Id = c.String(maxLength: 128),
                    Player2Id = c.String(maxLength: 128),
                    StartDate = c.DateTime(nullable: false),
                    EndDate = c.DateTime(),
                    IsActive = c.Boolean(nullable: false),
                    WinnerId = c.String(maxLength: 128),
                    PlayerTurn = c.String(maxLength: 128),
                    GameMode = c.String(),
                    ChallengeInProgress = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Player", t => t.PlayerTurn)
                .ForeignKey("dbo.Player", t => t.Player1Id)
                .ForeignKey("dbo.Player", t => t.Player2Id)
                .ForeignKey("dbo.Player", t => t.WinnerId)
                .Index(t => t.Player1Id)
                .Index(t => t.Player2Id)
                .Index(t => t.WinnerId)
                .Index(t => t.PlayerTurn);

            CreateTable(
                "dbo.Player",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 128),
                    UserName = c.String(),
                    OverallScore = c.Int(nullable: false),
                    Level = c.Int(nullable: false),
                    PrevScoreThreshold = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Question",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Text = c.String(),
                    CorrectAnswerId = c.Int(),
                    AnswerChoice1Id = c.Int(),
                    AnswerChoice2Id = c.Int(),
                    AnswerChoice3Id = c.Int(),
                    AnswerChoice4Id = c.Int(),
                    CategoryId = c.Int(nullable: false),
                    FunRatingCount = c.Int(nullable: false),
                    BoringRatingCount = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Answer", t => t.AnswerChoice1Id)
                .ForeignKey("dbo.Answer", t => t.AnswerChoice2Id)
                .ForeignKey("dbo.Answer", t => t.AnswerChoice3Id)
                .ForeignKey("dbo.Answer", t => t.AnswerChoice4Id)
                .ForeignKey("dbo.Category", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Answer", t => t.CorrectAnswerId)
                .Index(t => t.CorrectAnswerId)
                .Index(t => t.AnswerChoice1Id)
                .Index(t => t.AnswerChoice2Id)
                .Index(t => t.AnswerChoice3Id)
                .Index(t => t.AnswerChoice4Id)
                .Index(t => t.CategoryId);

            CreateTable(
                "dbo.Streak",
                c => new
                {
                    GameId = c.Int(nullable: false),
                    PlayerId = c.String(nullable: false, maxLength: 128),
                    PlayerStreak = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.GameId, t.PlayerId })
                .ForeignKey("dbo.Game", t => t.GameId, cascadeDelete: true)
                .ForeignKey("dbo.Player", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.GameId)
                .Index(t => t.PlayerId);

            CreateTable(
                "dbo.SubmittedQuestion",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Text = c.String(nullable: false),
                    CorrectAnswer = c.String(nullable: false),
                    IncorrectAnswer1 = c.String(nullable: false),
                    IncorrectAnswer2 = c.String(nullable: false),
                    IncorrectAnswer3 = c.String(nullable: false),
                    CategoryName = c.String(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Statistics",
                c => new
                    {
                        GameId = c.Int(nullable: false),
                        PlayerId = c.String(nullable: false, maxLength: 128),
                        QuestionId = c.Int(nullable: false),
                        IsAnsweredCorrectly = c.Boolean(nullable: false),
                        DateAnswered = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.GameId, t.PlayerId, t.QuestionId })
                .ForeignKey("dbo.Game", t => t.GameId, cascadeDelete: true)
                .ForeignKey("dbo.Player", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Question", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.GameId)
                .Index(t => t.PlayerId)
                .Index(t => t.QuestionId);

            CreateTable(
                "dbo.ClassicChallengeQuestion",
                c => new
                {
                    GameId = c.Int(nullable: false),
                    PlayerId = c.String(nullable: false, maxLength: 128),
                    Question1Id = c.Int(),
                    Question2Id = c.Int(),
                    Question3Id = c.Int(),
                    Question4Id = c.Int(),
                    Question5Id = c.Int(),
                    Question6Id = c.Int(),
                    TotalCorrect = c.Int(nullable: false),
                    CurrentQuestion = c.Int(nullable: false),
                    CategoryAtStake = c.String(),
                })
                .PrimaryKey(t => new { t.GameId, t.PlayerId })
                .ForeignKey("dbo.Game", t => t.GameId, cascadeDelete: true)
                .ForeignKey("dbo.Player", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Question", t => t.Question1Id)
                .ForeignKey("dbo.Question", t => t.Question2Id)
                .ForeignKey("dbo.Question", t => t.Question3Id)
                .ForeignKey("dbo.Question", t => t.Question4Id)
                .ForeignKey("dbo.Question", t => t.Question5Id)
                .ForeignKey("dbo.Question", t => t.Question6Id)
                .Index(t => t.GameId)
                .Index(t => t.PlayerId)
                .Index(t => t.Question1Id)
                .Index(t => t.Question2Id)
                .Index(t => t.Question3Id)
                .Index(t => t.Question4Id)
                .Index(t => t.Question5Id)
                .Index(t => t.Question6Id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Streak", "PlayerId", "dbo.Player");
            DropForeignKey("dbo.Streak", "GameId", "dbo.Game");
            DropForeignKey("dbo.Question", "CorrectAnswerId", "dbo.Answer");
            DropForeignKey("dbo.Question", "CategoryId", "dbo.Category");
            DropForeignKey("dbo.Question", "AnswerChoice4Id", "dbo.Answer");
            DropForeignKey("dbo.Question", "AnswerChoice3Id", "dbo.Answer");
            DropForeignKey("dbo.Question", "AnswerChoice2Id", "dbo.Answer");
            DropForeignKey("dbo.Question", "AnswerChoice1Id", "dbo.Answer");
            DropForeignKey("dbo.CategoriesCollected", "PlayerId", "dbo.Player");
            DropForeignKey("dbo.CategoriesCollected", "GameId", "dbo.Game");
            DropForeignKey("dbo.Game", "WinnerId", "dbo.Player");
            DropForeignKey("dbo.Game", "Player2Id", "dbo.Player");
            DropForeignKey("dbo.Game", "Player1Id", "dbo.Player");
            DropForeignKey("dbo.Game", "PlayerTurn", "dbo.Player");
            DropIndex("dbo.Streak", new[] { "PlayerId" });
            DropIndex("dbo.Streak", new[] { "GameId" });
            DropIndex("dbo.Question", new[] { "CategoryId" });
            DropIndex("dbo.Question", new[] { "AnswerChoice4Id" });
            DropIndex("dbo.Question", new[] { "AnswerChoice3Id" });
            DropIndex("dbo.Question", new[] { "AnswerChoice2Id" });
            DropIndex("dbo.Question", new[] { "AnswerChoice1Id" });
            DropIndex("dbo.Question", new[] { "CorrectAnswerId" });
            DropIndex("dbo.Game", new[] { "PlayerTurn" });
            DropIndex("dbo.Game", new[] { "WinnerId" });
            DropIndex("dbo.Game", new[] { "Player2Id" });
            DropIndex("dbo.Game", new[] { "Player1Id" });
            DropIndex("dbo.CategoriesCollected", new[] { "PlayerId" });
            DropIndex("dbo.CategoriesCollected", new[] { "GameId" });
            DropTable("dbo.SubmittedQuestion");
            DropTable("dbo.Streak");
            DropTable("dbo.Question");
            DropTable("dbo.Player");
            DropTable("dbo.Game");
            DropTable("dbo.CategoriesCollected");
            DropTable("dbo.Category");
            DropTable("dbo.Answer");
            DropForeignKey("dbo.Statistics", "QuestionId", "dbo.Question");
            DropForeignKey("dbo.Statistics", "PlayerId", "dbo.Player");
            DropForeignKey("dbo.Statistics", "GameId", "dbo.Game");
            DropIndex("dbo.Statistics", new[] { "QuestionId" });
            DropIndex("dbo.Statistics", new[] { "PlayerId" });
            DropIndex("dbo.Statistics", new[] { "GameId" });
            DropTable("dbo.Statistics");
            DropForeignKey("dbo.ClassicChallengeQuestion", "Question6Id", "dbo.Question");
            DropForeignKey("dbo.ClassicChallengeQuestion", "Question5Id", "dbo.Question");
            DropForeignKey("dbo.ClassicChallengeQuestion", "Question4Id", "dbo.Question");
            DropForeignKey("dbo.ClassicChallengeQuestion", "Question3Id", "dbo.Question");
            DropForeignKey("dbo.ClassicChallengeQuestion", "Question2Id", "dbo.Question");
            DropForeignKey("dbo.ClassicChallengeQuestion", "Question1Id", "dbo.Question");
            DropForeignKey("dbo.ClassicChallengeQuestion", "PlayerId", "dbo.Player");
            DropForeignKey("dbo.ClassicChallengeQuestion", "GameId", "dbo.Game");
            DropIndex("dbo.ClassicChallengeQuestion", new[] { "Question6Id" });
            DropIndex("dbo.ClassicChallengeQuestion", new[] { "Question5Id" });
            DropIndex("dbo.ClassicChallengeQuestion", new[] { "Question4Id" });
            DropIndex("dbo.ClassicChallengeQuestion", new[] { "Question3Id" });
            DropIndex("dbo.ClassicChallengeQuestion", new[] { "Question2Id" });
            DropIndex("dbo.ClassicChallengeQuestion", new[] { "Question1Id" });
            DropIndex("dbo.ClassicChallengeQuestion", new[] { "PlayerId" });
            DropIndex("dbo.ClassicChallengeQuestion", new[] { "GameId" });
            DropTable("dbo.ClassicChallengeQuestion");
        }
    }
}
