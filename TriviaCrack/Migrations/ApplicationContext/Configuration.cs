namespace TriviaCrack.Migrations.ApplicationContext
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TriviaCrack.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"Migrations\ApplicationContext";
        }

        protected override void Seed(TriviaCrack.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

        /*    Data_Access.TriviaContext db = new Data_Access.TriviaContext();

            
            if (!context.Roles.Any(r => r.Name == "Administrator"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Administrator" };

                manager.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == "admin@triviacrack.com"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { Id = "cc19f9ef-25e1-4a5a-94b7-6c1da239c9ef", UserName = "admin@triviacrack.com", Email = "admin@triviacrack.com" };
                //user.UserPhoto = defaultPhoto;
                manager.Create(user, ConfigurationManager.AppSettings["AdminPassword"]);
                manager.AddToRole(user.Id, "Administrator");
            }

            if (!context.Users.Any(u => u.UserName == "admin2@triviacrack.com"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user =  new ApplicationUser { Id = "138c65ea-7a09-4b14-b1a6-e54f03f57d11",  UserName = "admin2@triviacrack.com", Email = "admin2@triviacrack.com" };
                //user.UserPhoto = ;
                manager.Create(user, ConfigurationManager.AppSettings["AdminPassword"]);
                manager.AddToRole(user.Id, "Administrator");
            }
         */   
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
