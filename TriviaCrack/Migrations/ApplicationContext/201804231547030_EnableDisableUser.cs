namespace TriviaCrack.Migrations.ApplicationContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnableDisableUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsEnabled", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsEnabled");
        }
    }
}
