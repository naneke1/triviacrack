﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using Moq;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;

namespace TriviaCrack.Services.Tests
{
    [TestClass()]
    public class PlayerServiceTests
    {
        private List<Player> players;
        private PlayerService playerService;
        private Mock<TriviaContext> mockContext;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.players = testData.Players;

            var playersMockSet = new PlayerMockSet(this.players).GetDbSet<Player>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Players).Returns(playersMockSet.Object);
            this.playerService = new PlayerService(mockContext.Object);
        }

        [TestMethod()]
        public void FindPlayerByIdTest()
        {
            // Arrange
            // player with id of "a1" initialized in initializer

            // Act
            var player = this.playerService.FindPlayerById("a1");

            // Assert
            Assert.AreEqual("a1", player.Id);
        }

        [TestMethod()]
        public void FindPlayerByUsernameTest()
        {
            // Arrange
            // player with username of "admin-1" initialized in initializer

            // Act
            var player = this.playerService.FindPlayerByUsername("player-1");

            // Assert
            Assert.AreEqual("player-1", player.UserName);
        }

        [TestMethod()]
        public void IncrementOverallScoreTest()
        {
            // Arrange
            // player with overall score of 30 initialized in initializer

            // Act
            var player = this.playerService.FindPlayerByUsername("player-1");
            this.playerService.IncrementStats("a1");

            // Assert
            Assert.AreEqual(31, player.OverallScore);
        }
    }
}