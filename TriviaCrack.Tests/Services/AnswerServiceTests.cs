﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using TriviaCrack.Models;
using Moq;
using TriviaCrack.Data_Access;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;

namespace TriviaCrack.Services.Tests
{
    [TestClass()]
    public class AnswerServiceTests
    {
        private List<Answer> answers;
        private AnswerService answerService;
        private Mock<TriviaContext> mockContext;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.answers = testData.Answers;

            var answersMockSet = new AnswerMockSet(this.answers).GetDbSet<Answer>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Answers).Returns(answersMockSet.Object);

            this.answerService = new AnswerService(mockContext.Object);
        }

        [TestMethod()]
        public void Find_Existing_Answer_Test()
        {
            // Arrange
            // Answer with text of "1990" initialized in constructor with an id of 1

            // Act
            var answerId = this.answerService.GetAnswerId("1990");

            // Assert
            Assert.AreEqual(1, answerId);
        }

        [TestMethod()]
        public void ShuffleAnswerChoicesTest()
        {
            // Arrange
            // Answer with text of "1990" initialized at index 0 in initializer

            // Act
            string[] allAnswers = { answers[0].Text, answers[1].Text, answers[2].Text, answers[3].Text };
            string[] unshuffledAnswers = { answers[0].Text, answers[1].Text, answers[2].Text, answers[3].Text };

            this.answerService.ShuffleAnswerChoices(allAnswers);

            bool arraysAreEqual = Enumerable.SequenceEqual(allAnswers, unshuffledAnswers);

            // Assert
            Assert.AreEqual(false, arraysAreEqual);
        }
    }
}