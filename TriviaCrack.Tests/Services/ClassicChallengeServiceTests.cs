﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using TriviaCrack.Models;
using Moq;
using TriviaCrack.Data_Access;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;

namespace TriviaCrack.Services.Tests
{
    [TestClass()]
    public class ClassicChallengeServiceTests
    {
        private Player loggedInPlayer;
        private Player opponentPlayer;
        private Game newGame;

        private ClassicChallengeService classicChallengeQuestionService;
        private QuestionService questionService;
        private CategoryCollectService categoryCollectService;

        private Mock<TriviaContext> mockContext;

        private List<ClassicChallengeQuestion> classicChallengeQuestions;
        private List<Question> listOfQuestions;
        private List<CategoriesCollected> categoriesCollected;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.loggedInPlayer = testData.LoggedInPlayer;
            this.opponentPlayer = testData.OpponentPlayer;
            this.newGame = testData.Games[0];
            this.listOfQuestions = testData.Questions;
            this.classicChallengeQuestions = testData.ClassicChallengeQuestions;
            this.categoriesCollected = testData.CategoriesCollected;

            var categoriesCollectedMockSet = new CategoriesCollectedMockSet(this.categoriesCollected)
                                                .GetDbSet<CategoriesCollected>();
            var questionsMockSet = new QuestionMockSet(this.listOfQuestions).GetDbSet<Question>();
            var classicChallengeQuestionsMockSet = new ClassicChallengeQuestionMockSet(this.classicChallengeQuestions)
                                            .GetDbSet<ClassicChallengeQuestion>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.CategoriesCollected).Returns(categoriesCollectedMockSet.Object);
            mockContext.Setup(c => c.Questions).Returns(questionsMockSet.Object);
            mockContext.Setup(c => c.ClassicChallengeQuestions).Returns(classicChallengeQuestionsMockSet.Object);

            this.categoryCollectService = new CategoryCollectService(mockContext.Object);
            this.questionService = new QuestionService(mockContext.Object);
            this.classicChallengeQuestionService = new ClassicChallengeService(mockContext.Object, this.categoryCollectService, this.questionService);
        }

        [TestMethod()]
        public void CreateChallengeTest()
        {
            // Arrange
            // classicChallengeQuestions.Count() initialized to 3 in initializer

            var game = new Game {
                            Id = 8,
                            Player1Id = this.loggedInPlayer.Id,
                            Player1 = this.loggedInPlayer,
                            Player2 = this.opponentPlayer,
                            Player2Id = this.opponentPlayer.Id,
                            StartDate = Convert.ToDateTime(DateTime.UtcNow.ToString("MM/dd/yyyy")),
                            IsActive = true,
                            ChallengeInProgress = false         //game challengeInProgress set to false
             };

            // Act
            this.classicChallengeQuestionService.CreateChallenge(this.loggedInPlayer.Id, game, this.opponentPlayer, "Art", "Art");

            // Assert
            Assert.AreEqual(true, game.ChallengeInProgress);
            Assert.AreEqual(4, this.classicChallengeQuestions.Count());
        }

        [TestMethod()]
        public void EndChallengeTest()
        {
            // Arrange
            // classicChallengeQuestions.Count() initialized to 1 in initializer

            var game = new Game
            {
                Id = 8,
                Player1Id = this.loggedInPlayer.Id,
                Player1 = this.loggedInPlayer,
                Player2Id = this.opponentPlayer.Id,
                Player2 = this.opponentPlayer,
                StartDate = Convert.ToDateTime(DateTime.UtcNow.ToString("MM/dd/yyyy")),
                IsActive = true,
                ChallengeInProgress = false         //game challengeInProgress set to false
            };

            // Act
            this.classicChallengeQuestionService.CreateChallenge(this.loggedInPlayer.Id, game, this.opponentPlayer, "Art", "Art");

            // classicChallengeQuestions.Count() now equals 4, challengeInProgress equals true

            // Assert
            Assert.AreEqual(true, game.ChallengeInProgress);
            Assert.AreEqual(4, this.classicChallengeQuestions.Count());

            // Arrange 
            this.categoryCollectService.Create(game.Id, loggedInPlayer.Id);
            this.categoryCollectService.Create(game.Id, opponentPlayer.Id);

            var loggedInPlayerChallengeData = this.classicChallengeQuestionService.FindLoggedInPlayerChallengeData(game.Id, loggedInPlayer.Id);
            var opponentPlayerChallengeData = this.classicChallengeQuestionService.FindLoggedInPlayerChallengeData(game.Id, opponentPlayer.Id);

            // Act
            this.classicChallengeQuestionService.EndChallenge(loggedInPlayerChallengeData, opponentPlayerChallengeData, game);

            // Assert
            Assert.AreEqual(false, game.ChallengeInProgress);
            Assert.AreEqual(opponentPlayer.Id, game.PlayerTurn);

        }

        [TestMethod()]
        public void HandleCorrectAnswerTest()
        {
            // Arrange
            // Total correct and CurrentQuestion for loggedInPlayer default to 0 in initializer

            // Act
            this.classicChallengeQuestionService.HandleCorrectAnswer(this.newGame.Id, this.loggedInPlayer.Id);
            var loggedInPlayerChallengeData = this.classicChallengeQuestionService.FindLoggedInPlayerChallengeData(this.newGame.Id, this.loggedInPlayer.Id);

            // Assert
            Assert.AreEqual(1, loggedInPlayerChallengeData.TotalCorrect);
        }

        [TestMethod()]
        public void FindLoggedInPlayerChallengeDataTest()
        {
            // Arrange
            // GameId = 5, playerId = "a1", Question1Id, = 1, Question2Id, = 2, Question3Id, = 3, Question4Id, = 4, Question5Id, = 5, Question6Id, = 6 initialized in initializer 

            // Act
            var loggedInPlayerChallengeData = this.classicChallengeQuestionService.FindLoggedInPlayerChallengeData(this.newGame.Id, this.loggedInPlayer.Id);

            // Assert
            Assert.AreEqual(5, loggedInPlayerChallengeData.GameId);
            Assert.AreEqual("a1", loggedInPlayerChallengeData.PlayerId);
            Assert.AreEqual(1, loggedInPlayerChallengeData.Question1Id);
            Assert.AreEqual(2, loggedInPlayerChallengeData.Question2Id);
            Assert.AreEqual(3, loggedInPlayerChallengeData.Question3Id);
            Assert.AreEqual(4, loggedInPlayerChallengeData.Question4Id);
            Assert.AreEqual(8, loggedInPlayerChallengeData.Question5Id);
            Assert.AreEqual(9, loggedInPlayerChallengeData.Question6Id);
        }

        [TestMethod()]
        public void SwitchTurnsTest()
        {
            // Arrange
            // classicChallengeQuestions.Count() initialized to 1 in initializer

            var game = new Game
            {
                Id = 8,
                Player1Id = this.loggedInPlayer.Id,
                Player1 = this.loggedInPlayer,
                Player2Id = this.opponentPlayer.Id,
                Player2 = this.opponentPlayer,
                StartDate = Convert.ToDateTime(DateTime.UtcNow.ToString("MM/dd/yyyy")),
                IsActive = true,
                ChallengeInProgress = false         //game challengeInProgress set to false
            };

            // Act
            this.classicChallengeQuestionService.CreateChallenge(this.loggedInPlayer.Id, game, this.opponentPlayer, "Art", "Art");

            this.categoryCollectService.Create(game.Id, loggedInPlayer.Id);
            this.categoryCollectService.Create(game.Id, opponentPlayer.Id);

            var loggedInPlayerChallengeData = this.classicChallengeQuestionService.FindLoggedInPlayerChallengeData(game.Id, loggedInPlayer.Id);
            var opponentPlayerChallengeData = this.classicChallengeQuestionService.FindLoggedInPlayerChallengeData(game.Id, opponentPlayer.Id);

            this.classicChallengeQuestionService.SwitchTurns(game, loggedInPlayer.Id);

            // Assert
            Assert.AreEqual(opponentPlayer.Id, game.PlayerTurn);
        }

        [TestMethod()]
        public void GetNextQuestionTest()
        {
            // Arrange
            // Total correct and CurrentQuestion for loggedInPlayer default to 0 in initializer

            // Act
            this.classicChallengeQuestionService.HandleCorrectAnswer(this.newGame.Id, this.loggedInPlayer.Id);
            var loggedInPlayerChallengeData = this.classicChallengeQuestionService.FindLoggedInPlayerChallengeData(this.newGame.Id, this.loggedInPlayer.Id);

            var loggedInPlayerNextQuestion = this.classicChallengeQuestionService.GetNextQuestion(loggedInPlayerChallengeData);

            // Assert
            Assert.AreEqual(1, loggedInPlayerNextQuestion.Id);
        }
    }
}