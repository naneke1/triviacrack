﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using Moq;
using TriviaCrack.Tests.MockSets;
using TriviaCrack.Tests.Test_Data;

namespace TriviaCrack.Services.Tests
{
    [TestClass()]
    public class CategoryCollectServiceTests
    {
        private Game newGame;
        private Player loggedInPlayer;
        private Player opponentPlayer;

        private CategoryCollectService categoryCollectService;
        private Mock<TriviaContext> mockContext;
        private List<CategoriesCollected> categoriesCollected;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.categoriesCollected = testData.CategoriesCollected;
            this.newGame = testData.Games[0];
            this.loggedInPlayer = testData.Players[0];
            this.opponentPlayer = testData.Players[1];

            var categoriesCollectedMockSet = new CategoriesCollectedMockSet(this.categoriesCollected)
                                                .GetDbSet<CategoriesCollected>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.CategoriesCollected).Returns(categoriesCollectedMockSet.Object);
           
            this.categoryCollectService = new CategoryCollectService(mockContext.Object);
        }

        [TestMethod()]
        public void Create_Categories_CollectedTest()
        {
            // Arrange
            //categoriesCollected list set to a size of 2 in Initializer

            // Act
            var result = this.categoryCollectService.Create(this.newGame.Id, this.opponentPlayer.Id);

            // Assert
            Assert.AreEqual(3, this.categoriesCollected.Count());
        }

        [TestMethod()]
        public void FindLoggedInPlayerCategoriesCollectedTest()
        {
            // Arrange
            //categoriesCollected list has loggedInPlayer categoriesCollected row set in Initializer

            // Act
            var result = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(this.newGame.Id, this.loggedInPlayer.Id);

            // Assert
            Assert.AreEqual(5, result.GameId);
            Assert.AreEqual("a1", result.PlayerId);
            Assert.AreEqual(false, result.ArtCollected);
            Assert.AreEqual(false, result.EntertainmentCollected);
            Assert.AreEqual(false, result.SportsCollected);
            Assert.AreEqual(false, result.HistoryCollected);
            Assert.AreEqual(false, result.GeographyCollected);
            Assert.AreEqual(false, result.ScienceCollected);
        }

        [TestMethod()]
        public void FindOpponentCategoriesCollectedTest()
        {
            // Act
            var result = this.categoryCollectService.FindOpponentCategoriesCollected(this.newGame.Id, this.loggedInPlayer.Id);

            // Assert
            Assert.AreEqual(5, result.GameId);
            Assert.AreEqual("a2", result.PlayerId);
            Assert.AreEqual(false, result.ArtCollected);
            Assert.AreEqual(false, result.EntertainmentCollected);
            Assert.AreEqual(false, result.SportsCollected);
            Assert.AreEqual(false, result.HistoryCollected);
            Assert.AreEqual(false, result.GeographyCollected);
            Assert.AreEqual(false, result.ScienceCollected);
        }

        [TestMethod()]
        public void AddCategoryTest()
        {
            // Arrange
            var playerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(this.newGame.Id, this.loggedInPlayer.Id);

            // Act
            this.categoryCollectService.AddCategory(playerCategories, "Sports");

            // Assert
            Assert.AreEqual(false, playerCategories.ArtCollected);
            Assert.AreEqual(false, playerCategories.EntertainmentCollected);
            Assert.AreEqual(true, playerCategories.SportsCollected);            //Sports category added
            Assert.AreEqual(false, playerCategories.HistoryCollected);
            Assert.AreEqual(false, playerCategories.GeographyCollected);
            Assert.AreEqual(false, playerCategories.ScienceCollected);
        }

        [TestMethod()]
        public void RemoveCategoryTest()
        {
            // Arrange
            var playerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(this.newGame.Id, this.loggedInPlayer.Id);

            // Act
            this.categoryCollectService.AddCategory(playerCategories, "Sports");
            this.categoryCollectService.AddCategory(playerCategories, "History");

            // Assert
            Assert.AreEqual(true, playerCategories.SportsCollected);            //Sports category added
            Assert.AreEqual(true, playerCategories.HistoryCollected);           //History category added

            // Act
            this.categoryCollectService.RemoveCategory(playerCategories, "Sports");

            // Assert
            Assert.AreEqual(false, playerCategories.SportsCollected);            //Sports category removed
            Assert.AreEqual(true, playerCategories.HistoryCollected);           //History category kept
        }

        [TestMethod()]
        public void GetCategoryCount_All_CategoriesCollected_Set_To_False_Test()
        {
            // Arrange
            // All categories initially set to false, count is 0
            var playerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(this.newGame.Id, this.loggedInPlayer.Id);

            // Act
            var categoriesCount = this.categoryCollectService.GetCategoryCount(playerCategories);

            // Assert
            Assert.AreEqual(0, categoriesCount);
        }

        [TestMethod()]
        public void GetCategoryCount_Two_Categories_Collected_Test()
        {
            // Arrange
            // All categories initially set to false, count is 0
            var playerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(this.newGame.Id, this.loggedInPlayer.Id);

            // Act
            this.categoryCollectService.AddCategory(playerCategories, "Sports");
            this.categoryCollectService.AddCategory(playerCategories, "History");
            var categoriesCount = this.categoryCollectService.GetCategoryCount(playerCategories);

            // Assert
            Assert.AreEqual(2, categoriesCount);
        }

        [TestMethod()]
        public void PlayerHasCategory_No_Categories_Collected_Test()
        {
            // Arrange
            // All categories initially set to false, count is 0
            var playerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(this.newGame.Id, this.loggedInPlayer.Id);

            // Act
            var hasCategories = this.categoryCollectService.PlayerHasCategory(playerCategories);

            // Assert
            Assert.AreEqual(false, hasCategories);
        }

        [TestMethod()]
        public void PlayerHasCategory_Three_Categories_Collected_Test()
        {
            // Arrange
            // All categories initially set to false
            var playerCategories = this.categoryCollectService.FindLoggedInPlayerCategoriesCollected(this.newGame.Id, this.loggedInPlayer.Id);

            // Act
            this.categoryCollectService.AddCategory(playerCategories, "Sports");
            this.categoryCollectService.AddCategory(playerCategories, "History");
            this.categoryCollectService.AddCategory(playerCategories, "Art");
            var hasCategories = this.categoryCollectService.PlayerHasCategory(playerCategories);

            // Assert
            Assert.AreEqual(true, hasCategories);
        }
    }
}