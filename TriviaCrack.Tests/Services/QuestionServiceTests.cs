﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using Moq;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;

namespace TriviaCrack.Services.Tests
{
    [TestClass()]
    public class QuestionServiceTests
    {
        private List<Question> listOfQuestions;
        private QuestionService questionService;
        private Mock<TriviaContext> mockContext;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.listOfQuestions = testData.Questions;

            var questionsMockSet = new QuestionMockSet(this.listOfQuestions).GetDbSet<Question>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Questions).Returns(questionsMockSet.Object);
            this.questionService = new QuestionService(mockContext.Object);
        }

        [TestMethod()]
        public void FindQuestionByIdTest()
        {
            //Arrange
            //question with text "What year is the current year?" set to an id of 3

            // Act
            var result = this.questionService.FindQuestionById(3);

            // Assert
            Assert.AreEqual("What is the current year?", result.Text);
            Assert.AreEqual(3, result.Id);
        }

        [TestMethod()]
        public void FindQuestionByCategoryTest()
        {
            //Arrange
            //question with text "What year is the current year?" set to an id of 3

            // Act
            //var result = this.questionService.FindQuestionByCategory("Sports");
        }

        [TestMethod()]
        public void UpdateFunRatingTest()
        {
            //Arrange
            //questions initialized with a fun rating of 0

            // Act
            var result = this.questionService.FindQuestionById(3);
            this.questionService.UpdateRating(result, "fun");

            // Assert
            Assert.AreEqual(1, result.FunRatingCount);
        }

        [TestMethod()]
        public void UpdateFunRating_Should_Fail_Test()
        {
            //Arrange
            //questions initialized with a fun rating of 0

            // Act
            var result = this.questionService.FindQuestionById(3);
            this.questionService.UpdateRating(result, "fun");

            // Assert
            Assert.AreNotEqual(0, result.FunRatingCount);
        }

        [TestMethod()]
        public void UpdateBoringRatingTest()
        {
            //Arrange
            //questions initialized with a boring rating of 0

            // Act
            var result = this.questionService.FindQuestionById(3);
            this.questionService.UpdateRating(result, "boring");

            // Assert
            Assert.AreEqual(1, result.BoringRatingCount);
        }

        [TestMethod()]
        public void UpdateBoringRating_Should_Fail_Test()
        {
            //Arrange
            //questions initialized with a boring rating of 0

            // Act
            var result = this.questionService.FindQuestionById(3);
            this.questionService.UpdateRating(result, "boring");

            // Assert
            Assert.AreNotEqual(0, result.BoringRatingCount);
        }

        [TestMethod()]
        public void CreateQuestionTest()
        {
            //Arrange
            //List of questions with size of 7 initialized in Initializer
           this.questionService.Create("Did the creation work?", 1, 1, 2, 3, 4, 5, "Easy");

            // Act
            var result = this.listOfQuestions.Count;

            // Assert
            Assert.AreEqual(11, result);
        }
    }
}