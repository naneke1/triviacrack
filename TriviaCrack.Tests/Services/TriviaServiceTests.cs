﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriviaCrack.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriviaCrack.Models;
using Moq;
using TriviaCrack.Data_Access;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;
using TriviaCrack.ViewModels;

namespace TriviaCrack.Services.Tests
{
    [TestClass()]
    public class TriviaServiceTests
    {
        private Mock<TriviaContext> mockContext;
        private List<Player> players;
        private List<Game> games;
        private List<Statistics> stats;
        private List<CategoriesCollected> categoriesCollected;
        private List<ClassicChallengeQuestion> classicChallengeQuestions;
        private List<Streak> streaks;
        private List<Question> questions;
        private List<PowerUp> powerUps;
        private TriviaService triviaService;
        private TestData testData;

        [TestInitialize]
        public void Initialize()
        {
            this.testData = new TestData();
            this.players = testData.Players;
            this.games = testData.Games;
            this.stats = testData.Statistics;
            this.categoriesCollected = testData.CategoriesCollected;
            this.streaks = testData.Streaks;
            this.questions = testData.Questions;
            this.classicChallengeQuestions = testData.ClassicChallengeQuestions;
            this.powerUps = testData.PowerUps;

            var playersMockSet = new PlayerMockSet(this.players).GetDbSet<Player>();
            var gamesMockSet = new GameMockSet(this.games).GetDbSet<Game>();
            var statsMockSet = new StatisticMockSet(this.stats).GetDbSet<Statistics>();
            var categoriesCollectedMockSet = new CategoriesCollectedMockSet(this.categoriesCollected).GetDbSet<CategoriesCollected>();
            var streakMockSet = new StreakMockSet(this.streaks).GetDbSet<Streak>();
            var questionMockSet = new QuestionMockSet(this.questions).GetDbSet<Question>();
            var classicChallengeQuestionsMockSet = new ClassicChallengeQuestionMockSet(this.classicChallengeQuestions)
                                .GetDbSet<ClassicChallengeQuestion>();
            var powerUpMockSet = new PowerUpMockSet(this.powerUps).GetDbSet<PowerUp>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Players).Returns(playersMockSet.Object);
            mockContext.Setup(c => c.Games).Returns(gamesMockSet.Object);
            mockContext.Setup(c => c.Statistics).Returns(statsMockSet.Object);
            mockContext.Setup(c => c.CategoriesCollected).Returns(categoriesCollectedMockSet.Object);
            mockContext.Setup(c => c.Streaks).Returns(streakMockSet.Object);
            mockContext.Setup(c => c.Questions).Returns(questionMockSet.Object);
            mockContext.Setup(c => c.ClassicChallengeQuestions).Returns(classicChallengeQuestionsMockSet.Object);
            mockContext.Setup(c => c.PowerUps).Returns(powerUpMockSet.Object);

            this.triviaService = new TriviaService(mockContext.Object, testData.LoggedInPlayer.Id);
        }

        [TestMethod()]
        public void FindGameByIdTest()
        {
            //act
            Game game = this.triviaService.FindGameById(5);

            //assert
            Assert.AreEqual(5, game.Id);
        }

        [TestMethod()]
        public void SwitchTurnsTest()
        {
        }

        [TestMethod()]
        public void EndGameTest()
        {
            //act
            this.triviaService.EndGame(this.games[0]);

            //assert
            Assert.IsFalse(this.games[0].IsActive);
        }

        [TestMethod()]
        public void CreateNewGameTest()
        {
            //arrange
            int totalGames = this.games.Count;

            //act
            this.triviaService.CreateNewGame(this.players[1].UserName);

            //assert
            Assert.AreNotEqual(totalGames, this.games.Count);
            Assert.AreEqual(totalGames + 1, this.games.Count);
        }

        [TestMethod()]
        public void GetEmptyPlayerGamesTest()
        {
            //act
            PlayerGames emptyPlayerGames = this.triviaService.GetEmptyPlayerGames();

            //assert
            Assert.IsTrue(emptyPlayerGames.ActiveGames.Count() == 0);
            Assert.IsTrue(emptyPlayerGames.FinishedGames.Count() == 0);
        }

        [TestMethod()]
        public void FindPlayerGamesTest_One_Active_Game_Zero_Finished_Games()
        {
            //act
            PlayerGames playerGamesOneActive = this.triviaService.FindPlayerGames();

            //assert
            Assert.IsTrue(playerGamesOneActive.ActiveGames.Count() == 1);
            Assert.IsTrue(playerGamesOneActive.FinishedGames.Count() == 0);
        }

        [TestMethod()]
        public void FindPlayerGamesTest_One_Active_Game_One_Finished_Game()
        {
            //arrange
            this.games.Add(new Game
            {
                Id = 6,
                Player1Id = this.testData.LoggedInPlayer.Id,
                Player1 = this.players[0],
                Player2Id = this.testData.OpponentPlayer.Id,
                Player2 = this.players[1],
                StartDate = Convert.ToDateTime(DateTime.UtcNow.ToString("MM/dd/yyyy")),
                IsActive = false,
                PlayerTurn = this.testData.LoggedInPlayer.Id,
                ChallengeInProgress = false
            });

            //act
            PlayerGames playerGamesOneActiveOneFinished = this.triviaService.FindPlayerGames();

            //assert
            Assert.IsTrue(playerGamesOneActiveOneFinished.ActiveGames.Count() == 1);
            Assert.IsTrue(playerGamesOneActiveOneFinished.FinishedGames.Count() == 1);
        }

        [TestMethod()]
        public void CheckRoundRulesForIncorrectAnswerTest()
        {
        }

        [TestMethod()]
        public void GetOpponentTest()
        {
            //act
            Player opponent = this.triviaService.GetOpponent(this.games[0].Id);

            //assert
            Assert.AreEqual(this.testData.OpponentPlayer, opponent);
        }

        [TestMethod()]
        public void FindPlayerByUsernameTest_Find_Logged_In_Player()
        {
            //act
            Player loggedInPlayer = this.triviaService.FindPlayerByUsername(this.testData.LoggedInPlayer.UserName);

            //assert
            Assert.AreEqual(this.testData.LoggedInPlayer, loggedInPlayer);
        }

        [TestMethod()]
        public void FindPlayerByUsernameTest_Find_Opponent_Player()
        {
            //act
            Player opponentPlayer = this.triviaService.FindPlayerByUsername(this.testData.OpponentPlayer.UserName);

            //assert
            Assert.AreEqual(this.testData.OpponentPlayer, opponentPlayer);
        }

        [TestMethod()]
        public void IncrementPlayerStatsTest()
        {
        }

        [TestMethod()]
        public void AddCoinsToPlayerTest()
        {
        }

        [TestMethod()]
        public void RemoveCoinsFRomPlayerTest()
        {
        }

        [TestMethod()]
        public void UpdateQuestionRatingTest()
        {
            //arrange
            int initialFunRating = this.testData.Questions[0].FunRatingCount;

            //act
            this.triviaService.UpdateQuestionRating(this.testData.Questions[0].Id.ToString(), "fun");

            //assert
            Assert.AreNotEqual(initialFunRating, this.testData.Questions[0].FunRatingCount);
        }

        [TestMethod()]
        public void FindLoggedInPlayerStreakTest()
        {
            //act
            Streak loggedInPlayerStreak = this.triviaService.FindLoggedInPlayerStreak(this.games[0].Id);
            
            //assert
            Assert.AreEqual(2, loggedInPlayerStreak.PlayerStreak);
        }

        [TestMethod()]
        public void IncrementLoggedInPlayerStreakTest()
        {
            //arrange
            Streak initialPlayerStreak = this.triviaService.FindLoggedInPlayerStreak(this.testData.Games[0].Id);

            Assert.AreEqual(2, initialPlayerStreak.PlayerStreak);

            //act
            this.triviaService.IncrementLoggedInPlayerStreak(this.testData.Games[0].Id);

            //assert
            Assert.AreEqual(3, initialPlayerStreak.PlayerStreak);
        }

        [TestMethod()]
        public void ResetLoggedInPlayerStreakTest()
        {
            //act
            this.triviaService.ResetLoggedInPlayerStreak(this.games[0].Id);
            Streak loggedInPlayerStreak = this.triviaService.FindLoggedInPlayerStreak(this.games[0].Id);

            //assert
            Assert.AreEqual(0, loggedInPlayerStreak.PlayerStreak);
        }

        [TestMethod()]
        public void GetBothPlayerCategoriesTest()
        {
            //act
            PlayerCategories playersCategories = this.triviaService.GetBothPlayerCategories(this.testData.Games[0].Id);

            //assert
            Assert.AreEqual(this.testData.LoggedInPlayer.Id, playersCategories.LoggedInPlayerCategories.PlayerId);
            Assert.AreEqual(this.testData.OpponentPlayer.Id, playersCategories.OpponentCategories.PlayerId);
        }

        [TestMethod()]
        public void PlayerHasCategoryTest_Should_Be_False_No_Categories_Collected()
        {
            //arrange
            PlayerCategories playersCategories = this.triviaService.GetBothPlayerCategories(this.testData.Games[0].Id);
            var loggedInPlayerCategories = playersCategories.LoggedInPlayerCategories;

            //act
            bool playerHasCategory = this.triviaService.PlayerHasCategory(loggedInPlayerCategories);

            //assert
            Assert.IsFalse(playerHasCategory);
        }

        [TestMethod()]
        public void PlayerHasCategoryTest_Should_Be_True_One_Categories_Collected()
        {
            //arrange
            PlayerCategories playersCategories = this.triviaService.GetBothPlayerCategories(this.testData.Games[0].Id);
            var loggedInPlayerCategories = playersCategories.LoggedInPlayerCategories;

            //act
            loggedInPlayerCategories.ArtCollected = true;
            bool playerHasCategory = this.triviaService.PlayerHasCategory(loggedInPlayerCategories);

            //assert
            Assert.IsTrue(playerHasCategory);
        }

        [TestMethod()]
        public void AddCategoryForLoggedInPlayerTest()
        {
            //arrange
            PlayerCategories playersCategories = this.triviaService.GetBothPlayerCategories(this.testData.Games[0].Id);
            var loggedInPlayerCategories = playersCategories.LoggedInPlayerCategories;

            //act
            this.triviaService.AddCategoryForLoggedInPlayer(this.testData.Games[0].Id, "Science");

            //assert
            Assert.IsTrue(loggedInPlayerCategories.ScienceCollected);
        }

        [TestMethod()]
        public void GetCategoryCountTest()
        {
            //arrange
            PlayerCategories playersCategories = this.triviaService.GetBothPlayerCategories(this.testData.Games[0].Id);
            var loggedInPlayerCategories = playersCategories.LoggedInPlayerCategories;

            //act
            this.triviaService.AddCategoryForLoggedInPlayer(this.testData.Games[0].Id, "Science");
            this.triviaService.AddCategoryForLoggedInPlayer(this.testData.Games[0].Id, "Entertainment");
            int categoryCount = this.triviaService.GetCategoryCount(this.testData.Games[0].Id, this.testData.LoggedInPlayer.Id);
            
            //assert
            Assert.AreEqual(2, categoryCount);
        }

        [TestMethod()]
        public void FindLoggedInPlayerChallengeDataTest()
        {
            // Act
            var loggedInPlayerChallengeData = this.triviaService.FindLoggedInPlayerChallengeData(this.testData.Games[0].Id);

            // Assert
            Assert.AreEqual(5, loggedInPlayerChallengeData.GameId);
            Assert.AreEqual("a1", loggedInPlayerChallengeData.PlayerId);
            Assert.AreEqual(1, loggedInPlayerChallengeData.Question1Id);
            Assert.AreEqual(2, loggedInPlayerChallengeData.Question2Id);
            Assert.AreEqual(3, loggedInPlayerChallengeData.Question3Id);
            Assert.AreEqual(4, loggedInPlayerChallengeData.Question4Id);
            Assert.AreEqual(8, loggedInPlayerChallengeData.Question5Id);
            Assert.AreEqual(9, loggedInPlayerChallengeData.Question6Id);
        }

        [TestMethod()]
        public void FindOpponentPlayerChallengeDataTest()
        {
            // Act
            var opponentPlayerChallengeData = this.triviaService.FindOpponentPlayerChallengeData(this.testData.Games[0].Id);

            // Assert
            Assert.AreEqual(5, opponentPlayerChallengeData.GameId);
            Assert.AreEqual("a2", opponentPlayerChallengeData.PlayerId);
            Assert.AreEqual(1, opponentPlayerChallengeData.Question1Id);
            Assert.AreEqual(2, opponentPlayerChallengeData.Question2Id);
            Assert.AreEqual(3, opponentPlayerChallengeData.Question3Id);
            Assert.AreEqual(4, opponentPlayerChallengeData.Question4Id);
            Assert.AreEqual(8, opponentPlayerChallengeData.Question5Id);
            Assert.AreEqual(9, opponentPlayerChallengeData.Question6Id);
        }

        [TestMethod()]
        public void SwitchChallengeTurnsTest()
        {
            //act
            this.triviaService.SwitchChallengeTurns(this.testData.Games[0].Id);

            //assert
            Assert.AreEqual("a2", this.testData.Games[0].PlayerTurn);
        }

        [TestMethod()]
        public void GetNextChallengeQuestionTest()
        {
            //arrange
            this.testData.ClassicChallengeQuestions[0].CurrentQuestion = 4;

            //act
            this.triviaService.GetNextChallengeQuestion(this.testData.Games[0].Id);

            //assert
            Assert.AreEqual(5, this.testData.ClassicChallengeQuestions[0].CurrentQuestion);
        }

        [TestMethod()]
        public void StartNewChallengeTest()
        {
            //act
            this.triviaService.StartNewChallenge(this.testData.Games[0].Id, "Art", "Science");

            //assert
            Assert.IsTrue(this.testData.Games[0].IsActive);
        }

        [TestMethod()]
        public void HandleCorrectAnswerTest()
        {
            //act
            this.triviaService.HandleCorrectAnswer(this.testData.Games[0].Id);
            var loggedInPlayerChallengeData = this.triviaService.FindLoggedInPlayerChallengeData(this.testData.Games[0].Id);

            //assert
            Assert.AreEqual(1, loggedInPlayerChallengeData.TotalCorrect);
        }

        [TestMethod()]
        public void AddQuestionStatTest()
        {
        }

        [TestMethod()]
        public void MarkQuestionAnsweredCorrectlyTest()
        {
        }

        [TestMethod()]
        public void ReturnPlayerCategoryStatsPerGameTest()
        {
        }

        [TestMethod()]
        public void ReturnCorrectAnsweredPlayerCountTest()
        {
        }

        [TestMethod()]
        public void ReturnIncorrectAnsweredPlayerCountTest()
        {
        }

        [TestMethod()]
        public void ReturnTotalAnsweredPlayerCountTest()
        {
        }
    }
}