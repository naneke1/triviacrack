﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using Moq;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;

namespace TriviaCrack.Services.Tests
{
    [TestClass()]
    public class GameServiceTests
    {
        private List<Game> games;
        private GameService gameService;
        private Mock<TriviaContext> mockContext;
        private TestData testData;

        [TestInitialize]
        public void Initialize()
        {
            testData = new TestData();
            this.games = testData.Games;

            var gamesMockSet = new GameMockSet(this.games).GetDbSet<Game>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Games).Returns(gamesMockSet.Object);
            this.gameService = new GameService(mockContext.Object);
        }

        [TestMethod()]
        public void FindGameByIdTest()
        {
            // Arrange
            // game with an id of 5 created in initializer

            // Act
            var result = this.gameService.FindGameById(5);

            // Assert
            Assert.AreEqual(5, result.Id);
        }

        [TestMethod()]
        public void CreateTest()
        {
            // Arrange
            // games.Count() initialized to 1 in initializer
            // Act
            this.gameService.Create(testData.LoggedInPlayer, testData.OpponentPlayer);

            // Assert
            Assert.AreEqual(2, games.Count);
        }

        [TestMethod()]
        public void EndGameTest()
        {
            //Arrange
            //Game isActive set to true in initializer

            // Act
            this.gameService.EndGame(games[0], "a1");

            // Assert
            Assert.AreEqual(false, games[0].IsActive);
        }

        [TestMethod()]
        public void SwitchTurnsTest()
        {
            //Arrange
            //PlayerTurn initialized to "a1" in initializer

            // Act
            this.gameService.SwitchTurns(games[0], "a1");

            // Assert
            Assert.AreEqual("a2", games[0].PlayerTurn);
        }
    }
}