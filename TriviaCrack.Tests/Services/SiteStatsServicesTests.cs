﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TriviaCrack.Tests.Services
{
    [TestClass]
    public class SiteStatsServicesTests
    {
        [TestMethod]
        public void DetermineWeeklyStatsTest()
        {
            //Arrange
            DateTime currentDate = new DateTime(2018, 3, 27);   //DateTime.Now;
            DateTime startDate = currentDate.AddDays(-(int)currentDate.DayOfWeek);
            DateTime endDate = startDate.AddDays(6);

            //Act
            DateTime notInWeek = new DateTime(2018, 3, 20);     // Monday
            DateTime isInWeek = new DateTime(2018, 3, 27);      // Today
            DateTime isStart = new DateTime(2018, 3, 25);       // Sunday
            DateTime isEnd = new DateTime(2018, 3, 31);     // Saturday

            //Assert
            Assert.AreEqual(currentDate.DayOfWeek, DayOfWeek.Tuesday);
            Assert.AreEqual(startDate.DayOfWeek, DayOfWeek.Sunday);
            Assert.AreEqual(startDate.Day, 25);
            Assert.AreEqual(endDate.DayOfWeek, DayOfWeek.Saturday);
            Assert.AreEqual(endDate.Day, 31);

            Assert.IsFalse(notInWeek >= startDate && notInWeek <= endDate);
            Assert.IsTrue(isInWeek >= startDate && isInWeek <= endDate);
            Assert.IsTrue(isStart >= startDate && isStart <= endDate);
            Assert.IsTrue(isEnd >= startDate && isEnd <= endDate);
        }
    }
}