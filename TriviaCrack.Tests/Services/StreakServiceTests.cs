﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TriviaCrack.Models;
using Moq;
using TriviaCrack.Data_Access;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;

namespace TriviaCrack.Services.Tests
{
    [TestClass()]
    public class StreakServiceTests
    {
        private Player loggedInPlayer;
        private Player opponentPlayer;
        private Game newGame;

        private StreakService streakService;
        private Mock<TriviaContext> mockContext;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();

            this.loggedInPlayer = testData.LoggedInPlayer;
            this.opponentPlayer = testData.OpponentPlayer;
            this.newGame = testData.Games[0];

            var streaksMockSet = new StreakMockSet(testData.Streaks).GetDbSet<Streak>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Streaks).Returns(streaksMockSet.Object);
            this.streakService = new StreakService(mockContext.Object);
        }

        [TestMethod()]
        public void FindLoggedInPlayerStreakTest()
        {
            //Arrange
            //loggedInPlayer streak set to 2 in initializer

            // Act
            var result = this.streakService.FindLoggedInPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;

            // Assert
            Assert.AreEqual(2, result);
        }

        [TestMethod()]
        public void FindOpponentPlayerStreakTest()
        {
            //Arrange
            //opponentPlayer streak set to 1 in initializer

            // Act
            var result = this.streakService.FindOpponentPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;

            // Assert
            Assert.AreEqual(1, result);
        }

        [TestMethod()]
        public void ResetPlayerStreakTest()
        {
            //Arrange
            //loggedInPlayerStreak set to 2 in initializer 

            // Act
            this.streakService.ResetPlayerStreak(newGame.Id, loggedInPlayer.Id);

            var loggedInPlayerStreak = streakService.FindLoggedInPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;

            // Assert
            Assert.AreEqual(0, loggedInPlayerStreak);
        }

        [TestMethod()]
        public void ResetPlayerStreak_Should_Fail_Test()
        {
            //Arrange
            //loggedInPlayerStreak set to 2 in initializer 

            // Act
            this.streakService.ResetPlayerStreak(newGame.Id, loggedInPlayer.Id);

            var loggedInPlayerStreak = streakService.FindLoggedInPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;

            // Assert
            Assert.AreNotEqual(2, loggedInPlayerStreak);
        }

        [TestMethod()]
        public void ResetBothPlayerStreakTest()
        {
            //Arrange
            //loggedInPlayerStreak set to 2 in initializer 
            //oppponentPlayerStreak set to 1 in initializer

            // Act
            this.streakService.ResetBothPlayerStreaks(newGame.Id, loggedInPlayer.Id);

            var loggedInPlayerStreak = streakService.FindLoggedInPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;
            var opponentPlayerStreak = streakService.FindOpponentPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;

            // Assert
            Assert.AreEqual(0, loggedInPlayerStreak);
            Assert.AreEqual(0, opponentPlayerStreak);
        }

        [TestMethod()]
        public void ResetBothPlayerStreak_Should_Fail_Test()
        {
            //Arrange
            //loggedInPlayerStreak set to 2 in initializer 
            //oppponentPlayerStreak set to 1 in initializer

            // Act
            this.streakService.ResetBothPlayerStreaks(newGame.Id, loggedInPlayer.Id);

            var loggedInPlayerStreak = streakService.FindLoggedInPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;
            var opponentPlayerStreak = streakService.FindOpponentPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;

            // Assert
            Assert.AreNotEqual(2, loggedInPlayerStreak);
            Assert.AreNotEqual(1, opponentPlayerStreak);
        }

        [TestMethod()]
        public void IncrementPlayerStreakTest()
        {
            //Arrange
            //loggedInPlayerStreak set to 2 in initializer 
            //oppponentPlayerStreak set to 1 in initializer

            // Act
            this.streakService.IncrementPlayerStreak(newGame.Id, loggedInPlayer.Id);
            this.streakService.IncrementPlayerStreak(newGame.Id, opponentPlayer.Id);

            var loggedInPlayerStreak = streakService.FindLoggedInPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;
            var opponentPlayerStreak = streakService.FindOpponentPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;

            // Assert
            Assert.AreEqual(3, loggedInPlayerStreak);
            Assert.AreEqual(2, opponentPlayerStreak);
        }

        [TestMethod()]
        public void IncrementPlayerStreak_Should_Fail_Test()
        {
            //Arrange
            //loggedInPlayerStreak set to 2 in initializer 
            //oppponentPlayerStreak set to 1 in initializer

            // Act
            this.streakService.IncrementPlayerStreak(newGame.Id, loggedInPlayer.Id);
            this.streakService.IncrementPlayerStreak(newGame.Id, opponentPlayer.Id);

            var loggedInPlayerStreak = streakService.FindLoggedInPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;
            var opponentPlayerStreak = streakService.FindOpponentPlayerStreak(newGame.Id, loggedInPlayer.Id).PlayerStreak;

            // Assert
            Assert.AreNotEqual(2, loggedInPlayerStreak);
            Assert.AreNotEqual(1, opponentPlayerStreak);
        }

        [TestMethod()]
        public void AddDefaultPlayerStreaksTest()
        {
            //Arrange
            var player = new Player { Id = "player", UserName = "player" };
            var opponent = new Player { Id = "opponent", UserName = "opponent" };

            var game = new Game
            {
                Id = 6,
                Player1Id = player.Id,
                Player2Id = opponent.Id,
                StartDate = Convert.ToDateTime(DateTime.UtcNow.ToString("MM/dd/yyyy")),
                IsActive = true,
                PlayerTurn = player.Id,
                ChallengeInProgress = false
            };

            //Act
            this.streakService.AddDefaultPlayerStreaks(game);
            var playerOneStreak = this.streakService.FindLoggedInPlayerStreak(game.Id, player.Id).PlayerStreak;
            var playerTwoStreak = this.streakService.FindOpponentPlayerStreak(game.Id, player.Id).PlayerStreak;

            //Assert
            mockContext.Verify(x => x.SaveChanges(), Times.Once());
            Assert.AreEqual(0, playerOneStreak);
            Assert.AreEqual(0, playerTwoStreak);
        }
    }
}