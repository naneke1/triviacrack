﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.MockSets
{
    internal class AnswerMockSet : GenericMockSet<Answer>
    {
        private Mock<DbSet<Answer>> answersMockSet;

        public AnswerMockSet(List<Answer> answers)
        {
            this.answersMockSet = base.GetMockSet(answers);
            this.answersMockSet.Setup(m => m.Find(It.IsAny<object[]>()))
                   .Returns<object[]>(ids => answers.FirstOrDefault(d => d.Id == (int)ids[0]));
            this.answersMockSet.Setup(m => m.Add(It.IsAny<Answer>())).Callback<Answer>(answers.Add);
        }

        public override Mock<DbSet<Answer>> GetDbSet<Answer>()
        {
            return answersMockSet as Mock<DbSet<Answer>>;
        }
    }
}
