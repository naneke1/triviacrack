﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaCrack.Tests.MockSets
{
    internal abstract class GenericMockSet<T>
    {
        protected virtual Mock<DbSet<T>> GetMockSet<T>(List<T> data) where T : class
        {
            var mockSet = new Mock<DbSet<T>>();
            var queryableData = data.AsQueryable();

            mockSet.As<IQueryable<T>>().Setup(s => s.ElementType).Returns(queryableData.ElementType);
            mockSet.As<IQueryable<T>>().Setup(s => s.Expression).Returns(queryableData.Expression);
            mockSet.As<IQueryable<T>>().Setup(s => s.Provider).Returns(queryableData.Provider);
            mockSet.As<IQueryable<T>>().Setup(s => s.GetEnumerator()).Returns(() => queryableData.GetEnumerator());
            return mockSet;
        }

        public abstract Mock<DbSet<T>> GetDbSet<T>() where T : class;
    }
}
