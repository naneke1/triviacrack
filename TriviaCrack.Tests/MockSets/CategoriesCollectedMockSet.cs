﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.MockSets
{
    internal class CategoriesCollectedMockSet : GenericMockSet<CategoriesCollected>
    {
        private Mock<DbSet<CategoriesCollected>> categoriesCollectedMockSet;

        public CategoriesCollectedMockSet(List<CategoriesCollected> categoriesCollected)
        {
            this.categoriesCollectedMockSet = base.GetMockSet(categoriesCollected);
            this.categoriesCollectedMockSet.Setup(m => m.Add(It.IsAny<CategoriesCollected>())).Callback<CategoriesCollected>(categoriesCollected.Add);
        }

        public override Mock<DbSet<CategoriesCollected>> GetDbSet<CategoriesCollected>()
        {
            return categoriesCollectedMockSet as Mock<DbSet<CategoriesCollected>>;
        }
    }
}
