﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Moq;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.MockSets
{
    internal class GameMockSet : GenericMockSet<Game>
    {
        private Mock<DbSet<Game>> gamesMockSet;

        public GameMockSet(List<Game> games)
        {
            this.gamesMockSet = base.GetMockSet(games);
            this.gamesMockSet.Setup(m => m.Find(It.IsAny<object[]>()))
                   .Returns<object[]>(ids => games.FirstOrDefault(d => d.Id == (int)ids[0]));
            this.gamesMockSet.Setup(m => m.Add(It.IsAny<Game>())).Callback<Game>(games.Add);
        }

        public override Mock<DbSet<Game>> GetDbSet<Game>()
        {
            return this.gamesMockSet as Mock<DbSet<Game>>;
        }
    }
}
