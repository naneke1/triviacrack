﻿using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.MockSets
{
    internal class PlayerMockSet : GenericMockSet<Player>
    {
        private Mock<DbSet<Player>> playersMockSet;

        public PlayerMockSet(List<Player> players)
        {
            this.playersMockSet = base.GetMockSet(players);
            this.playersMockSet.Setup(m => m.Find(It.IsAny<object[]>()))
                                .Returns<object[]>(ids => players.FirstOrDefault(d => d.Id == (string)ids[0]));
            this.playersMockSet.Setup(m => m.Add(It.IsAny<Player>())).Callback<Player>(players.Add);
        }

        public override Mock<DbSet<Player>> GetDbSet<Player>()
        {
            return this.playersMockSet as Mock<DbSet<Player>>;
        }
    }
}
