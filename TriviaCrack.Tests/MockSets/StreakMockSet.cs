﻿using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.MockSets
{
    internal class StreakMockSet : GenericMockSet<Streak>
    {
        private Mock<DbSet<Streak>> streaksMockSet;

        public StreakMockSet(List<Streak> streaks)
        {
            this.streaksMockSet = base.GetMockSet(streaks);
            this.streaksMockSet.Setup(m => m.Add(It.IsAny<Streak>())).Callback<Streak>(streaks.Add);
        }

        public override Mock<DbSet<Streak>> GetDbSet<Streak>()
        {
            return this.streaksMockSet as Mock<DbSet<Streak>>;
        }
    }
}
