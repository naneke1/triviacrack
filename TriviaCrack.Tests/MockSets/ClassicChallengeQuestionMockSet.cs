﻿using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.MockSets
{
    internal class ClassicChallengeQuestionMockSet : GenericMockSet<ClassicChallengeQuestion>
    {
        private Mock<DbSet<ClassicChallengeQuestion>> classicChallengeQuestionsMockSet;

        public ClassicChallengeQuestionMockSet(List<ClassicChallengeQuestion> classicChallengeQuestions)
        {
            this.classicChallengeQuestionsMockSet = base.GetMockSet(classicChallengeQuestions);
            this.classicChallengeQuestionsMockSet.Setup(m => m.Add(It.IsAny<ClassicChallengeQuestion>())).Callback<ClassicChallengeQuestion>(classicChallengeQuestions.Add);
        }

        public override Mock<DbSet<ClassicChallengeQuestion>> GetDbSet<ClassicChallengeQuestion>()
        {
            return this.classicChallengeQuestionsMockSet as Mock<DbSet<ClassicChallengeQuestion>>;
        }
    }
}
