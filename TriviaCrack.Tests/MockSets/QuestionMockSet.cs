﻿using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.MockSets
{
    internal class QuestionMockSet : GenericMockSet<Question>
    {
        private Mock<DbSet<Question>> questionsMockSet;

        public QuestionMockSet(List<Question> questions)
        {
            this.questionsMockSet = base.GetMockSet(questions);
            this.questionsMockSet.Setup(m => m.Find(It.IsAny<object[]>()))
                                .Returns<object[]>(ids => questions.FirstOrDefault(d => d.Id == (int)ids[0]));
            this.questionsMockSet.Setup(m => m.Add(It.IsAny<Question>())).Callback<Question>(questions.Add);
        }

        public override Mock<DbSet<Question>> GetDbSet<Question>()
        {
            return this.questionsMockSet as Mock<DbSet<Question>>;
        }
    }
}
