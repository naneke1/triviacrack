﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.MockSets
{
    class PowerUpMockSet : GenericMockSet<PowerUp>
    {
        private Mock<DbSet<PowerUp>> powerUpsMockSet;

        public PowerUpMockSet(List<PowerUp> powerUps)
        {
            this.powerUpsMockSet = base.GetMockSet(powerUps);
            this.powerUpsMockSet.Setup(m => m.Find(It.IsAny<object[]>()))
                                            .Returns<object[]>(ids => powerUps.FirstOrDefault(d => d.Name == (string)ids[0]));
        }

        public override Mock<DbSet<PowerUp>> GetDbSet<PowerUp>()
        {
            return this.powerUpsMockSet as Mock<DbSet<PowerUp>>;
        }
    }
}
