﻿using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.MockSets
{
    internal class StatisticMockSet : GenericMockSet<Statistics>
    {
        private Mock<DbSet<Statistics>> statsMockSet;

        public StatisticMockSet(List<Statistics> stats)
        {
            this.statsMockSet = base.GetMockSet(stats);
            this.statsMockSet.Setup(m => m.Add(It.IsAny<Statistics>())).Callback<Statistics>(stats.Add);
        }

        public override Mock<DbSet<Statistics>> GetDbSet<Statistics>()
        {
            return statsMockSet as Mock<DbSet<Statistics>>;
        }
    }
}
