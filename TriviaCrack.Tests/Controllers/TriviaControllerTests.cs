﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using TriviaCrack.Data_Access;
using TriviaCrack.ViewModels;
using TriviaCrack.Models;
using TriviaCrack.Tests.HttpContextMock;
using System.Web;
using System.Web.Routing;
using Moq;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;
using System.Web.Mvc;

namespace TriviaCrack.Controllers.Tests
{
    [TestClass()]
    public class TriviaControllerTests
    {
        private Mock<TriviaContext> mockContext;
        private List<Player> players;
        private List<Game> games;
        private List<Statistics> stats;
        private List<CategoriesCollected> categoriesCollected;
        private List<Streak> streaks;
        private List<Question> questions;
        private List<PowerUp> powerUps;
        private TriviaController controller;
        private Mock<HttpContextBase> httpContextBaseMock;
        private TestData testData; 

        [TestInitialize]
        public void Initialize()
        {
            this.testData = new TestData();
            this.players = testData.Players;
            this.games = testData.Games;
            this.stats = testData.Statistics;
            this.categoriesCollected = testData.CategoriesCollected;
            this.streaks = testData.Streaks;
            this.questions = testData.Questions;
            this.powerUps = testData.PowerUps;

            var playersMockSet = new PlayerMockSet(this.players).GetDbSet<Player>();
            var gamesMockSet = new GameMockSet(this.games).GetDbSet<Game>();
            var statsMockSet = new StatisticMockSet(this.stats).GetDbSet<Statistics>();
            var categoriesCollectedMockSet = new CategoriesCollectedMockSet(this.categoriesCollected).GetDbSet<CategoriesCollected>();
            var streakMockSet = new StreakMockSet(this.streaks).GetDbSet<Streak>();
            var questionMockSet = new QuestionMockSet(this.questions).GetDbSet<Question>();
            var powerUpMockSet = new PowerUpMockSet(this.powerUps).GetDbSet<PowerUp>();
            
            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Players).Returns(playersMockSet.Object);
            mockContext.Setup(c => c.Games).Returns(gamesMockSet.Object);
            mockContext.Setup(c => c.Statistics).Returns(statsMockSet.Object);
            mockContext.Setup(c => c.CategoriesCollected).Returns(categoriesCollectedMockSet.Object);
            mockContext.Setup(c => c.Streaks).Returns(streakMockSet.Object);
            mockContext.Setup(c => c.Questions).Returns(questionMockSet.Object);
            mockContext.Setup(c => c.PowerUps).Returns(powerUpMockSet.Object);
            
            controller = new TriviaController(mockContext.Object, testData.LoggedInPlayer.Id);

            HttpContextMock contextMock = new HttpContextMock();
            httpContextBaseMock = contextMock.GetMockHttpContext();
            httpContextBaseMock.Setup(c => c.Session["LoggedInUserId"]).Returns("a1");
            HttpContext.Current = contextMock.httpContext;

            controller.TempData = new TempDataDictionary();
            controller.ControllerContext = new ControllerContext(httpContextBaseMock.Object, new RouteData(), controller);
            controller.Url = new UrlHelper(new RequestContext(httpContextBaseMock.Object, new RouteData()));
        }

        [TestMethod]
        public void TriviaController_Question_Should_Redirect_Home_If_User_Refreshes()
        {
            //arrange
            httpContextBaseMock.Setup(c => c.Session["gameId"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["category"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["isCrownQuestion"]).Returns("False");
            //not initializing TempData["activeRequest"] which means user refreshed

            //act
            var result = controller.Question() as RedirectToRouteResult;

            //assert
            Assert.AreEqual("Home", result.RouteValues.Values.ElementAt(1));
            Assert.AreEqual("Index", result.RouteValues.Values.ElementAt(0));
        }

        [TestMethod]
        public void TriviaController_Question_Should_Switch_Turns_On_Refresh()
        {
            //arrange
            httpContextBaseMock.Setup(c => c.Session["gameId"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["category"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["isCrownQuestion"]).Returns("False");
            //not initializing TempData["activeRequest"] which means user refreshed
            string initialPlayerTurn = this.games[0].PlayerTurn;

            //act
            var result = controller.Question() as RedirectToRouteResult;

            //assert
            Assert.AreNotEqual(initialPlayerTurn, this.games[0].PlayerTurn);
            Assert.AreEqual(this.testData.OpponentPlayer.Id, this.games[0].PlayerTurn);
        }

        [TestMethod]
        public void TriviaController_Question_Should_Redirect_Home_If_Opponents_Turn()
        {
            //arrange
            httpContextBaseMock.Setup(c => c.Session["gameId"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["category"]).Returns("Sports");
            httpContextBaseMock.Setup(c => c.Session["isCrownQuestion"]).Returns("False");
            controller.TempData.Add("activeRequest", "true");

            //act
            this.games[0].PlayerTurn = this.testData.OpponentPlayer.Id;
            var result = controller.Question() as RedirectToRouteResult;

            //assert
            Assert.AreEqual("Home", result.RouteValues.Values.ElementAt(1));
            Assert.AreEqual("Index", result.RouteValues.Values.ElementAt(0));
        }

        [TestMethod]
        public void TriviaController_Question_View_Model_Should_Be_GameInformation_With_Valid_Input()
        {
            //arrange
            httpContextBaseMock.Setup(c => c.Session["gameId"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(5);
            httpContextBaseMock.Setup(c => c.Session["category"]).Returns("Sports");
            httpContextBaseMock.Setup(c => c.Session["isCrownQuestion"]).Returns("False");
            controller.TempData.Add("activeRequest", "true");

            //act
            var result = controller.Question() as ViewResult;

            //assert
            Assert.AreEqual("TriviaCrack.ViewModels.GameInformation", result.Model.ToString());
        }

        [TestMethod]
        public void TriviaController_Create_New_Game_Should_Show_NewGame_View()
        {
            //act
            var result = controller.CreateNewGame() as ViewResult;

            //assert
            Assert.AreEqual("NewGame", result.ViewName);
        }

        [TestMethod]
        public void TriviaController_Start_New_Game_Should_Show_Spinner_View()
        {
            //arrange
            controller.TempData.Add("FriendName", "player-2");

            //act
            var result = controller.StartNewGame() as ViewResult;

            //assert
            Assert.AreEqual("Spinner", result.ViewName);
        }

        [TestMethod]
        public void TriviaController_Start_New_Game_Should_Show_Increment_Games_Total()
        {
            //arrange
            controller.TempData.Add("FriendName", "player-2");
            int initialGamesCount = this.games.Count;

            //act
            controller.StartNewGame();

            //assert
            Assert.AreNotEqual(initialGamesCount, games.Count);
            Assert.AreEqual(initialGamesCount + 1, games.Count);
        }

        [TestMethod]
        public void TriviaController_Resume_Game_Should_Show_Spinner_View()
        {
            //arrange
            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(5);

            //act
            var result = controller.ResumeGame() as ViewResult;

            //assert
            Assert.AreEqual("Spinner", result.ViewName);
        }

        [TestMethod]
        public void TriviaController_CollectCategoryTest1()
        {
            //assemble
            var gameId = 5;
            var category = "art";
            var questionId = 4;
            var questionType = "Regular";

            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(gameId);
            httpContextBaseMock.Setup(c => c.Session["currentQuestionId"]).Returns(questionId);

            //act
            var result = controller.CollectCategory(gameId, category, questionType);

            //assert
            Assert.IsTrue(this.stats[3].IsAnsweredCorrectly);
        }

        [TestMethod]
        public void TriviaController_CollectCategoryTest2()
        {
            //assemble
            var gameId = 5;
            var category = "Sports";
            var questionId = 6;
            var questionType = "Regular";

            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(gameId);
            httpContextBaseMock.Setup(c => c.Session["currentQuestionId"]).Returns(questionId);

            //act
            var result = controller.CollectCategory(gameId, category, questionType);

            //assert
            Assert.IsTrue(this.stats[5].IsAnsweredCorrectly);
        }

        [TestMethod]
        public void TriviaController_CollectCategoryTest3()
        {
            //assemble
            var gameId = 5;
            var category = "Science";
            var questionId = 5;
            var questionType = "Regular";

            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(gameId);
            httpContextBaseMock.Setup(c => c.Session["currentQuestionId"]).Returns(questionId);

            //act
            var result = controller.CollectCategory(gameId, category, questionType);

            //assert
            Assert.IsTrue(this.stats[2].IsAnsweredCorrectly);
        }

        [TestMethod]
        public void TriviaController_UpdateCountTest1()
        {
            //assemble
            var questionId = "5";
            var countName = "Fun";

            //act
            var result = controller.UpdateCount(questionId, countName);
            var funCount = this.questions[4].FunRatingCount;

            //assert
            Assert.AreEqual(1, funCount + 1);
        }

        [TestMethod]
        public void TriviaController_UpdateCountTest2()
        {
            //assemble
            var questionId = "5";
            var countName = "Boring";

            //act
            var result = controller.UpdateCount(questionId, countName);
            var boringCount = this.questions[4].BoringRatingCount;

            //assert
            Assert.AreEqual(2, boringCount + 1);
        }

        [TestMethod]
        public void TriviaController_UpdateCountTest3()
        {
            //assemble
            var questionId = "2";
            var countName = "Fun";

            //act
            var result = controller.UpdateCount(questionId, countName);
            var funCount = this.questions[1].FunRatingCount;

            //assert
            Assert.AreNotEqual(3, funCount + 1);
        }

        [TestMethod]
        public void TriviaController_SelectCategoryTest1()
        {
            //assemble
            var gameId = 5;
            var type = "Crown";
            var playerCategories = new PlayerCategories();

            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(gameId);
            httpContextBaseMock.Setup(c => c.Session["PlayerCategories"]).Returns(playerCategories);
            httpContextBaseMock.Setup(c => c.Session["QuestionType"]).Returns(type);

            //act
            var result = controller.SelectCategory(gameId, type);

            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TriviaController_SelectCategoryTest2()
        {
            //assemble
            var gameId = 5;
            var type = "Challenge";
            var playerCategories = new PlayerCategories();

            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(gameId);
            httpContextBaseMock.Setup(c => c.Session["PlayerCategories"]).Returns(playerCategories);
            httpContextBaseMock.Setup(c => c.Session["QuestionType"]).Returns(type);

            //act
            var result = controller.SelectCategory(gameId, type);

            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TriviaController_SelectCategoryTest3()
        {
            //assemble
            var gameId = 5;
            var type = "Challenge";
            var playerCategories = new PlayerCategories();

            httpContextBaseMock.Setup(c => c.Session["gameToResume"]).Returns(gameId);
            httpContextBaseMock.Setup(c => c.Session["PlayerCategories"]).Returns(playerCategories);
            httpContextBaseMock.Setup(c => c.Session["QuestionType"]).Returns(type);

            var player1Streak = this.streaks[0].PlayerStreak;
            var resetValue = 2;
            //act
            var result = controller.SelectCategory(gameId, type);

            //assert
            Assert.AreEqual(0, player1Streak - resetValue);
        }
    }
}