﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TriviaCrack.Controllers;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.Controllers
{
    [TestClass()]
    public class CategoryControllerTest
    {
        [TestMethod()]
        public void IndexTest()
        {
            //Arrange

            var mockSet = new Mock<DbSet<Category>>();
            var listOfCategories = new List<Category>
            {
                new Category {Id = 1, Description = "Art"},
                new Category {Id = 2, Description = "Entertainment"},
                new Category {Id = 3, Description = "Sports"},
                new Category {Id = 4, Description = "Geography"},
                new Category {Id = 5, Description = "History"},
                new Category {Id = 6, Description = "Science"}
            };
            mockSet.As<IQueryable<Category>>()
                   .Setup(s => s.ElementType)
                   .Returns(listOfCategories.AsQueryable().ElementType);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Expression)
                .Returns(listOfCategories.AsQueryable().Expression);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Provider)
                .Returns(listOfCategories.AsQueryable().Provider);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.GetEnumerator())
                .Returns(listOfCategories.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Categories).Returns(mockSet.Object);

            var controller = new CategoryController(mockContext.Object);

            //Act
            var result = controller.Index() as ViewResult;
            var model = result.Model as List<Category>;

            //Assert

            Assert.AreEqual(6, model.Count);
            Assert.AreEqual("Art", model[0].Description);
            Assert.AreEqual("Entertainment", model[1].Description);
            Assert.AreEqual("Sports", model[2].Description);
            Assert.AreEqual("Geography", model[3].Description);
            Assert.AreEqual("History", model[4].Description);
            Assert.AreEqual("Science", model[5].Description);
        }


        [TestMethod()]
        public void Create_Should_Redirect_To_Index_When_Model_Valid()
        {
            //Arrange
            var aCategory = new Category() {Id = 1, Description = "Sports"};
            var mockSet = new Mock<DbSet<Category>>();
            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Categories).Returns(mockSet.Object);
            var controller = new CategoryController(mockContext.Object);

            //Act
            var result = controller.Create(aCategory) as RedirectToRouteResult;

            //Assert
            mockSet.Verify(m => m.Add(It.IsAny<Category>()), Times.Once);
            mockContext.Verify(c => c.SaveChanges());
            Assert.AreEqual("Index" as object, result.RouteValues["action"]);
        }

        [TestMethod]
        public void Create_Category_Should_Increment_List_Count()
        {
            //Arrange
            var listOfCategories = new List<Category>
            {
                new Category {Id = 1, Description = "Art"},
                new Category {Id = 2, Description = "Entertainment"},
                new Category {Id = 3, Description = "Sports"},
                new Category {Id = 4, Description = "Geography"},
                new Category {Id = 5, Description = "History"},
                new Category {Id = 6, Description = "Science"}
            };

            var mockSet = new Mock<DbSet<Category>>();
            var mockContext = new Mock<TriviaContext>();

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.ElementType)
                .Returns(listOfCategories.AsQueryable().ElementType);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Expression)
                .Returns(listOfCategories.AsQueryable().Expression);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Provider)
                .Returns(listOfCategories.AsQueryable().Provider);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.GetEnumerator())
                .Returns(listOfCategories.GetEnumerator());

            mockSet.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => listOfCategories.FirstOrDefault(d => d.Id == (int)ids[0]));

            mockContext.Setup(c => c.Categories).Returns(mockSet.Object);
            mockSet.Setup(m => m.Add(It.IsAny<Category>())).Callback<Category>(listOfCategories.Add);

            var controller = new CategoryController(mockContext.Object);

            //Act
            var initialListSize = listOfCategories.Count;
            Category category = new Category { Id = 7, Description = "Math" };
            controller.Create(category);

            //Assert
            mockContext.Verify(x => x.SaveChanges(), Times.Once());
            Assert.IsFalse(listOfCategories.Count == initialListSize);
        }

        [TestMethod()]
        public void Edit_Should_Redirect_To_Index_When_Model_Valid()
        {
            //Arrange
            var aCategory = new Category() {Id = 1, Description = "Sports"};
            var mockSet = new Mock<DbSet<Category>>();
            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Categories).Returns(mockSet.Object);
            var controller = new CategoryController(mockContext.Object);

            //Act
            var result = controller.Edit(aCategory) as RedirectToRouteResult;

            //Assert
            mockContext.Verify(c => c.SaveChanges());
            Assert.AreEqual("Index" as object, result.RouteValues["action"]);
        }

        [TestMethod]
        public void Edit_Category_Should_Return_Model_In_View()
        {
            //Arrange
            var listOfCategories = new List<Category>
            {
                new Category {Id = 1, Description = "Art"},
                new Category {Id = 2, Description = "Entertainment"},
                new Category {Id = 3, Description = "Sports"},
                new Category {Id = 4, Description = "Geography"},
                new Category {Id = 5, Description = "History"},
                new Category {Id = 6, Description = "Science"}
            };

            var mockSet = new Mock<DbSet<Category>>();
            var mockContext = new Mock<TriviaContext>();

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.ElementType)
                .Returns(listOfCategories.AsQueryable().ElementType);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Expression)
                .Returns(listOfCategories.AsQueryable().Expression);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Provider)
                .Returns(listOfCategories.AsQueryable().Provider);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.GetEnumerator())
                .Returns(listOfCategories.GetEnumerator());

            mockSet.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => listOfCategories.FirstOrDefault(d => d.Id == (int)ids[0]));

            mockContext.Setup(c => c.Categories).Returns(mockSet.Object);

            var controller = new CategoryController(mockContext.Object);

            //Act
            var results = controller.Edit(1) as ViewResult;
            var categoryRendered = (Category)results.ViewData.Model;

            //Assert
            Assert.AreEqual(1, categoryRendered.Id);
            Assert.AreEqual("Art", categoryRendered.Description);
        }

        [TestMethod]
        public void Details_Should_Return_Model_In_View()
        {
            //Arrange
            var listOfCategories = new List<Category>
            {
                new Category {Id = 1, Description = "Art"},
                new Category {Id = 2, Description = "Entertainment"},
                new Category {Id = 3, Description = "Sports"},
                new Category {Id = 4, Description = "Geography"},
                new Category {Id = 5, Description = "History"},
                new Category {Id = 6, Description = "Science"}
            };

            var mockSet = new Mock<DbSet<Category>>();
            var mockContext = new Mock<TriviaContext>();

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.ElementType)
                .Returns(listOfCategories.AsQueryable().ElementType);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Expression)
                .Returns(listOfCategories.AsQueryable().Expression);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Provider)
                .Returns(listOfCategories.AsQueryable().Provider);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.GetEnumerator())
                .Returns(listOfCategories.GetEnumerator());

            mockSet.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => listOfCategories.FirstOrDefault(d => d.Id == (int)ids[0]));

            mockContext.Setup(c => c.Categories).Returns(mockSet.Object);

            var controller = new CategoryController(mockContext.Object);

            //Act
            var results = controller.Details(2) as ViewResult;
            var categoryRendered = (Category)results.ViewData.Model;

            //Assert
            Assert.AreEqual(2, categoryRendered.Id);
            Assert.AreEqual("Entertainment", categoryRendered.Description);
        }

        [TestMethod()]
        public void Delete_Should_Redirect_To_Index_When_Model_Valid()
        {
            //Arrange
            var aCategory = new Category() { Id = 1, Description = "Sports" };
            var mockSet = new Mock<DbSet<Category>>();
            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Categories).Returns(mockSet.Object);
            var controller = new CategoryController(mockContext.Object);

            //Act
            var result = controller.DeleteConfirmed(aCategory.Id) as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("Index" as object, result.RouteValues["action"]);
        }

        [TestMethod]
        public void Delete_Category_Should_Remove_Category_From_List()
        {
            //Arrange

            var listOfCategories = new List<Category>
            {
                new Category {Id = 1, Description = "Art"},
                new Category {Id = 2, Description = "Entertainment"},
                new Category {Id = 3, Description = "Sports"},
                new Category {Id = 4, Description = "Geography"},
                new Category {Id = 5, Description = "History"},
                new Category {Id = 6, Description = "Science"}
            };

            var mockSet = new Mock<DbSet<Category>>();
            var mockContext = new Mock<TriviaContext>();

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.ElementType)
                .Returns(listOfCategories.AsQueryable().ElementType);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Expression)
                .Returns(listOfCategories.AsQueryable().Expression);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.Provider)
                .Returns(listOfCategories.AsQueryable().Provider);

            mockSet.As<IQueryable<Category>>()
                .Setup(s => s.GetEnumerator())
                .Returns(listOfCategories.GetEnumerator());

            mockSet.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => listOfCategories.FirstOrDefault(d => d.Id == (int)ids[0]));
            
            mockContext.Setup(c => c.Categories).Returns(mockSet.Object);

            mockSet.Setup(m => m.Remove(It.IsAny<Category>()))
                .Callback<Category>((entity) => listOfCategories.Remove(entity));

            var controller = new CategoryController(mockContext.Object);

            //Act
            controller.DeleteConfirmed(5);

            //Assert
            mockContext.Verify(x => x.Categories, Times.Exactly(2));
            mockContext.Verify(x => x.SaveChanges(), Times.Once());

            Assert.IsFalse(listOfCategories.Any(x => x.Id == 5));
        }
    }
}
