﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriviaCrack.Models;
using System.Collections.Generic;
using System.Linq;
using Moq;
using System.Data.Entity;
using TriviaCrack.Data_Access;
using TriviaCrack.Controllers;
using System.Web.Mvc;

namespace TriviaCrack.Tests.Controllers
{
    [TestClass]
    public class SubmittedQuestionsControllerTest
    {
        [TestMethod]
        public void SubmittedQuestions_IndexTest()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            }.AsQueryable();

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();

            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            var controller = new SubmittedQuestionsController(mockContext.Object);

            //Act

            var result = controller.Index() as ViewResult;
            var model = result.Model as List<SubmittedQuestion>;

            //Assert

            Assert.AreEqual(3, model.Count);
        }

        [TestMethod]
        public void SubmittedQuestions_IndexTest2()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            }.AsQueryable();

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();

            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            var controller = new SubmittedQuestionsController(mockContext.Object);

            //Act

            var result = controller.Index() as ViewResult;
            var model = result.Model as List<SubmittedQuestion>;

            //Assert

            Assert.AreNotEqual(10, model.Count);
        }

        [TestMethod]
        public void SubmittedQuestions_IndexTest3()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>().AsQueryable();

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();

            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            var controller = new SubmittedQuestionsController(mockContext.Object);

            //Act

            var result = controller.Index() as ViewResult;
            var model = result.Model as List<SubmittedQuestion>;

            //Assert

            Assert.AreEqual(0, model.Count);
        }

        [TestMethod]
        public void SubmittedQuestions_DetailsTest()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            };

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            mockContext.Setup(s => s.SubmittedQuestions.Find(It.IsAny<int>())).Returns<object[]>(d => listOfSubmittedQuestions.Find(m => m.Id == (int)d[0]));
            var controller = new SubmittedQuestionsController(mockContext.Object);
            var id = 2;

            //Act

            var result = controller.Details(id);
            var viewResult = result as ViewResult;
            var question = (SubmittedQuestion) viewResult.ViewData.Model;

            //Assert

            Assert.AreEqual("Which of the following is a continent?", question.Text);
        }

        [TestMethod]
        public void SubmittedQuestions_DetailsTest2()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"},
               new SubmittedQuestion {Id=4, Text="Who is the current (2018) president of the USA?", CorrectAnswer="Trump", IncorrectAnswer1="Obama", IncorrectAnswer2="Woodrow", IncorrectAnswer3="Roosevelt", CategoryName="History"},
               new SubmittedQuestion {Id=5, Text="Which of the following is a winter sport?", CorrectAnswer="skiing", IncorrectAnswer1="volleyball", IncorrectAnswer2="baseball", IncorrectAnswer3="cricket", CategoryName="Sports"}
            };

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            mockContext.Setup(s => s.SubmittedQuestions.Find(It.IsAny<int>())).Returns<object[]>(d => listOfSubmittedQuestions.Find(m => m.Id == (int)d[0]));
            var controller = new SubmittedQuestionsController(mockContext.Object);
            var id = 4;

            //Act

            var result = controller.Details(id);
            var viewResult = result as ViewResult;
            var question = (SubmittedQuestion)viewResult.ViewData.Model;

            //Assert

            Assert.AreEqual("Who is the current (2018) president of the USA?", question.Text);
        }

        [TestMethod]
        public void SubmittedQuestions_DetailsTest3()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>();

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            mockContext.Setup(s => s.SubmittedQuestions.Find(It.IsAny<int>())).Returns<object[]>(d => listOfSubmittedQuestions.Find(m => m.Id == (int)d[0]));
            var controller = new SubmittedQuestionsController(mockContext.Object);
            var id = 0;

            //Act

            var result = controller.Details(id);
            var viewResult = result as ViewResult;
            var expectedResult = new System.Web.Mvc.HttpNotFoundResult();

            //Assert

            Assert.IsInstanceOfType(result, expectedResult.GetType());
        }

        [TestMethod]
        public void SubmittedQuestions_CreateTest()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            };

            var queryableSet = listOfSubmittedQuestions.AsQueryable();
            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(queryableSet.ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(queryableSet.Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(queryableSet.Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => queryableSet.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            mockSet.Setup(d => d.Add(It.IsAny<SubmittedQuestion>())).Callback<SubmittedQuestion>((s) => listOfSubmittedQuestions.Add(s));
           
            var controller = new SubmittedQuestionsController(mockContext.Object);
            var questionToAdd = new SubmittedQuestion {Text="Who is known for discovering gravity?", CorrectAnswer="Isaac Newton", IncorrectAnswer1="Socrates", IncorrectAnswer2="Galileo", IncorrectAnswer3="Copernicus", CategoryName="Science"};
            
            //Act

            var result = controller.Create(questionToAdd);
            var viewResult = result as ViewResult;
            
            //Assert

            Assert.AreEqual(4, mockSet.Object.Count());
        }

        [TestMethod]
        public void SubmittedQuestions_CreateTest2()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"},
               new SubmittedQuestion {Id=4, Text="Who is the current (2018) president of the USA?", CorrectAnswer="Trump", IncorrectAnswer1="Obama", IncorrectAnswer2="Woodrow", IncorrectAnswer3="Roosevelt", CategoryName="History"}
            };

            var queryableSet = listOfSubmittedQuestions.AsQueryable();
            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(queryableSet.ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(queryableSet.Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(queryableSet.Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => queryableSet.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            mockSet.Setup(d => d.Add(It.IsAny<SubmittedQuestion>())).Callback<SubmittedQuestion>((s) => listOfSubmittedQuestions.Add(s));

            var controller = new SubmittedQuestionsController(mockContext.Object);
            var questionToAdd = new SubmittedQuestion { Text = "Who is known for discovering gravity?", CorrectAnswer = "Isaac Newton", IncorrectAnswer1 = "Socrates", IncorrectAnswer2 = "Galileo", IncorrectAnswer3 = "Copernicus", CategoryName = "Science" };

            //Act

            var result = controller.Create(questionToAdd);
            var viewResult = result as ViewResult;

            //Assert

            Assert.AreEqual(5, mockSet.Object.Count());
        }

        [TestMethod]
        public void SubmittedQuestions_CreateTest3()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>();

            var queryableSet = listOfSubmittedQuestions.AsQueryable();
            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(queryableSet.ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(queryableSet.Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(queryableSet.Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => queryableSet.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            mockSet.Setup(d => d.Add(It.IsAny<SubmittedQuestion>())).Callback<SubmittedQuestion>((s) => listOfSubmittedQuestions.Add(s));

            var controller = new SubmittedQuestionsController(mockContext.Object);
            var questionToAdd = new SubmittedQuestion { Text = "Who is known for discovering gravity?", CorrectAnswer = "Isaac Newton", IncorrectAnswer1 = "Socrates", IncorrectAnswer2 = "Galileo", IncorrectAnswer3 = "Copernicus", CategoryName = "Science" };

            //Act

            var result = controller.Create(questionToAdd);
            var viewResult = result as ViewResult;

            //Assert

            Assert.AreEqual(1, mockSet.Object.Count());
        }

        [TestMethod]
        public void SubmittedQuestions_EditTest()
        {
            //Arrange
            
            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            }.AsQueryable();

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            
            var controller = new SubmittedQuestionsController(mockContext.Object);
            var questionToEdit = new SubmittedQuestion { Text = "Which of the following is a continent?", CorrectAnswer = "Africa", IncorrectAnswer1 = "Austria", IncorrectAnswer2 = "Paraguay", IncorrectAnswer3 = "Alabama", CategoryName = "Geography" };

            //Act

            var result = controller.Edit(questionToEdit);
            
            //Assert

            mockContext.Verify(x => x.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void SubmittedQuestions_EditTest2()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            }.AsQueryable();

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);

            var controller = new SubmittedQuestionsController(mockContext.Object);
            var questionToEdit = new SubmittedQuestion { Text = "Which of the following is a continent?", CorrectAnswer = "Africa", IncorrectAnswer1 = "Finland", IncorrectAnswer2 = "Paraguay", IncorrectAnswer3 = "Alabama", CategoryName = "Geography" };

            //Act

            var result = controller.Edit(questionToEdit);

            //Assert

            mockContext.Verify(x => x.SaveChanges(), Times.AtLeastOnce());
        }

        [TestMethod]
        public void SubmittedQuestions_EditTest3()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            }.AsQueryable();

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);

            var controller = new SubmittedQuestionsController(mockContext.Object);
            var questionToEdit = new SubmittedQuestion { Text = "Which of the following is not a continent?", CorrectAnswer = "Austria", IncorrectAnswer1 = "Africa", IncorrectAnswer2 = "North America", IncorrectAnswer3 = "Georgia", CategoryName = "Geography" };

            //Act

            var result = controller.Edit(questionToEdit);

            //Assert

            mockContext.Verify(x => x.SaveChanges(), Times.AtLeastOnce());
        }

        [TestMethod]
        public void SubmittedQuestions_DeleteTest()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            };

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.AsQueryable().ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.AsQueryable().Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.AsQueryable().Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());
            mockSet.Setup(r => r.Remove(It.IsAny<SubmittedQuestion>())).Callback<SubmittedQuestion>((entity) => listOfSubmittedQuestions.Remove(entity));

            var id = 3;
            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            mockContext.Setup(s => s.SubmittedQuestions.Find(It.IsAny<SubmittedQuestion>())).Returns<object[]>(d => listOfSubmittedQuestions.Find(m => m.Id == (int)d[0]));

            var controller = new SubmittedQuestionsController(mockContext.Object);
            
            //Act

            var result = controller.DeleteConfirmed(id);
            var viewResult = result as ViewResult;
            
            //Assert

            mockContext.Verify(s => s.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void SubmittedQuestions_DeleteTest2()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            };

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.AsQueryable().ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.AsQueryable().Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.AsQueryable().Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());
            mockSet.Setup(r => r.Remove(It.IsAny<SubmittedQuestion>())).Callback<SubmittedQuestion>((entity) => listOfSubmittedQuestions.Remove(entity));

            var id = 2;
            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            mockContext.Setup(s => s.SubmittedQuestions.Find(It.IsAny<SubmittedQuestion>())).Returns<object[]>(d => listOfSubmittedQuestions.Find(m => m.Id == (int)d[0]));

            var controller = new SubmittedQuestionsController(mockContext.Object);

            //Act

            var result = controller.DeleteConfirmed(id);
            var viewResult = result as ViewResult;

            //Assert

            mockContext.Verify(s => s.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void SubmittedQuestions_DeleteTest3()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            };

            var mockSet = new Mock<DbSet<SubmittedQuestion>>();
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.AsQueryable().ElementType);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.AsQueryable().Expression);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.AsQueryable().Provider);
            mockSet.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());
            mockSet.Setup(r => r.Remove(It.IsAny<SubmittedQuestion>())).Callback<SubmittedQuestion>((entity) => listOfSubmittedQuestions.Remove(entity));

            var id = 1;
            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet.Object);
            mockContext.Setup(s => s.SubmittedQuestions.Find(It.IsAny<SubmittedQuestion>())).Returns<object[]>(d => listOfSubmittedQuestions.Find(m => m.Id == (int)d[0]));

            var controller = new SubmittedQuestionsController(mockContext.Object);

            //Act

            var result = controller.DeleteConfirmed(id);
            var viewResult = result as ViewResult;

            //Assert

            mockContext.Verify(s => s.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void SubmittedQuestions_ApproveTest()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            }.AsQueryable();
           
            var listOfAnswers = new List<Answer>
            {
                new Answer {Id=1, Text="Carrot"},
                new Answer {Id=2, Text="Cabbage" },
                new Answer {Id=3, Text="Turnip" },
                new Answer {Id=4, Text="Tomato" }
            };

            var listOfCategories = new List<Category>
            {
                new Category {Id=1, Description="Art"},
                new Category {Id=2, Description="Entertainment" },
                new Category {Id=3, Description="Geography" },
                new Category {Id=4, Description="History"},
                new Category {Id=5, Description="Science" },
                new Category {Id=6, Description="Sports" }
            }.AsQueryable();

            var listOfQuestions = new List<Question>
            {
                new Question {Id=1, Text="Which is not a vegetable?", CorrectAnswerId=4, AnswerChoice1Id=4, AnswerChoice2Id=1, AnswerChoice3Id=2, AnswerChoice4Id=3, CategoryId=5},
                new Question {Id=2, Text="Which of these can be purple?" , CorrectAnswerId=2, AnswerChoice1Id=2, AnswerChoice2Id=1, AnswerChoice3Id=4, AnswerChoice4Id=3, CategoryId=5}
            };

            var queryableAnswers = listOfAnswers.AsQueryable();
            var queryableQuestions = listOfQuestions.AsQueryable();
            var mockSet2 = new Mock<DbSet<SubmittedQuestion>>();
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.Provider);
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.Expression);
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.ElementType);
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());
            var mockSet = new Mock<DbSet<Answer>>();
            mockSet.As<IQueryable<Answer>>().Setup(s => s.Provider).Returns(queryableAnswers.Provider);
            mockSet.As<IQueryable<Answer>>().Setup(s => s.Expression).Returns(queryableAnswers.Expression);
            mockSet.As<IQueryable<Answer>>().Setup(s => s.ElementType).Returns(queryableAnswers.ElementType);
            mockSet.As<IQueryable<Answer>>().Setup(s => s.GetEnumerator()).Returns(() => listOfAnswers.GetEnumerator());
            mockSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => listOfAnswers.FirstOrDefault(d => d.Id == (int)ids[0]));
            mockSet.Setup(d => d.Add(It.IsAny<Answer>())).Callback<Answer>((s) => listOfAnswers.Add(s));
            var mockSet3 = new Mock<DbSet<Category>>();
            mockSet3.As<IQueryable<Category>>().Setup(s => s.Provider).Returns(listOfCategories.Provider);
            mockSet3.As<IQueryable<Category>>().Setup(s => s.Expression).Returns(listOfCategories.Expression);
            mockSet3.As<IQueryable<Category>>().Setup(s => s.ElementType).Returns(listOfCategories.ElementType);
            mockSet3.As<IQueryable<Category>>().Setup(s => s.GetEnumerator()).Returns(() => listOfCategories.GetEnumerator());
            mockSet3.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => listOfCategories.FirstOrDefault(d => d.Id == (int)ids[0]));
            var mockSet4 = new Mock<DbSet<Question>>();
            mockSet4.As<IQueryable<Question>>().Setup(d => d.Provider).Returns(queryableQuestions.Provider);
            mockSet4.As<IQueryable<Question>>().Setup(d => d.Expression).Returns(queryableQuestions.Expression);
            mockSet4.As<IQueryable<Question>>().Setup(d => d.ElementType).Returns(queryableQuestions.ElementType);
            mockSet4.As<IQueryable<Question>>().Setup(d => d.GetEnumerator()).Returns(() => listOfQuestions.GetEnumerator());
            mockSet4.Setup(d => d.Add(It.IsAny<Question>())).Callback<Question>((s) => listOfQuestions.Add(s));

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Answers).Returns(mockSet.Object);
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet2.Object);
            mockContext.Setup(c => c.Categories).Returns(mockSet3.Object);
            mockContext.Setup(c => c.Questions).Returns(mockSet4.Object);

            var controller = new SubmittedQuestionsController(mockContext.Object);
            Mock<UrlHelper> UrlHelperMock = new Mock<UrlHelper>();

            controller.Url = UrlHelperMock.Object;


            var id = 2;

            //Act
            
            var result = controller.Approve(id) as JsonResult;
            
            //Assert

            Assert.AreEqual(8, listOfAnswers.Count());
        }

        [TestMethod]
        public void SubmittedQuestions_ApproveTest2()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            }.AsQueryable();

            var listOfAnswers = new List<Answer>
            {
                new Answer {Id=1, Text="Carrot"},
                new Answer {Id=2, Text="Cabbage" },
                new Answer {Id=3, Text="Turnip" },
                new Answer {Id=4, Text="Tomato" },
                new Answer {Id=5, Text="Collie"},
                new Answer {Id=6, Text="Siamese" },
                new Answer {Id=7, Text="Great Dane" },
                new Answer {Id=8, Text="Poodle" }
            };

            var listOfCategories = new List<Category>
            {
                new Category {Id=1, Description="Art"},
                new Category {Id=2, Description="Entertainment" },
                new Category {Id=3, Description="Geography" },
                new Category {Id=4, Description="History"},
                new Category {Id=5, Description="Science" },
                new Category {Id=6, Description="Sports" }
            }.AsQueryable();

            var listOfQuestions = new List<Question>
            {
                new Question {Id=1, Text="Which is not a vegetable?", CorrectAnswerId=4, AnswerChoice1Id=4, AnswerChoice2Id=1, AnswerChoice3Id=2, AnswerChoice4Id=3, CategoryId=5},
                new Question {Id=2, Text="Which of these can be purple?" , CorrectAnswerId=2, AnswerChoice1Id=2, AnswerChoice2Id=1, AnswerChoice3Id=4, AnswerChoice4Id=3, CategoryId=5},
                new Question {Id=3, Text="Which of these is a cat breed?" , CorrectAnswerId=6, AnswerChoice1Id=5, AnswerChoice2Id=6, AnswerChoice3Id=7, AnswerChoice4Id=8, CategoryId=2}
            };

            var queryableAnswers = listOfAnswers.AsQueryable();
            var queryableQuestions = listOfQuestions.AsQueryable();
            var mockSet2 = new Mock<DbSet<SubmittedQuestion>>();
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.Provider);
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.Expression);
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.ElementType);
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());
            var mockSet = new Mock<DbSet<Answer>>();
            mockSet.As<IQueryable<Answer>>().Setup(s => s.Provider).Returns(queryableAnswers.Provider);
            mockSet.As<IQueryable<Answer>>().Setup(s => s.Expression).Returns(queryableAnswers.Expression);
            mockSet.As<IQueryable<Answer>>().Setup(s => s.ElementType).Returns(queryableAnswers.ElementType);
            mockSet.As<IQueryable<Answer>>().Setup(s => s.GetEnumerator()).Returns(() => listOfAnswers.GetEnumerator());
            mockSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => listOfAnswers.FirstOrDefault(d => d.Id == (int)ids[0]));
            mockSet.Setup(d => d.Add(It.IsAny<Answer>())).Callback<Answer>((s) => listOfAnswers.Add(s));
            var mockSet3 = new Mock<DbSet<Category>>();
            mockSet3.As<IQueryable<Category>>().Setup(s => s.Provider).Returns(listOfCategories.Provider);
            mockSet3.As<IQueryable<Category>>().Setup(s => s.Expression).Returns(listOfCategories.Expression);
            mockSet3.As<IQueryable<Category>>().Setup(s => s.ElementType).Returns(listOfCategories.ElementType);
            mockSet3.As<IQueryable<Category>>().Setup(s => s.GetEnumerator()).Returns(() => listOfCategories.GetEnumerator());
            mockSet3.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => listOfCategories.FirstOrDefault(d => d.Id == (int)ids[0]));
            var mockSet4 = new Mock<DbSet<Question>>();
            mockSet4.As<IQueryable<Question>>().Setup(d => d.Provider).Returns(queryableQuestions.Provider);
            mockSet4.As<IQueryable<Question>>().Setup(d => d.Expression).Returns(queryableQuestions.Expression);
            mockSet4.As<IQueryable<Question>>().Setup(d => d.ElementType).Returns(queryableQuestions.ElementType);
            mockSet4.As<IQueryable<Question>>().Setup(d => d.GetEnumerator()).Returns(() => listOfQuestions.GetEnumerator());
            mockSet4.Setup(d => d.Add(It.IsAny<Question>())).Callback<Question>((s) => listOfQuestions.Add(s));

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Answers).Returns(mockSet.Object);
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet2.Object);
            mockContext.Setup(c => c.Categories).Returns(mockSet3.Object);
            mockContext.Setup(c => c.Questions).Returns(mockSet4.Object);

            var controller = new SubmittedQuestionsController(mockContext.Object);
            Mock<UrlHelper> UrlHelperMock = new Mock<UrlHelper>();

            controller.Url = UrlHelperMock.Object;


            var id = 2;

            //Act

            var result = controller.Approve(id) as JsonResult;

            //Assert

            Assert.AreEqual(12, listOfAnswers.Count());
        }

        [TestMethod]
        public void SubmittedQuestions_ApproveTest3()
        {
            //Arrange

            var listOfSubmittedQuestions = new List<SubmittedQuestion>
            {
               new SubmittedQuestion {Id=1, Text="Which of the following is not a dog breed?", CorrectAnswer="Burmese", IncorrectAnswer1="Cocker Spaniel", IncorrectAnswer2="Great Dane", IncorrectAnswer3="Yorkshire Terrier", CategoryName="Science"},
               new SubmittedQuestion {Id=2, Text="Which of the following is a continent?", CorrectAnswer="Africa", IncorrectAnswer1="Austria", IncorrectAnswer2="Paraguay", IncorrectAnswer3="Mississippi", CategoryName="Geography"},
               new SubmittedQuestion {Id=3, Text="Which of the following is not an art medium?", CorrectAnswer="keyboard", IncorrectAnswer1="Pastels", IncorrectAnswer2="Markers", IncorrectAnswer3="Pencil", CategoryName="Art"}
            }.AsQueryable();
           
            var listOfAnswers = new List<Answer>();

            var listOfCategories = new List<Category>().AsQueryable();

            var listOfQuestions = new List<Question>();

            var queryableAnswers = listOfAnswers.AsQueryable();
            var queryableQuestions = listOfQuestions.AsQueryable();
            var mockSet2 = new Mock<DbSet<SubmittedQuestion>>();
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Provider).Returns(listOfSubmittedQuestions.Provider);
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.Expression).Returns(listOfSubmittedQuestions.Expression);
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.ElementType).Returns(listOfSubmittedQuestions.ElementType);
            mockSet2.As<IQueryable<SubmittedQuestion>>().Setup(s => s.GetEnumerator()).Returns(() => listOfSubmittedQuestions.GetEnumerator());
            var mockSet = new Mock<DbSet<Answer>>();
            mockSet.As<IQueryable<Answer>>().Setup(s => s.Provider).Returns(queryableAnswers.Provider);
            mockSet.As<IQueryable<Answer>>().Setup(s => s.Expression).Returns(queryableAnswers.Expression);
            mockSet.As<IQueryable<Answer>>().Setup(s => s.ElementType).Returns(queryableAnswers.ElementType);
            mockSet.As<IQueryable<Answer>>().Setup(s => s.GetEnumerator()).Returns(() => listOfAnswers.GetEnumerator());
            mockSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => listOfAnswers.FirstOrDefault(d => d.Id == (int)ids[0]));
            mockSet.Setup(d => d.Add(It.IsAny<Answer>())).Callback<Answer>((s) => listOfAnswers.Add(s));
            var mockSet3 = new Mock<DbSet<Category>>();
            mockSet3.As<IQueryable<Category>>().Setup(s => s.Provider).Returns(listOfCategories.Provider);
            mockSet3.As<IQueryable<Category>>().Setup(s => s.Expression).Returns(listOfCategories.Expression);
            mockSet3.As<IQueryable<Category>>().Setup(s => s.ElementType).Returns(listOfCategories.ElementType);
            mockSet3.As<IQueryable<Category>>().Setup(s => s.GetEnumerator()).Returns(() => listOfCategories.GetEnumerator());
            mockSet3.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => listOfCategories.FirstOrDefault(d => d.Id == (int)ids[0]));
            var mockSet4 = new Mock<DbSet<Question>>();
            mockSet4.As<IQueryable<Question>>().Setup(d => d.Provider).Returns(queryableQuestions.Provider);
            mockSet4.As<IQueryable<Question>>().Setup(d => d.Expression).Returns(queryableQuestions.Expression);
            mockSet4.As<IQueryable<Question>>().Setup(d => d.ElementType).Returns(queryableQuestions.ElementType);
            mockSet4.As<IQueryable<Question>>().Setup(d => d.GetEnumerator()).Returns(() => listOfQuestions.GetEnumerator());
            mockSet4.Setup(d => d.Add(It.IsAny<Question>())).Callback<Question>((s) => listOfQuestions.Add(s));

            var mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Answers).Returns(mockSet.Object);
            mockContext.Setup(c => c.SubmittedQuestions).Returns(mockSet2.Object);
            mockContext.Setup(c => c.Categories).Returns(mockSet3.Object);
            mockContext.Setup(c => c.Questions).Returns(mockSet4.Object);

            var controller = new SubmittedQuestionsController(mockContext.Object);
            Mock<UrlHelper> UrlHelperMock = new Mock<UrlHelper>();

            controller.Url = UrlHelperMock.Object;


            var id = 2;

            //Act
            
            var result = controller.Approve(id) as JsonResult;
            
            //Assert

            Assert.AreEqual(4, listOfAnswers.Count());
        }
    }
}
