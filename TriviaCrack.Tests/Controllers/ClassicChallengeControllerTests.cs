﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using TriviaCrack.Models;
using Moq;
using TriviaCrack.Data_Access;
using System.Web.Routing;
using System.Web;
using System.Web.Mvc;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;
using TriviaCrack.Tests.HttpContextMock;

namespace TriviaCrack.Controllers.Tests
{
    [TestClass()]
    public class ClassicChallengeControllerTests
    {
        private Mock<TriviaContext> mockContext;
        private ClassicChallengeController controller;
        private List<Player> players;
        private List<Question> questions;
        private List<Answer> answers;
        private List<Game> games;
        private List<Category> categories;
        private List<CategoriesCollected> categoriesCollected;
        private List<ClassicChallengeQuestion> classicChallengeQuestions;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();

            this.categories = testData.Categories;
            this.players = testData.Players;
            this.answers = testData.Answers;
            this.questions = testData.Questions;
            this.games = testData.Games;
            this.classicChallengeQuestions = testData.ClassicChallengeQuestions;
            this.categoriesCollected = testData.CategoriesCollected;

            var answersMockSet = new AnswerMockSet(this.answers).GetDbSet<Answer>();
            var questionsMockSet = new QuestionMockSet(this.questions).GetDbSet<Question>();
            var playerMockSet = new PlayerMockSet(this.players).GetDbSet<Player>();
            var gameMockSet = new GameMockSet(this.games).GetDbSet<Game>();
            var categoriesCollectedMockSet = new CategoriesCollectedMockSet(this.categoriesCollected)
                                                .GetDbSet<CategoriesCollected>();
            var classicChallengeQuestionMockSet = new ClassicChallengeQuestionMockSet(this.classicChallengeQuestions)
                                            .GetDbSet<ClassicChallengeQuestion>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.CategoriesCollected).Returns(categoriesCollectedMockSet.Object);
            mockContext.Setup(c => c.Questions).Returns(questionsMockSet.Object);
            mockContext.Setup(c => c.Answers).Returns(answersMockSet.Object);
            mockContext.Setup(c => c.Players).Returns(playerMockSet.Object);
            mockContext.Setup(c => c.Games).Returns(gameMockSet.Object);
            mockContext.Setup(c => c.ClassicChallengeQuestions).Returns(classicChallengeQuestionMockSet.Object);

            controller = new ClassicChallengeController(mockContext.Object, "a1");

            var httpContextMock = new HttpContextMock().GetMockHttpContext();
            httpContextMock.Setup(c => c.Session["gameId"]).Returns(5);
            controller.ControllerContext = new ControllerContext(httpContextMock.Object, new RouteData(), controller);
            controller.Url = new UrlHelper(new RequestContext(httpContextMock.Object, new RouteData()));
        }

        [TestMethod()]
        public void QuestionTest()
        {
            //Act
            controller.TempData.Add("gameId", "5");
            var result = controller.Question() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("TriviaCrack.Models.Question", result.Model.ToString());
            Assert.AreEqual(5, result.ViewData.Count);
            Assert.AreEqual("CurrentQuestion", result.ViewData.Keys.ElementAt(0));
            Assert.AreEqual("ClassicChallengeGameId", result.ViewData.Keys.ElementAt(1));
            Assert.AreEqual("TotalCorrect", result.ViewData.Keys.ElementAt(2));
            Assert.AreEqual("OpponentTotalCorrect", result.ViewData.Keys.ElementAt(3));
            Assert.AreEqual("OpponentCurrentQuestion", result.ViewData.Keys.ElementAt(4));
        }

        [TestMethod()]
        public void SwitchTurnsTest()
        {
            //Act
            var result = controller.SwitchTurns(5) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
            
            //games[0] was initialized with turn set to "a1"
            Assert.AreEqual("a2", games[0].PlayerTurn);
        }

        [TestMethod()]
        public void GetNextQuestionTest()
        {
            //Act
            var result = controller.GetNextQuestion(5) as RedirectToRouteResult;
            var resultValues = result.RouteValues.Values.ToList();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Question", resultValues[0]);
        }

        [TestMethod()]
        public void StartNewClassicChallengeTest()
        {
            //Arrange
            //games[0].ChallengeInProgress set to false in initializer

            //Act
            var result = controller.StartNewClassicChallenge("Art", 5, "Sports") as JsonResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, games[0].ChallengeInProgress);
        }

        [TestMethod()]
        public void CorrectAnswerTest()
        {
            //Arrange
            //ClassicChallengeQuestions[0] initialized with 0 total correct in initializer

            //Act
            var result = controller.CorrectAnswer(5) as EmptyResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, classicChallengeQuestions[0].TotalCorrect);
        }

        [TestMethod()]
        public void IncorrectAnswerTest()
        {
            //Arrange
            //ClassicChallengeQuestions[0] initialized with 0 total correct in initializer
            
            //Act
            var result = controller.IncorrectAnswer(5) as EmptyResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, classicChallengeQuestions[0].TotalCorrect);
        }
    }
}