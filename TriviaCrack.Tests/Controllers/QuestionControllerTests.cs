﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TriviaCrack.Controllers;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using TriviaCrack.Tests.MockSets;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.ViewModels;
using PagedList;

namespace TriviaCrack.Tests.Controllers
{
    [TestClass]
    public class QuestionControllerTests
    {
        private List<Question> questions;
        private List<Answer> answers;
        private Mock<TriviaContext> mockContext;
        private QuestionController questionController;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.questions = testData.Questions;
            this.answers = testData.Answers;

            var questionsMockSet = new QuestionMockSet(this.questions).GetDbSet<Question>();
            questionsMockSet.Setup(m => m.Remove(It.IsAny<Question>()))
                .Callback<Question>((entity) => this.questions.Remove(entity));

            var answersMockSet = new AnswerMockSet(this.answers).GetDbSet<Answer>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Questions).Returns(questionsMockSet.Object);
            mockContext.Setup(c => c.Answers).Returns(answersMockSet.Object);

            this.questionController = new QuestionController(mockContext.Object);
        }

        [TestMethod]
        public void IndexTest()
        {
            // Act
            var result = this.questionController.Index(1) as ViewResult;
            var model = result.Model as PagedList<Question>;

            // Assert
            Assert.AreEqual(8, model.Count);
        }

        [TestMethod]
        public void DetailsTest()
        {
            // Act
            var result = this.questionController.Details(1) as ViewResult;
            var model = (Question) result.ViewData.Model;

            // Assert
            Assert.AreEqual("How do you make green?", model.Text);
        }

        [TestMethod]
        public void CreateTest()
        {
            //Arrange
            var questionToAdd = new NewQuestion {
                Text = "What is the sun?",CategoryId = 1, CorrectChoice = "1990",
                Choice2 = "1995", Choice3 = "2000", Choice4 = "2005"
            };

            // Act
            this.questionController.Create(questionToAdd);

            // Assert
            Assert.AreEqual(11, this.questions.Count());
        }

        [TestMethod]
        public void DeleteTest()
        {
            // Act
            this.questionController.DeleteConfirmed(2);

            // Assert
            mockContext.Verify(s => s.SaveChanges(), Times.Once);
            Assert.IsFalse(this.questions.Any(x => x.Id == 2));
        }

        /*
        [TestMethod]
        public void EditTest()
        {
            // Arrange
            var questionToEdit = new NewQuestion
            {
                Text = "How do you make green?",
                CategoryId = 1,
                CorrectChoice = "Blue",
                Choice2 = "Yellow",
                Choice3 = "Green",
                Choice4 = "Purple"
            };

            // Act
            var result = this.questionController.Edit(questionToEdit) as ViewResult;
            var model = (NewQuestion) result.ViewData.Model;

            // Assert
            Assert.AreEqual("What color is the sky when sunny?", model.Text);
            //mockContext.Verify(s => s.SaveChanges());
            //Assert.AreEqual("Index" as object, result.RouteValues["action"]);
        }
        */
    }
}
