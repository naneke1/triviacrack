﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriviaCrack.Data_Access;
using Moq;
using TriviaCrack.Models;
using System.Collections.Generic;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;
using TriviaCrack.Tests.HttpContextMock;
using System.Web.Mvc;
using System.Web.Routing;

namespace TriviaCrack.Controllers.Tests
{
    [TestClass]
    public class ManageControllerTests
    {
        private Mock<TriviaContext> mockContext;
        private List<Player> players;
        private List<Game> games;
        private List<Statistics> stats;
        private ManageController controller;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.players = testData.Players;
            this.games = testData.Games;
            this.stats = testData.Statistics;

            var playersMockSet = new PlayerMockSet(this.players).GetDbSet<Player>();
            var gamesMockSet = new GameMockSet(this.games).GetDbSet<Game>();
            var statsMockSet = new StatisticMockSet(this.stats).GetDbSet<Statistics>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Players).Returns(playersMockSet.Object);
            mockContext.Setup(c => c.Games).Returns(gamesMockSet.Object);

            controller = new ManageController(mockContext.Object);

            var httpContextMock = new HttpContextMock().GetMockHttpContext();
            controller.ControllerContext = new ControllerContext(httpContextMock.Object, new RouteData(), controller);
            controller.Url = new UrlHelper(new RequestContext(httpContextMock.Object, new RouteData()));
        }

        [TestMethod]
        public void ManageController_IndexTest1()
        {
            //Act
            var result = controller.Index(0);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_IndexTest2()
        {
            //Act
            var result = controller.Index((ManageController.ManageMessageId)9);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_IndexTest3()
        {
            //Act
            var result = controller.Index((ManageController.ManageMessageId)5);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_ChangeProfilePhotoTest1() { 

            //Asemble
            var model = new IndexViewModel();

            //Act
            var result = controller.ChangeProfilePhoto(model);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_ChangeProfilePhotoTest2()
        {

            //Act
            var result = controller.ChangeProfilePhoto(null);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_ChangeLocationTest1()
        {

            //Asemble
            var model = new ChangeLocationViewModel();
            var value = new FormCollection();
         
            //Act
            var result = controller.ChangeLocation(model, value);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_ChangeLocationTest2()
        {

            //Asemble
            var model = new ChangeLocationViewModel();

            //Act
            var result = controller.ChangeLocation(model, null);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_RemoveLoginTest1()
        {

            //Asemble
            var provider = "Google";
            var key = "xxx";

            //Act
            var result = controller.RemoveLogin(provider, key);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_RemoveLoginTest2()
        {

            //Asemble
            var provider = "Yahoo";
            var key = "xYxY";

            //Act
            var result = controller.RemoveLogin(provider, key);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_AddPhoneNumberTest1()
        {

            //Act
            var result = controller.AddPhoneNumber();

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_AddPhoneNumberTest2()
        {
    
            //Act
            var result = controller.AddPhoneNumber();

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_Enable2FactorAuthTest1()
        {

            //Act
            var result = controller.EnableTwoFactorAuthentication();

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_Disable2FactorAuthTest1()
        {

            //Act
            var result = controller.DisableTwoFactorAuthentication();

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_VerifyPhoneNumberTest1()
        {

            //Asemble
            var model = new VerifyPhoneNumberViewModel();

            //Act
            var result = controller.VerifyPhoneNumber(model);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_VerifyPhoneNumberTest2()
        {

            //Act
            var result = controller.VerifyPhoneNumber((AddPhoneNumberViewModel)null);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_RemovePhoneNumberTest1()
        {

            //Act
            var result = controller.RemovePhoneNumber();

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_ChangePasswordTest1()
        {

            //Asemble
            var model = new ChangePasswordViewModel();

            //Act
            var result = controller.ChangePassword(model);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_ChangePasswordTest2()
        {

            //Act
            var result = controller.ChangePassword(null);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_SetPasswordTest1()
        {

            //Asemble
            var model = new SetPasswordViewModel();

            //Act
            var result = controller.SetPassword(model);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ManageController_SetPasswordTest2()
        {

            //Act
            var result = controller.SetPassword(null);

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
