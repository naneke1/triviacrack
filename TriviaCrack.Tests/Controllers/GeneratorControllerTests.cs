﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TriviaCrack.Data_Access;
using Moq;
using TriviaCrack.Models;
using System.Web.Routing;
using System.Web;
using System.Web.Mvc;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.MockSets;
using TriviaCrack.Tests.HttpContextMock;

namespace TriviaCrack.Controllers.Tests
{
    [TestClass()]
    public class GeneratorControllerTests
    {
        private Mock<TriviaContext> mockContext;
        private List<Player> players;
        private List<Question> questions;
        private List<Answer> answers;
        private GeneratorController generatorController;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.players = testData.Players;
            this.answers = testData.Answers;
            this.questions = testData.Questions;

            var answersMockSet = new AnswerMockSet(this.answers).GetDbSet<Answer>();
            var questionsMockSet = new QuestionMockSet(this.questions).GetDbSet<Question>();
            var playersMockSet = new PlayerMockSet(this.players).GetDbSet<Player>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Answers).Returns(answersMockSet.Object);
            mockContext.Setup(c => c.Questions).Returns(questionsMockSet.Object);
            mockContext.Setup(c => c.Players).Returns(playersMockSet.Object);

            generatorController = new GeneratorController(mockContext.Object, "a1");

            var httpContextMock = new HttpContextMock().GetMockHttpContext();
            generatorController.ControllerContext = new ControllerContext(httpContextMock.Object, new RouteData(), generatorController);
            generatorController.Url = new UrlHelper(new RequestContext(httpContextMock.Object, new RouteData()));
        }

        [TestMethod()]
        public void QuestionsTest()
        {
            //Act
            var result = generatorController.Questions() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void CreateQuestionsTest()
        {
            //Act
            var result = generatorController.CreateQuestions(4, "25", "Hard") as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}