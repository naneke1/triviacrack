﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriviaCrack.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using System.Web.Mvc;
using System.Web;
using System.Web.Routing;
using TriviaCrack.Models;
using TriviaCrack.Data_Access;
using System.Data.Entity;
using System.Security.Principal;
using System.IO;
using System.Web.SessionState;
using System.Reflection;
using TriviaCrack.Tests.HttpContextMock;

namespace TriviaCrack.Controllers.Tests
{
    [TestClass()]
    public class HomeControllerTests
    {
        private Mock<TriviaContext> mockContext;
        private Mock<HttpContextBase> httpContextMock;

        private HomeController controller;
        private List<Player> players;
        private List<Game> games;

        [TestInitialize]
        public void Initialize()
        {
            this.players = new List<Player>
            {
                new Player
                {
                    Id = "a1",
                    UserName = "admin-1",
                    OverallScore = 30
                },

                new Player
                {
                    Id = "a2",
                    UserName = "admin-2",
                    OverallScore = 40
                }
            };

            this.games = new List<Game>
            {
                new Game
                {
                Id = 5,
                Player1Id = "a1",
                Player1 = players[0],
                Player2Id = "a2",
                Player2 = players[1],
                StartDate = Convert.ToDateTime(DateTime.UtcNow.ToString("MM/dd/yyyy")),
                IsActive = true,
                PlayerTurn = "a1",
                ChallengeInProgress = false
                }
            };

            var playerMockSet = new Mock<DbSet<Player>>();
            playerMockSet.As<IQueryable<Player>>().Setup(s => s.ElementType).Returns(players.AsQueryable().ElementType);
            playerMockSet.As<IQueryable<Player>>().Setup(s => s.Expression).Returns(players.AsQueryable().Expression);
            playerMockSet.As<IQueryable<Player>>().Setup(s => s.Provider).Returns(players.AsQueryable().Provider);
            playerMockSet.As<IQueryable<Player>>().Setup(s => s.GetEnumerator()).Returns(() => players.GetEnumerator());

            var gameMockSet = new Mock<DbSet<Game>>();
            gameMockSet.As<IQueryable<Game>>().Setup(s => s.ElementType).Returns(games.AsQueryable().ElementType);
            gameMockSet.As<IQueryable<Game>>().Setup(s => s.Expression).Returns(games.AsQueryable().Expression);
            gameMockSet.As<IQueryable<Game>>().Setup(s => s.Provider).Returns(games.AsQueryable().Provider);
            gameMockSet.As<IQueryable<Game>>().Setup(s => s.GetEnumerator()).Returns(() => games.GetEnumerator());
            gameMockSet.Setup(s => s.Find(It.IsAny<int>())).Returns<object[]>(d => this.games.Find(m => m.Id == (int)d[0]));

            this.mockContext = new Mock<TriviaContext>();
            playerMockSet.Setup(m => m.Add(It.IsAny<Player>())).Callback<Player>(players.Add);
            mockContext.Setup(c => c.Players).Returns(playerMockSet.Object);
            mockContext.Setup(c => c.Games).Returns(gameMockSet.Object);

            HttpContextMock contextMock = new HttpContextMock();
            httpContextMock = contextMock.GetMockHttpContext();
            httpContextMock.Setup(c => c.Session["LoggedInUserId"]).Returns("a1");
            HttpContext.Current = contextMock.httpContext;

            controller = new HomeController(mockContext.Object);
            controller.ControllerContext = new ControllerContext(httpContextMock.Object, new RouteData(), controller);
            controller.Url = new UrlHelper(new RequestContext(httpContextMock.Object, new RouteData()));
        }

        [TestMethod()]
        public void IndexTest()
        {
            //Arrange
            var userIdValue = "LoggedInUserId";
            var userIdKey = "a1";

            //Act
            HttpContext.Current.Session.Add(userIdValue, userIdKey);
            var result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("TriviaCrack.ViewModels.PlayerGames", result.Model.ToString());
            Assert.AreEqual("loggedInPlayerId", result.ViewData.Keys.ElementAt(0).ToString());
            Assert.AreEqual("a1", result.ViewData.Values.ElementAt(0).ToString());
        }

        [TestMethod()]
        public void Resign_Get_Test()
        {
            //Act
            var result = controller.Resign(5) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("TriviaCrack.Models.Game", result.Model.ToString());
            Assert.AreEqual("LoggedInPlayerId", result.ViewData.Keys.ElementAt(0).ToString());
            Assert.AreEqual("a1", result.ViewData.Values.ElementAt(0).ToString());
        }

        [TestMethod()]
        public void ResignGame_Should_Redirect_To_Index_Test()
        {
            //Act
            var result = controller.ResignGame(5) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.RouteValues.Values.ElementAt(0).ToString());
        }
    }
}