﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TriviaCrack.Controllers;
using TriviaCrack.Data_Access;
using TriviaCrack.Models;
using TriviaCrack.Tests.MockSets;
using TriviaCrack.Tests.Test_Data;

namespace TriviaCrack.Tests.Controllers
{
    [TestClass()]
    public class AnswerControllerTests
    {
        private List<Answer> answers;
        private Mock<TriviaContext> mockContext;
        private AnswerController answerController;

        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.answers = testData.Answers;

            var answersMockSet = new AnswerMockSet(this.answers).GetDbSet<Answer>();
            answersMockSet.Setup(m => m.Remove(It.IsAny<Answer>()))
                .Callback<Answer>((entity) => this.answers.Remove(entity));

            this.mockContext = new Mock<TriviaContext>();

            mockContext.Setup(c => c.Answers).Returns(answersMockSet.Object);
            this.answerController = new AnswerController(mockContext.Object);
        }

        [TestMethod()]
        public void IndexTest()
        {
            //Act
            var result = answerController.Index(1) as ViewResult;
            var model = result.Model as PagedList<Answer>;

            //Assert
            Assert.AreEqual(8, model.Count);
        }

        [TestMethod()]
        public void Create_Should_Redirect_To_Index_When_Model_Valid()
        {
            //Arrange
            var answer = new Answer { Text = "new answer" };
            //Act
            var result = this.answerController.Create(answer) as RedirectToRouteResult;

            //Assert
            mockContext.Verify(c => c.SaveChanges());
            Assert.AreEqual("Index" as object, result.RouteValues["action"]);
        }

        [TestMethod]
        public void Create_Answer_Should_Increment_List_Count()
        {
            //Act
            var initialListSize = this.answers.Count;
            var answer = new Answer { Text = "Alaska" };
            this.answerController.Create(answer);

            //Assert
            Assert.IsFalse(this.answers.Count == initialListSize);
        }

        [TestMethod()]
        public void Edit_Should_Redirect_To_Index_When_Model_Valid()
        {
            //Act
            var result = this.answerController.Edit(this.answers[0]) as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("Index" as object, result.RouteValues["action"]);
        }

        [TestMethod]
        public void Edit_Answer_Should_Return_Model_In_View()
        {
            //Act
            var results = this.answerController.Edit(1) as ViewResult;
            var answerRendered = (Answer)results.ViewData.Model;

            //Assert
            Assert.AreEqual(1, answerRendered.Id);
            Assert.AreEqual("1990", answerRendered.Text);
        }

        [TestMethod]
        public void Details_Should_Return_Model_In_View()
        {
            //Act
            var results = this.answerController.Details(2) as ViewResult;
            var answerRendered = (Answer)results.ViewData.Model;

            //Assert
            Assert.AreEqual(2, answerRendered.Id);
            Assert.AreEqual("1995", answerRendered.Text);
        }

        [TestMethod()]
        public void Delete_Should_Redirect_To_Index_When_Model_Valid()
        {
            //Act
            var result = this.answerController.DeleteConfirmed(1) as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("Index" as object, result.RouteValues["action"]);
        }

        [TestMethod]
        public void Delete_Answer_Should_Remove_Answer_From_List()
        {
            //Act
            this.answerController.DeleteConfirmed(5);

            //Assert
            mockContext.Verify(x => x.Answers, Times.Exactly(2));
            mockContext.Verify(x => x.SaveChanges(), Times.Once());

            Assert.IsFalse(this.answers.Any(x => x.Id == 5));
        }
    }
}
