﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriviaCrack.Models;
using System.Collections.Generic;
using TriviaCrack.Data_Access;
using Moq;
using TriviaCrack.Tests.MockSets;
using TriviaCrack.Tests.Test_Data;
using TriviaCrack.Tests.HttpContextMock;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web;
using static System.Collections.Specialized.NameObjectCollectionBase;
using System.Linq;
using TriviaCrack.ViewModels;

namespace TriviaCrack.Controllers.Tests
{
    [TestClass]
    public class TrivaControllerTests
    {
        private Mock<TriviaContext> mockContext;
        private List<Player> players;
        private List<Game> games;
        private List<Statistics> stats;
        private List<CategoriesCollected> categoriesCollected;
        private List<Streak> streaks;
        private List<Question> questions;
        private TriviaController controller;
       
        [TestInitialize]
        public void Initialize()
        {
            TestData testData = new TestData();
            this.players = testData.Players;
            this.games = testData.Games;
            this.stats = testData.Statistics;
            this.categoriesCollected = testData.CategoriesCollected;
            this.streaks = testData.Streaks;
            this.questions = testData.Questions;

            var playersMockSet = new PlayerMockSet(this.players).GetDbSet<Player>();
            var gamesMockSet = new GameMockSet(this.games).GetDbSet<Game>();
            var statsMockSet = new StatisticMockSet(this.stats).GetDbSet<Statistics>();
            var categoriesCollectedMockSet = new CategoriesCollectedMockSet(this.categoriesCollected).GetDbSet<CategoriesCollected>();
            var streakMockSet = new StreakMockSet(this.streaks).GetDbSet<Streak>();
            var questionMockSet = new QuestionMockSet(this.questions).GetDbSet<Question>();

            this.mockContext = new Mock<TriviaContext>();
            mockContext.Setup(c => c.Players).Returns(playersMockSet.Object);
            mockContext.Setup(c => c.Games).Returns(gamesMockSet.Object);
            mockContext.Setup(c => c.Statistics).Returns(statsMockSet.Object);
            mockContext.Setup(c => c.CategoriesCollected).Returns(categoriesCollectedMockSet.Object);
            mockContext.Setup(c => c.Streaks).Returns(streakMockSet.Object);
            mockContext.Setup(c => c.Questions).Returns(questionMockSet.Object);

            controller = new TriviaController(mockContext.Object, testData.LoggedInPlayer.Id);

            SetupMockHttp(5, 4);
        }

        private void SetupMockHttp(int gameId, int questionId)
        {
            var server = new Mock<HttpServerUtilityBase>(MockBehavior.Loose);
            var response = new Mock<HttpResponseBase>(MockBehavior.Strict);
            var request = new Mock<HttpRequestBase>(MockBehavior.Strict);
            request.Setup(r => r.UserHostAddress).Returns("127.0.0.1");
            var session = new Mock<HttpSessionStateBase>();
            session.Setup(s => s.SessionID).Returns(Guid.NewGuid().ToString());
            session.SetupGet(s => s["gameToResume"]).Returns(gameId);
            session.SetupGet(s => s["currentQuestionId"]).Returns(questionId);

            var httpContextMock = new HttpContextMock().GetMockHttpContext();
            httpContextMock.SetupGet(c => c.Request).Returns(request.Object);
            httpContextMock.SetupGet(c => c.Response).Returns(response.Object);
            httpContextMock.SetupGet(c => c.Server).Returns(server.Object);
            httpContextMock.SetupGet(c => c.Session).Returns(session.Object);

            controller.ControllerContext = new ControllerContext(httpContextMock.Object, new RouteData(), controller);
            controller.Url = new UrlHelper(new RequestContext(httpContextMock.Object, new RouteData()));
        }

        private void SetupMockHttpForCategories(PlayerCategories playersCategories, string questionType)
        {
            var server = new Mock<HttpServerUtilityBase>(MockBehavior.Loose);
            var response = new Mock<HttpResponseBase>(MockBehavior.Strict);
            var request = new Mock<HttpRequestBase>(MockBehavior.Strict);
            request.Setup(r => r.UserHostAddress).Returns("127.0.0.1");
            var session = new Mock<HttpSessionStateBase>();
            session.Setup(s => s.SessionID).Returns(Guid.NewGuid().ToString());
            session.SetupGet(s => s["PlayerCategories"]).Returns(playersCategories);
            session.SetupGet(s => s["QuestionType"]).Returns(questionType);

            var httpContextMock = new HttpContextMock().GetMockHttpContext();
            httpContextMock.SetupGet(c => c.Request).Returns(request.Object);
            httpContextMock.SetupGet(c => c.Response).Returns(response.Object);
            httpContextMock.SetupGet(c => c.Server).Returns(server.Object);
            httpContextMock.SetupGet(c => c.Session).Returns(session.Object);

            controller.ControllerContext = new ControllerContext(httpContextMock.Object, new RouteData(), controller);
            controller.Url = new UrlHelper(new RequestContext(httpContextMock.Object, new RouteData()));
        }
        [TestMethod]
        public void TriviaController_CollectCategoryTest1()
        {
            //assemble
            var gameId = 5;
            var category = "art";

            //act
            var result = controller.CollectCategory(gameId, category);

            //assert
            Assert.IsTrue(this.stats[3].IsAnsweredCorrectly);
        }

        [TestMethod]
        public void TriviaController_CollectCategoryTest2()
        {
            //assemble
            var gameId = 5;
            var category = "Sports";
            var questionId = 6;
            SetupMockHttp(gameId, questionId);

            //act
            var result = controller.CollectCategory(gameId, category);

            //assert
            Assert.IsTrue(this.stats[5].IsAnsweredCorrectly);
        }

        [TestMethod]
        public void TriviaController_CollectCategoryTest3()
        {
            //assemble
            var gameId = 5;
            var category = "Science";
            var questionId = 5;
            SetupMockHttp(gameId, questionId);

            //act
            var result = controller.CollectCategory(gameId, category);

            //assert
            Assert.IsTrue(this.stats[2].IsAnsweredCorrectly);
        }

        [TestMethod]
        public void TriviaController_UpdateCountTest1()
        {
            //assemble
            var questionId = "5";
            var countName = "Fun";

            //act
            var result = controller.UpdateCount(questionId, countName);
            var funCount = this.questions[4].FunRatingCount;

            //assert
            Assert.AreEqual(1, funCount + 1);
        }

        [TestMethod]
        public void TriviaController_UpdateCountTest2()
        {
            //assemble
            var questionId = "5";
            var countName = "Boring";

            //act
            var result = controller.UpdateCount(questionId, countName);
            var boringCount = this.questions[4].BoringRatingCount;

            //assert
            Assert.AreEqual(2, boringCount + 1);
        }

        [TestMethod]
        public void TriviaController_UpdateCountTest3()
        {
            //assemble
            var questionId = "2";
            var countName = "Fun";

            //act
            var result = controller.UpdateCount(questionId, countName);
            var funCount = this.questions[1].FunRatingCount;

            //assert
            Assert.AreNotEqual(3, funCount + 1);
        }

        [TestMethod]
        public void TriviaController_SelectCategoryTest1()
        {
            //assemble
            var gameId = 5;
            var type = "Crown";
            var playerCategories = new PlayerCategories();
            SetupMockHttpForCategories(playerCategories, type);

            //act
            var result = controller.SelectCategory(gameId, type);

            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TriviaController_SelectCategoryTest2()
        {
            //assemble
            var gameId = 5;
            var type = "Challenge";
            var playerCategories = new PlayerCategories();
            SetupMockHttpForCategories(playerCategories, type);

            //act
            var result = controller.SelectCategory(gameId, type);

            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TriviaController_SelectCategoryTest3()
        {
            //assemble
            var gameId = 5;
            var type = "Challenge";
            var playerCategories = new PlayerCategories();
            SetupMockHttpForCategories(playerCategories, type);
            var player1Streak = this.streaks[0].PlayerStreak;
            var resetValue = 2;
            //act
            var result = controller.SelectCategory(gameId, type);

            //assert
            Assert.AreEqual(0, player1Streak - resetValue);
        }
    }
}
