﻿using System;
using System.Collections.Generic;
using TriviaCrack.Models;

namespace TriviaCrack.Tests.Test_Data
{
    internal class TestData
    {
        internal List<Player> Players { get; set; }
        internal List<Question> Questions { get; set; }
        internal List<Answer> Answers { get; set; }
        internal List<Game> Games { get; set; }
        internal List<CategoriesCollected> CategoriesCollected { get; set; }
        internal List<Streak> Streaks { get; set; }
        internal List<Category> Categories { get; set; }
        internal List<ClassicChallengeQuestion> ClassicChallengeQuestions { get; set; }
        internal List<Statistics> Statistics { get; set; }
        internal List<PowerUp> PowerUps { get; set; }

        internal Player LoggedInPlayer { get; set; }
        internal Player OpponentPlayer { get; set; }

        public TestData()
        {
            this.InitializePlayers();
            this.InitializeCategories();
            this.InitializeAnswers();
            this.InitializeQuestions();
            this.InitializeGames();
            this.InitializeCategoriesCollected();
            this.InitializeStreaks();
            this.InitializeClassicChallengeQuestions();
            this.InitializeStats();
            this.InitializePowerUps();
        }

        private void InitializePlayers()
        {
            Players = new List<Player>
            {
                new Player
                {
                    Id = "a1",
                    UserName = "player-1",
                    OverallScore = 30,
                    Level = 5,
                    PrevScoreThreshold = 35
                },
                new Player
                {
                    Id = "a2",
                    UserName = "player-2",
                    OverallScore = 25,
                    Level = 4,
                    PrevScoreThreshold = 30
                }
            };

            this.LoggedInPlayer = Players[0];
            this.OpponentPlayer = Players[1];
        }

        private void InitializeAnswers()
        {
            Answers = new List<Answer> {
                new Answer {Id = 1, Text = "1990"},
                new Answer {Id = 2, Text = "1995"},
                new Answer {Id = 3, Text = "2000"},
                new Answer {Id = 4, Text = "2005"},
                new Answer {Id = 5, Text = "Blue"},
                new Answer {Id = 6, Text = "Yellow"},
                new Answer {Id = 7, Text = "Green"},
                new Answer {Id = 8, Text = "Purple"},
                new Answer {Id = 9, Text = "2011"},
                new Answer {Id = 10, Text = "2018"},
                new Answer {Id = 11, Text = "2016"},
                new Answer {Id = 12, Text = "2015"},
                new Answer {Id = 13, Text = "zz"},
                new Answer {Id = 14, Text = "zzz"},
                new Answer {Id = 15, Text = "zzzz"},
                new Answer {Id = 16, Text = "zzzzz"}
            };
        }

        private void InitializeCategories()
        {
            Categories = new List<Category>
            {
                new Category { Description = "Art" },
                new Category { Description = "Entertainment" },
                new Category { Description = "Sports" },
                new Category { Description = "Geography" },
                new Category { Description = "History" },
                new Category { Description = "Science" }
            };
        }

        private void InitializeQuestions()
        {
            Questions = new List<Question> {
                new Question {
                    Id = 1, Text = "How do you make green?", CategoryId = 1, Category = Categories[0],
                    AnswerChoice1Id = 1, AnswerChoice2Id = 2,
                    AnswerChoice3Id = 3, AnswerChoice4Id = 4,
                    CorrectAnswerId = 1,
                    Difficulty = "Easy"
                },
                new Question {
                    Id = 2, Text = "What color is the sky?", CategoryId = 1, Category = Categories[0],
                    AnswerChoice1Id = 5, AnswerChoice2Id = 6,
                    AnswerChoice3Id = 7, AnswerChoice4Id = 8,
                    CorrectAnswerId = 5,
                    Difficulty = "Easy"
                },
                new Question {
                    Id = 3, Text = "What is the current year?", CategoryId = 1, Category = Categories[0],
                    AnswerChoice1 = Answers[8],
                    AnswerChoice2 = Answers[9],
                    AnswerChoice3 = Answers[10],
                    AnswerChoice4 = Answers[11],
                    CorrectAnswerId = 10,
                    Difficulty = "Easy"
                },
                new Question {
                    Id = 4, Text = "Random question?", CategoryId = 1, Category = Categories[0],
                    AnswerChoice1 = Answers[12],
                    AnswerChoice2 = Answers[13],
                    AnswerChoice3 = Answers[14],
                    AnswerChoice4 = Answers[15],
                    CorrectAnswerId = 16,
                    Difficulty = "Medium"
                },
                new Question {
                    Id = 5, Text = "Example Question 5?", CategoryId = 6, Category = Categories[5],
                    AnswerChoice1 = new Answer {Id = 17, Text = "nope"},
                    AnswerChoice2 = new Answer {Id = 18, Text = "not it"},
                    AnswerChoice3 = new Answer {Id = 19, Text = "correct"},
                    AnswerChoice4 = new Answer {Id = 20, Text = "try again"},
                    CorrectAnswerId = 19,
                    Difficulty = "Medium"
                },
                new Question {
                    Id = 6, Text = "Example Question 6?", CategoryId = 3, Category = Categories[2],
                    AnswerChoice1 = new Answer {Id = 21, Text = "you got it"},
                    AnswerChoice2 = new Answer {Id = 22, Text = "running"},
                    AnswerChoice3 = new Answer {Id = 23, Text = "out of "},
                    AnswerChoice4 = new Answer {Id = 24, Text = "questions"},
                    CorrectAnswerId = 21,
                    Difficulty = "Medium"
                },
                new Question {
                    Id = 7, Text = "Example Question 7?", CategoryId = 1, Category = Categories[0],
                    AnswerChoice1 = new Answer {Id = 21, Text = "you got it"},
                    AnswerChoice2 = new Answer {Id = 22, Text = "running"},
                    AnswerChoice3 = new Answer {Id = 23, Text = "out of "},
                    AnswerChoice4 = new Answer {Id = 24, Text = "questions"},
                    CorrectAnswerId = 23,
                    Difficulty = "Hard"
                },
                new Question {
                    Id = 8, Text = "Example Question 8?", CategoryId = 1, Category = Categories[0],
                    AnswerChoice1 = new Answer {Id = 21, Text = "you got it"},
                    AnswerChoice2 = new Answer {Id = 22, Text = "running"},
                    AnswerChoice3 = new Answer {Id = 23, Text = "out of "},
                    AnswerChoice4 = new Answer {Id = 24, Text = "questions"},
                    CorrectAnswerId = 22,
                    Difficulty = "Hard"
                },
                new Question {
                    Id = 9, Text = "Example Question 9?", CategoryId = 1, Category = Categories[0],
                    AnswerChoice1 = new Answer {Id = 21, Text = "you got it"},
                    AnswerChoice2 = new Answer {Id = 22, Text = "running"},
                    AnswerChoice3 = new Answer {Id = 23, Text = "out of "},
                    AnswerChoice4 = new Answer {Id = 24, Text = "questions"},
                    CorrectAnswerId = 2,
                    Difficulty = "Hard"
                },
                new Question {
                    Id = 10, Text = "Example Question 10?", CategoryId = 3, Category = Categories[2],
                    AnswerChoice1 = new Answer {Id = 21, Text = "you got it"},
                    AnswerChoice2 = new Answer {Id = 22, Text = "running"},
                    AnswerChoice3 = new Answer {Id = 23, Text = "out of "},
                    AnswerChoice4 = new Answer {Id = 24, Text = "questions"},
                    CorrectAnswerId = 2,
                    Difficulty = "Hard"
                }
            };
        }

        private void InitializeGames()
        {
            Games = new List<Game>
            {
                new Game
                {
                    Id = 5,
                    Player1Id = LoggedInPlayer.Id,
                    Player1 = Players[0],
                    Player2Id = OpponentPlayer.Id,
                    Player2 = Players[1],
                    StartDate = Convert.ToDateTime(DateTime.UtcNow.ToString("MM/dd/yyyy")),
                    IsActive = true,
                    PlayerTurn = LoggedInPlayer.Id,
                    ChallengeInProgress = false
                }
            };
        }

        private void InitializeCategoriesCollected()
        {
            CategoriesCollected = new List<CategoriesCollected>
            {
               new CategoriesCollected
                {
                    GameId = Games[0].Id,
                    PlayerId = LoggedInPlayer.Id,
                    ArtCollected = false,
                    EntertainmentCollected = false,
                    SportsCollected = false,
                    GeographyCollected = false,
                    HistoryCollected = false,
                    ScienceCollected = false
                },
               new CategoriesCollected
                {
                    GameId = Games[0].Id,
                    PlayerId = OpponentPlayer.Id,
                    ArtCollected = false,
                    EntertainmentCollected = false,
                    SportsCollected = false,
                    GeographyCollected = false,
                    HistoryCollected = false,
                    ScienceCollected = false
                }
            };
        }

        private void InitializeStreaks()
        {
            Streaks = new List<Streak>
            {
                new Streak { GameId = 5, PlayerId = LoggedInPlayer.Id, PlayerStreak = 2 },
                new Streak { GameId = 5, PlayerId = OpponentPlayer.Id, PlayerStreak = 1}
            };
        }

        private void InitializeClassicChallengeQuestions()
        {
            ClassicChallengeQuestions = new List<ClassicChallengeQuestion>
            {
               new ClassicChallengeQuestion
                {
                    GameId = Games[0].Id,
                    PlayerId = LoggedInPlayer.Id,
                    Question1Id = 1,
                    Question1 = Questions[0],
                    Question2Id = 2,
                    Question2 = Questions[1],
                    Question3Id = 3,
                    Question3 = Questions[2],
                    Question4Id = 4,
                    Question4 = Questions[3],
                    Question5Id = 8,
                    Question5 = Questions[7],
                    Question6Id = 9,
                    Question6 = Questions[8],
                    TiebreakerQuestionId = 7,
                    TiebreakerQuestion = Questions[6],
                },
               new ClassicChallengeQuestion
                {
                    GameId = Games[0].Id,
                    PlayerId = OpponentPlayer.Id,
                    Question1Id = 1,
                    Question1 = Questions[0],
                    Question2Id = 2,
                    Question2 = Questions[1],
                    Question3Id = 3,
                    Question3 = Questions[2],
                    Question4Id = 4,
                    Question4 = Questions[3],
                    Question5Id = 8,
                    Question5 = Questions[7],
                    Question6Id = 9,
                    Question6 = Questions[8],
                    TiebreakerQuestionId = 7,
                    TiebreakerQuestion = Questions[6],
                }
            };
        }

        private void InitializeStats()
        {
            Statistics = new List<Statistics> {
                new Statistics
                {
                    GameId = 5,
                    PlayerId = LoggedInPlayer.Id,
                    QuestionId = 3,
                    Question = Questions[2],
                    IsAnsweredCorrectly = true,
                    DateAnswered = DateTime.Now
                },
                new Statistics
                {
                    GameId = 5,
                    PlayerId = LoggedInPlayer.Id,
                    QuestionId = 1,
                    Question = Questions[0],
                    IsAnsweredCorrectly = true,
                    DateAnswered = DateTime.Now
                },
                new Statistics
                {
                    GameId = 5,
                    PlayerId = LoggedInPlayer.Id,
                    QuestionId = 5,
                    Question = Questions[4],
                    IsAnsweredCorrectly = false,
                    DateAnswered = DateTime.Now
                },
                new Statistics
                {
                    GameId = 5,
                    PlayerId = LoggedInPlayer.Id,
                    QuestionId = 4,
                    Question = Questions[3],
                    IsAnsweredCorrectly = false,
                    DateAnswered = DateTime.Now
                },
                new Statistics
                {
                    GameId = 5,
                    PlayerId = OpponentPlayer.Id,
                    QuestionId = 2,
                    Question = Questions[1],
                    IsAnsweredCorrectly = true,
                    DateAnswered = DateTime.Now
                },
                new Statistics
                {
                    GameId = 5,
                    PlayerId = LoggedInPlayer.Id,
                    QuestionId = 6,
                    Question = Questions[5],
                    IsAnsweredCorrectly = false,
                    DateAnswered = DateTime.Now
                },
            };
        }

        private void InitializePowerUps()
        {
            this.PowerUps = new List<PowerUp>
            {
                new PowerUp
                {
                    Name = "Extra Time",
                    Cost = 1
                },

                new PowerUp
                {
                    Name = "Bomb",
                    Cost = 5
                },

                new PowerUp
                {
                    Name = "Auto Correct",
                    Cost = 250
                }
            };
        }
    }
}
