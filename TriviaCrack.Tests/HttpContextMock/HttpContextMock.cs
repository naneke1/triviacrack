﻿using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using System.Web.SessionState;

namespace TriviaCrack.Tests.HttpContextMock
{
    public class HttpContextMock
    {
        public HttpContext httpContext {get; set;}

        public Mock<HttpContextBase> GetMockHttpContext()
        {
            var routes = new RouteCollection();
            RouteConfig.RegisterRoutes(routes);

            var requestContextMock = new Mock<HttpRequestBase>();
            requestContextMock.Setup(r => r.AppRelativeCurrentExecutionFilePath).Returns("/");
            requestContextMock.Setup(r => r.ApplicationPath).Returns("/");

            var session = new Mock<HttpSessionStateBase>();

            var responseMock = new Mock<HttpResponseBase>();
            responseMock.Setup(s => s.ApplyAppPathModifier(It.IsAny<string>())).Returns<string>(s => s);

            var httpContextMock = new Mock<HttpContextBase>();
            httpContextMock.Setup(h => h.Request).Returns(requestContextMock.Object);
            httpContextMock.Setup(h => h.Response).Returns(responseMock.Object);

            var fakeIdentity = new GenericIdentity("User");
            var principal = new GenericPrincipal(fakeIdentity, null);

            httpContextMock.Setup(t => t.User).Returns(principal);

            var httpRequest = new HttpRequest("", "http://localhost/", "")
            {
                ContentEncoding = Encoding.UTF8
            };
            var httpResponce = new HttpResponse(new StringWriter());
            httpContext = new HttpContext(httpRequest, httpResponce);
            var sessionContainer =
                new HttpSessionStateContainer("id",
                                               new SessionStateItemCollection(),
                                               new HttpStaticObjectsCollection(),
                                               10,
                                               true,
                                               HttpCookieMode.AutoDetect,
                                               SessionStateMode.InProc,
                                               false);
            httpContext.Items["AspSession"] =
                typeof(HttpSessionState)
                .GetConstructor(
                                    BindingFlags.NonPublic | BindingFlags.Instance,
                                    null,
                                    CallingConventions.Standard,
                                    new[] { typeof(HttpSessionStateContainer) },
                                    null)
                .Invoke(new object[] { sessionContainer });

            var data = new Dictionary<string, object>()
            {
                {"a", "b"}
            };

            httpContext.Items["owin.Environment"] = data;

            return httpContextMock;
        }
    }
}
